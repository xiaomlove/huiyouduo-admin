
测试域名: [http://veykoo.com:7777](http://veykoo.com:7777)  
正式域名: [http://huiyouduoapp.com](http://huiyouduoapp.com)  

测试服务器宿主机路径：/data/docker-compose/code/huiyouduo-admin  
容器内路径: /code/huiyouduo-admin  

由于宿主机环境不能满足，当需要执行重置或其他操作时，请进入容器内操作：33
```
docker exec -it container-php-fpm bash
cd /code/huiyouduo-admin
php artisan migrate:refresh --seed
```

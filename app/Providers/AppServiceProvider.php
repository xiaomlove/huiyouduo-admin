<?php

namespace App\Providers;

use App\Models\Like;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Relations\Relation;

use App\Models\Comment;
use App\Models\Topic;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Illuminate\Support\Facades\Schema::defaultStringLength(191);
        Carbon::setLocale('zh');
        $this->customRule();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\Faker\Generator::class, function() {
            return \Faker\Factory::create('zh_CN');
        });
    }

    private function customRule()
    {
        \Validator::extend('phone', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^1[\d]{10}$/', $value);
        }, ':attribute 不是合法的手机号码');
    }

}

<?php

namespace App\Models;

use Encore\Admin\Auth\Database\Administrator;
use App\User;

class Admin extends Administrator
{
    /**
     * 是否super admin
     *
     * @return bool|mixed
     */
    public function isSuperAdmin()
    {
        return $this->isRole(Role::SLUG_ADMIN);
    }
    /**
     * 是否官方人员
     *
     * @return bool|mixed
     */
    public function isOfficial()
    {
        return $this->isRole(Role::SLUG_OFFICIAL);
    }

    public function isSuperior()
    {
        return $this->isSuperAdmin() || $this->isOfficial();
    }

    /**
     * 是否一级代理商
     *
     * @return bool|mixed
     */
    public function isAgentLevelOne()
    {
        return $this->isRole(Role::SLUG_AGENT_LEVEL_1);
    }
    /**
     * 是否二级代理商
     *
     * @return bool|mixed
     */
    public function isAgentLevelTwo()
    {
        return $this->isRole(Role::SLUG_AGENT_LEVEL_2);
    }

    /**
     * 是否代理商，任何一级都算
     *
     * @return bool|mixed
     *
     */
    public function isAgent()
    {
        return $this->inRoles(Role::$agentRoles);
    }

    /**
     * 绑定的前端用户
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function frontUser()
    {
        return $this->belongsTo(User::class, "front_uid");
    }

    /**
     * 对应代理商家信息
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function agent()
    {
        return $this->hasOne(Agent::class, "admin_uid");
    }

    public function stores()
    {
        return $this->hasManyThrough(AgentStore::class, Agent::class, 'admin_uid', 'agent_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $table = "items";

    protected $casts = [
        'detail_images' => 'json'
    ];

    protected $fillable = [
        'cid',
        'cate_leaf_id',
        'category_id',
        'category_name',
        'commission_rate',
        'coupon_end_time',
        'coupon_id',
        'coupon_info',
        'coupon_price',
        'coupon_after_price',
        'coupon_remain_count',
        'coupon_start_time',
        'coupon_total_count',
        'item_url',
        'level_one_category_id',
        'level_one_category_name',
        'num_iid',
        'pict_url',
        'provcity',
        'reserve_price',
        'seller_id',
        'shop_dsr',
        'shop_title',
        'short_title',
        'small_images',
        'detail_images',
        'title',
        'tk_total_commi',
        'tk_total_sales',
        'user_type',
        'volume',
        'white_image',
        'zk_final_price',
        'quan_link',
    ];



    /**
     * 一个商品一个一级栏目
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function oneCate()
    {
        return $this->belongsTo(ItemsCate::class, "cid","id");
    }

    /**
     * 一个商品一个二级栏目
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function oneLeafCate()
    {
        return $this->belongsTo(ItemsCate::class, "cate_leaf_id","id");
    }
}

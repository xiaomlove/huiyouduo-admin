<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Banner extends Model
{
    protected $table = "banners";
    protected $fillable = [
        "title",
        "images",
        "type",
        "url",
        "params_id",
        "position",
        "sort",
    ];

    protected $appends = ['item_id'];

    public function getImagesAttribute()
    {
        if(empty($this->attributes['images'])) return "";
        // 如果 image 字段本身就已经是完整的 url 就直接返回
        return imageUrl($this->attributes['images']);
    }

    public function getItemIdAttribute()
    {
        return $this->attributes['params_id'] ?? 0;
    }


}

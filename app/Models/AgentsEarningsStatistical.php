<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentsEarningsStatistical extends Model
{
    protected $table = "agent_earnings_statistical";
    protected $fillable = [
        "day",
        "agent_admin_uid",
        "income",
        "spending",
        "remark",
    ];

}

<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
    use SoftDeletes;

    const TYPE_RECRUITMENT = 1;
    const TYPE_SHENGHUODAREN = 2;
    const TYPE_APPLY_JOB = 3;
    const TYPE_ACTIVITY = 4;
    const TYPE_GOUWUFENXIANG = 5;
    const TYPE_ZHOUBIANFENXIANG = 6;

    public static $typeNames = [
        self::TYPE_RECRUITMENT => '招聘',
        self::TYPE_SHENGHUODAREN => '生活达人',
        self::TYPE_APPLY_JOB => '求职',
        self::TYPE_ACTIVITY => '活动信息',
        self::TYPE_GOUWUFENXIANG => '购物分享',
        self::TYPE_ZHOUBIANFENXIANG => '周边商家',
    ];

    protected $fillable = ['uid', 'type'];

    public function getTypeNameAttribute()
    {
        return self::$typeNames[$this->type] ?? '';
    }

    public function getIsFollowedAttribute()
    {
        $user = \Auth()->user();
        return $user->isFollowing($this->uid) ? 1 : 0;
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, "target");
    }

    public function first_comment()
    {
        return $this->hasOne(Comment::class, "target_id")
            ->where('target_type', __CLASS__)
            ->where('floor_num', 1);
    }

    public function user()
    {
        return $this->belongsTo(User::class, "uid");
    }

    public function activity()
    {
        return $this->hasOne(StoreActivity::class, 'tid');
    }

    public function complains()
    {
        return $this->morphMany(Complain::class, 'target');
    }

    public function shareLogs()
    {
        return $this->morphMany(ShareLog::class, 'target');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TaobaoPids extends Model
{
    protected $table = "tb_pids";
    protected $fillable = [
        "adzone_id",
        "site_id",
        "unid",
        "pid",
        "uid",
    ];
}

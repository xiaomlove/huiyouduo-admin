<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = ['name', 'parent_id', 'code', 'order'];

    public function cities()
    {
        return $this->hasMany(__CLASS__, 'parent_id');
    }

    public function districts()
    {
        return $this->hasMany(__CLASS__, 'parent_id');
    }

    public function province()
    {
        return $this->belongsTo(__CLASS__, 'parent_id');
    }

    public function city()
    {
        return $this->belongsTo(__CLASS__, 'parent_id');
    }

    public function stores()
    {
        return $this->hasMany(AgentStore::class, 'city_code', 'code');
    }

    /**
     * 通过给定的 code 返回城市，这个 code 有可能是市，或者区
     * @param $code
     * @return mixed
     */
    public static function getCityByCode($code)
    {
        $result = static::where('code', $code)->firstOrFail();
        //假定为城市。若不是，则是区。
        if ($result->districts()->count()) {
            return $result;
        } else {
            return $result->city;
        }
    }
}

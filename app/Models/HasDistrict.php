<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

trait HasDistrict
{
    public function province()
    {
        return $this->belongsTo(District::class, "province_code", "code");
    }

    public function city()
    {
        return $this->belongsTo(District::class, "city_code", "code");
    }

    public function district()
    {
        return $this->belongsTo(District::class, "district_code", "code");
    }
}
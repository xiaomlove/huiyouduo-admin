<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Request;
use App\User;

class NewUser extends Model
{
    public function paginate()
    {
        $perPage = Request::get('per_page', 10);

        $page = Request::get('page', 1);

        $isSuperAdmin = \Admin::user()->isSuperAdmin();
        $isOfficial = \Admin::user()->isOfficial();

        if(!$isSuperAdmin && !$isOfficial)
        {
            //如果是代理商管理员登陆
            $user = User::all()->toArray();
            $cur_agent_admin = \Admin::user();
            $agents_admin_front_uid = $cur_agent_admin->front_uid;
            $model_data = subtree($user,$agents_admin_front_uid);
        }
        else
        {
            $model_data = \App\User::all()->toArray();
        }

        $total = count($model_data);

        $datas = static::hydrate($model_data);

        $paginator = new LengthAwarePaginator($datas, $total, $perPage);

        $paginator->setPath(url()->current());

        return $paginator;
    }

    public static function with($relations)
    {
        return new static;
    }

    public function findOrFail($id)
    {
        $data = file_get_contents("http://api.douban.com/v2/movie/subject/$id");

        $data = json_decode($data, true);

        return static::newFromBuilder($data);
    }

    public function save(array $options = [])
    {
        dd($this->getAttributes());
    }
}
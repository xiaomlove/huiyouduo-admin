<?php

namespace App\Models;

use Encore\Admin\Auth\Database\Role as AdminRole;

class Role extends AdminRole
{
    const SLUG_OFFICIAL = 'official';
    const SLUG_ADMIN = 'administrator';
    const SLUG_AGENT = 'agent';//代理商，不区分级别。不区分级别的权限放到这里来。
    const SLUG_AGENT_LEVEL_1 = 'agent-1';
    const SLUG_AGENT_LEVEL_2 = 'agent-2';

    public static $defaults = [
        self::SLUG_OFFICIAL => [
            'name' => '官方人员',
            'permissions' => [
                '*'
            ],
        ],
        self::SLUG_AGENT => [
            'name' => '代理商',
            'permissions' => [
                Permission::SLUG_COUPON_CARD_MANAGEMENT,
                Permission::SLUG_AGENT_MANAGEMENT,// 进设置，编辑自己的信息
                Permission::SLUG_USER,
                Permission::SLUG_ORDER_LIST,
                Permission::SLUG_ORDER_STATISTICAL,
                Permission::SLUG_EARN_STATISTICAL,
                Permission::SLUG_AGENT_SETTLE,
                Permission::SLUG_STORE,
                Permission::SLUG_ROOT,
            ],
        ],
        self::SLUG_AGENT_LEVEL_1 => [
            'name' => '一级代理商',
            'permissions' => [
                Permission::SLUG_AGENT_MANAGEMENT,
                Permission::SLUG_COUPON_ASSIGN_CARD,
            ],
        ],
        self::SLUG_AGENT_LEVEL_2 => [
            'name' => '二级代理商',
            'permissions' => [

            ],
        ],
    ];

    public static $agentRoles = [self::SLUG_AGENT, self::SLUG_AGENT_LEVEL_1, self::SLUG_AGENT_LEVEL_2];

    public function scopeAgent($query)
    {
        $query->where("slug", self::SLUG_AGENT);
    }

    public function scopeAgentLevelOne($query)
    {
        $query->where("slug", self::SLUG_AGENT_LEVEL_1);
    }

    public function scopeAgentLevelTwo($query)
    {
        $query->where("slug", self::SLUG_AGENT_LEVEL_2);
    }

    public function scopeOfficial($query)
    {
        $query->where("slug", self::SLUG_OFFICIAL);
    }
    public function scopeSuperAdmin($query)
    {
        $query->where("slug", self::SLUG_ADMIN);
    }

    /**
     * 判断一个角色是否属于代理商系列
     *
     * @param $role
     * @return bool
     */
    public static function isInAgent($role)
    {
        return in_array($role, self::$agentRoles);
    }

}

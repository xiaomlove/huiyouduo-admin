<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TbkOficalOrder extends Model
{
    protected $table = "tbk_ofical_order";

    protected $fillable = [
        'adzone_id',
        'adzone_name',
        'alipay_total_price',
        'auction_category',
        'click_time',
        'commission',
        'commission_rate',
        'create_time',
        'income_rate',
        'item_num',
        'item_title',
        'num_iid',
        'order_type',
        'pay_price',
        'price',
        'pub_share_pre_fee',
        'seller_nick',
        'seller_shop_title',
        'site_id',
        'site_name',
        'subsidy_rate',
        'subsidy_type',
        'terminal_type',
        'tk3rd_type',
        'tk_status',
        'total_commission_rate',
        'trade_id',
        'trade_parent_id',
    ];

    /**
     * 当给用户结算时，会产生用户资金流水
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function userMoneyWaterLogs()
    {
        return $this->morphMany(UserMoneyWaterLog::class, 'source');
    }

    /**
     * @desc 批量更新
     * @param array $multipleData
     * @return bool
     */
    public function updateBatch($multipleData = [])
    {
        try
        {
            if (empty($multipleData))
            {
                throw new \Exception("数据不能为空");
            }
            $tableName = DB::getTablePrefix() . $this->getTable(); // 表名
            $firstRow  = current($multipleData);

            $updateColumn = array_keys($firstRow);
            // 默认以id为条件更新，如果没有ID则以第一个字段为条件
            $referenceColumn = isset($firstRow['id']) ? 'id' : current($updateColumn);
            unset($updateColumn[0]);
            // 拼接sql语句
            $updateSql = "UPDATE " . $tableName . " SET ";
            $sets      = [];
            $bindings  = [];
            foreach ($updateColumn as $uColumn)
            {
                $setSql = "`" . $uColumn . "` = CASE ";
                foreach ($multipleData as $data)
                {
                    $setSql .= "WHEN `" . $referenceColumn . "` = ? THEN ? ";
                    $bindings[] = $data[$referenceColumn];
                    $bindings[] = $data[$uColumn];
                }
                $setSql .= "ELSE `" . $uColumn . "` END ";
                $sets[] = $setSql;
            }
            $updateSql .= implode(', ', $sets);
            $whereIn   = collect($multipleData)->pluck($referenceColumn)->values()->all();
            $bindings  = array_merge($bindings, $whereIn);
            $whereIn   = rtrim(str_repeat('?,', count($whereIn)), ',');
            $updateSql = rtrim($updateSql, ", ") . " WHERE `" . $referenceColumn . "` IN (" . $whereIn . ")";
            // 传入预处理sql语句和对应绑定数据
            return DB::update($updateSql, $bindings);
        }
        catch (\Exception $e)
        {
            return false;
        }
    }
}

<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserMoneyWaterLog extends Model
{
    protected $fillable = ['uid', 'source_type', 'source_id', 'money'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            $total = $model->where("uid", $model->uid)->where('id', '<=', $model->id)->sum('money');
            $model->total = $total;
            $model->save();
            User::updateMoney($model->uid);
        });
    }

    public static $sourceTypes = [
        WithdrawalLog::class => '提现',
    ];

    public function getSourceTypeTextAttribute()
    {
        return self::$sourceTypes[$this->source_type] ?? '';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }

    public function source()
    {
        return $this->morphTo();
    }

}

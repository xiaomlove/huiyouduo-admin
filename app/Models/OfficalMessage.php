<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfficalMessage extends Model
{
    protected $table = "offical_message";
    protected $fillable = [
        "title",
        "params_id",
    ];

}

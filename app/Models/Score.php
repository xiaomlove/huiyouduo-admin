<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Score extends Model
{
    const TYPE_OTHER = 0;
    const TYPE_REGISTER = 1;
    const TYPE_SHARE_ITEM = 2;
    const TYPE_SHARE_TOPIC = 3;
    const TYPE_LUCKY_DRAW_AWARD = 4;
    const TYPE_SIGN_IN = 5;
    const TYPE_COUPON_CARD_TOP_UP = 6;
    const TYPE_EXCHANGE_ITEM = 7;//兑换商品
    const TYPE_REGISTER_BY_QR_CODE = 8;
    const TYPE_SHARE_STORE_ACTIVITY = 9;
    const TYPE_SHARE_AGENT_STORE = 10;
    const TYPE_USER_RED_PACKET = 11;

    public static $typeTexts = [
        self::TYPE_OTHER =>['name'=> '其他', 'value' => 10], //一般是特殊临时性质
        self::TYPE_REGISTER =>['name'=>  '新用户注册', 'value' => 10],
        self::TYPE_SHARE_ITEM =>['name'=>  '分享商品', 'value' => 10],
        self::TYPE_SHARE_TOPIC =>['name'=>  '分享话题', 'value' => 10],
        self::TYPE_LUCKY_DRAW_AWARD =>['name'=> '进行抽奖', 'value' => -10],
        self::TYPE_SIGN_IN =>['name'=> '签到奖励', 'value' => 10],
        self::TYPE_COUPON_CARD_TOP_UP =>['name'=> '充值卡充值', 'value' => 0],
        self::TYPE_EXCHANGE_ITEM =>['name'=> '兑换优惠劵', 'value' => 0],
        self::TYPE_REGISTER_BY_QR_CODE =>['name'=> '扫码注册', 'value' => 10],
        self::TYPE_SHARE_STORE_ACTIVITY =>['name'=> '分享店铺活动', 'value' => 10],
        self::TYPE_SHARE_AGENT_STORE =>['name'=> '分享店铺', 'value' => 10],
        self::TYPE_USER_RED_PACKET =>['name'=> '领取红包', 'value' => 10],
    ];

    protected $fillable = ['uid', 'type', 'score', 'total', 'remarks', 'info'];
    protected $hidden = ['info'];
    protected $appends = ['couponcardno','name'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
           $total = $model->where("uid", $model->uid)->where('id', '<=', $model->id)->sum('score');
           $model->total = $total;
           $model->save();
           User::where('id', $model->uid)->update(['scores' => $total]);
        });
    }
    public function getCouponcardnoAttribute()
    {
        if(empty($this->attributes['info'])) return "";
        $info = \GuzzleHttp\json_decode($this->attributes['info'],true);
        return $info['coupon_card_pub_key'] ?? "";
    }
    public function getTypeTextAttribute()
    {
        return self::$typeTexts[$this->type] ? self::$typeTexts[$this->type]['name'] : '';
    }

    public function getNameAttribute()
    {
        $info = json_decode($this->info,true);
        if(!$info) return '';
        else
        {
            return $info['name'] ?? "";
        }
    }


    public function user()
    {
        return $this->belongsTo(User::class, "uid");
    }

    /**
     * 其他类型的积分发送，一般是不固定临时或者活动性质的
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     */
    public static function assignForOther($uid, $score, $remarks, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_OTHER,
            'score' => $score,
            'remarks' => $remarks,
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * @desc 领取红包
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     */
    public static function assignForRedPacket($uid, $score, $remarks, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_USER_RED_PACKET,
            'score' => $score,
            'remarks' => $remarks,
            'info' => json_encode($info, 336),
        ]);
    }
    /**
     * 兑换商品
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     */
    public static function assignForExchangeItem($uid, $score, $remarks, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_EXCHANGE_ITEM,
            'score' => -abs($score),
            'remarks' => $remarks,
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 用户新注册时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     */
    public static function assignForRegister($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_REGISTER,
            'score' => $score ?? Config::getRegisterScore() ?? self::$typeTexts[self::TYPE_REGISTER]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_REGISTER]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 分享商品时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     */
    public static function assignForShareItem($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_SHARE_ITEM,
            'score' => $score ?? Config::getShareItemScore() ?? self::$typeTexts[self::TYPE_SHARE_ITEM]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_SHARE_ITEM]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 分享帖子时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     *
     */
    public static function assignForShareTopic($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_SHARE_TOPIC,
            'score' => $score ?? Config::getShareTopicScore() ?? self::$typeTexts[self::TYPE_SHARE_TOPIC]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_SHARE_TOPIC]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 进行抽奖时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     *
     */
    public static function assignForLuckyDrawAward($uid, $score = null, $remarks = null, array $info = [])
    {
        $score = $score ?? self::$typeTexts[self::TYPE_LUCKY_DRAW_AWARD]['value'];
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_LUCKY_DRAW_AWARD,
            'score' => -abs($score),
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_LUCKY_DRAW_AWARD]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 签到时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     *
     */
    public static function assignForSignIn($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_SIGN_IN,
            'score' => $score ?? self::$typeTexts[self::TYPE_SIGN_IN]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_SIGN_IN]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 充值卡充值积分时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     *
     */
    public static function assignForCouponCardTopUp($uid, $score = null, $remarks = null, array $info = [])
    {
        return static::create([
            'uid' => $uid,
            'type' => self::TYPE_COUPON_CARD_TOP_UP,
            'score' => $score ?? self::$typeTexts[self::TYPE_COUPON_CARD_TOP_UP]['value'],
            'remarks' => $remarks ?? self::$typeTexts[self::TYPE_COUPON_CARD_TOP_UP]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    /**
     * 扫码注册时发送
     *
     * @param $uid
     * @param $score
     * @param $remarks
     * @param array $info
     * @return mixed
     *
     */
    public static function assignForRegisterByQrCode($uid, $score = null, $remarks = null, array $info = [])
    {
        $type = self::TYPE_REGISTER_BY_QR_CODE;
        return static::create([
            'uid' => $uid,
            'type' => $type,
            'score' => $score ?? Config::getRegisterByQrCodeScore() ?? self::$typeTexts[$type]['value'],
            'remarks' => $remarks ?? self::$typeTexts[$type]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    public static function assignForShareStoreActivity($uid, $score = null, $remarks = null, array $info = [])
    {
        $type = self::TYPE_SHARE_STORE_ACTIVITY;
        return static::create([
            'uid' => $uid,
            'type' => $type,
            'score' => $score ?? self::$typeTexts[$type]['value'],
            'remarks' => $remarks ?? self::$typeTexts[$type]['name'],
            'info' => json_encode($info, 336),
        ]);
    }

    public static function assignForShareAgentStore($uid, $score = null, $remarks = null, array $info = [])
    {
        $type = self::TYPE_SHARE_AGENT_STORE;
        return static::create([
            'uid' => $uid,
            'type' => $type,
            'score' => $score ?? self::$typeTexts[$type]['value'],
            'remarks' => $remarks ?? self::$typeTexts[$type]['name'],
            'info' => json_encode($info, 336),
        ]);
    }
}

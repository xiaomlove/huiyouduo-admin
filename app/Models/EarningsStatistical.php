<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EarningsStatistical extends Model
{
    protected $table = "earnings_statistical";
    protected $fillable = [
        "day",
        "income",
        "spending",
        "remark",
    ];

}

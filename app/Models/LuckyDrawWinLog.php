<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class LuckyDrawWinLog extends Model
{
    protected $fillable = ['uid', 'lucky_draw_id', 'lucky_draw_prize_id','is_verify_take_prize'];

    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }

    public function luckyDraw()
    {
        return $this->belongsTo(LuckyDraw::class, 'lucky_draw_id');
    }

    public function luckyDrawPrize()
    {
        return $this->belongsTo(LuckyDrawPrize::class, 'lucky_draw_prize_id');
    }
}

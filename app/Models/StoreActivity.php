<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreActivity extends Model
{
    protected $fillable = ['title', 'cover', 'tid', 'store_id','act_desc'];

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'tid');
    }

    public function store()
    {
        return $this->belongsTo(AgentStore::class, 'store_id');
    }

    public function shareLogs()
    {
        return $this->morphMany(ShareLog::class, 'target');
    }
}

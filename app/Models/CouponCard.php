<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class CouponCard extends Model
{
    protected $table = 'coupon_card';

    protected $fillable = [
        'coupon_card_no',
        'coupon_card_pub_key',
        'coupon_card_pri_key',
        'first_class_agent_id',
        'second_class_agent_id',
        'points_quota',
        'is_used',
        'bind_uid',
        'used_at'
    ];

    public static function checkValidCardNo($card_start_no = '',$card_end_no = '',$agent_id = 0)
    {
        $ret = false;$message = "";
        if(empty($card_start_no) || empty($card_end_no))
        {
            $message = "优惠卡编号不能为空";
            return [$ret,$message];
        }
        if(mb_strlen($card_start_no) != 13 || mb_strlen($card_end_no) != 13)
        {
            $message = '优惠卡编号格式不正确';
            return [$ret,$message];
        }
        $int_card_start_no = (int)$card_start_no;
        $int_card_end_no = (int)$card_end_no;
        if($int_card_start_no > $int_card_end_no)
        {
            $message = '优惠卡开始编号应小于结束编号';
            return [$ret,$message];
        }
        if($int_card_end_no - $int_card_start_no > 5000)
        {
            $message = '为了不影响平台性能，一次性最多只能分配5000个优惠卡';
            return [$ret,$message];
        }
        //检测是否在数据库中，并且未被激活使用的
        $card_no_count = CouponCard::where(function($query) use ($card_start_no,$card_end_no) {
            $query->where('coupon_card_no', $card_start_no);
            $query->orWhere('coupon_card_no',$card_end_no);
        })->where(['first_class_agent_id'=>0,'second_class_agent_id'=>0])->count();
        if($card_no_count < 1)
        {
            $message = '优惠卡开始编号或者结束标号不存在';
            return [$ret,$message];
        }
        //批次编号连续性检测
        $constant_count = CouponCard::whereBetween("coupon_card_no",[$card_start_no,$card_end_no])
            ->where(['first_class_agent_id'=>0,'second_class_agent_id'=>0])->count();
        if($constant_count != $int_card_end_no - $int_card_start_no + 1)
        {
            $message = '请填写连续的未分配过的编号，可充值卡管理查看';
            return [$ret,$message];
        }

        $ret = true;
        return [$ret,$message];
    }

    /**
     * @desc 一级 代理商给二级代理商分配优惠券时，只能分配自己所属的优惠卡
     * @param string $card_start_no
     * @param string $card_end_no
     */
    public static function checkValidCardNoAgent($card_start_no = '',$card_end_no = '',$agent_id = 0)
    {
        $ret = false;$message = "";
        if(empty($card_start_no) || empty($card_end_no))
        {
            $message = "优惠卡编号不能为空";
            return [$ret,$message];
        }
        if(mb_strlen($card_start_no) != 13 || mb_strlen($card_end_no) != 13)
        {
            $message = '优惠卡编号格式不正确';
            return [$ret,$message];
        }
        $int_card_start_no = (int)$card_start_no;
        $int_card_end_no = (int)$card_end_no;
        if($int_card_start_no > $int_card_end_no)
        {
            $message = '优惠卡开始编号应小于结束编号';
            return [$ret,$message];
        }
        if($int_card_end_no - $int_card_start_no > 5000)
        {
            $message = '为了不影响平台性能，一次性最多只能分配5000个优惠卡';
            return [$ret,$message];
        }
        //权限检测
        $cur_login_admin_map_agent_id = \Admin::user()->agent()->first()->id;
        $is_my_children_agent = Agent::where(['id'=>$agent_id,'parent_id'=>$cur_login_admin_map_agent_id])->exists();
        if(!$is_my_children_agent)
        {
            $message = "权限不足";
            return [$ret,$message];
        }
        //检测是否在数据库中，并且未被激活使用的
        $card_no_count = CouponCard::where(function($query) use ($card_start_no,$card_end_no,$cur_login_admin_map_agent_id) {
            $query->where('coupon_card_no', $card_start_no);
            $query->orWhere('coupon_card_no',$card_end_no);
        })->where(['first_class_agent_id'=>$cur_login_admin_map_agent_id,'second_class_agent_id'=>0])->count();
        if($card_no_count < 1)
        {
            $message = '优惠卡开始编号或者结束标号不存在';
            return [$ret,$message];
        }

        //批次编号连续性检测
        $constant_count = CouponCard::whereBetween("coupon_card_no",[$card_start_no,$card_end_no])
            ->where(['first_class_agent_id'=>$cur_login_admin_map_agent_id,'second_class_agent_id'=>0])->count();
        if($constant_count != $int_card_end_no - $int_card_start_no + 1)
        {
            $message = '请填写连续的未分配过的编号，可充值卡管理查看';
            return [$ret,$message];
        }

        $ret = true;
        return [$ret,$message];
    }

    /**
     * 一个用户可以有多个优惠卡，一个优惠卡对应一个用户
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function belongsToUser()
    {
        return $this->belongsTo(User::class, "bind_uid");
    }

    /**
     * 一个卡对应一个一级代理商  一个一级代理商可以有多个卡
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function belongsToLevelOneAgent()
    {
        return $this->belongsTo(Agent::class, "first_class_agent_id");
    }

    /**
     * 一个卡对应一个二级代理商  一个二级代理商可以有多个卡
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function belongsToLevelTwoAgent()
    {
        return $this->belongsTo(Agent::class, "second_class_agent_id");
    }

}

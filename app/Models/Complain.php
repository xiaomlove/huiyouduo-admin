<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    const STATUS_INIT = 0;
    const STATUS_RESOLVED = 1;

    public static $statusText = [
        self::STATUS_INIT => '未处理',
        self::STATUS_RESOLVED => '已处理',
    ];

    protected $casts = [
        'images' => 'json',
    ];

    protected $fillable = ['uid', 'target_id', 'target_type', 'status', 'content', 'images'];

    public static $targetTypeNames = [
        Topic::class => '帖子',
        Comment::class => '评论',
    ];


    public function getTargetTypeNameAttribute()
    {
        return self::$targetTypeNames[$this->target_type] ?? '';
    }

    public function getStatusTextAttribute()
    {
        return self::$statusText[$this->status] ?? '';
    }

    public function target()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }


}

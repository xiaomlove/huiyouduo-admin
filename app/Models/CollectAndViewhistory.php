<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectAndViewhistory extends Model
{
    const COLLECT_LOG_TYPE = 1;
    const VIEW_HISTORY_LOG_TYPE = 2;

    protected $table = "collect_and_viewhistory_log";
    protected $fillable = [
        "uid",
        "item_id",
        "type",
    ];

}

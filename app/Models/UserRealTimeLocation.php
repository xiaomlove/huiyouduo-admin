<?php

namespace App\Models;

use App\Repositories\LBSRepository;
use Illuminate\Database\Eloquent\Model;

class UserRealTimeLocation extends Model
{
    use HasDistrict;

    protected $fillable = [
        'uid', 'ip', 'longitude', 'latitude',
        'province_code', 'city_code', 'district_code',
        'province_name', 'city_name', 'district_name',
    ];

    public static function updateByGeo($uid, $longitude, $latitude)
    {
        $logPrefix = sprintf('%s, uid: %s, longitude: %s, latitude: %s', __METHOD__, $uid, $longitude, $latitude);
        $location = (new LBSRepository())->getLocationInfoByGeo($latitude, $longitude);
        if ($location['status'] != 0) {
            \log::warning("$logPrefix, getLocationInfoByGeo() failed", $location);
            return false;
        }
        $adCode = $location['result']['addressComponent']['adcode'] ?? 0;
        $someCity = District::where('code', $adCode)->first();
        //判断是城市还是区
        $hasDistrict = $someCity->districts()->count();
        $city = $district = null;
        if ($hasDistrict) {
            $city = $someCity;
        } else {
            $district = $someCity;
            $city = $district->city;
        }
        $province = $city->province;

        $data = [
            'uid' => $uid,
            'city_code' => $city->code,
            'city_name' => $city->name,
            'province_name' => $province->name,
            'province_code' => $province->code,
            'longitude' => $location['result']['location']['lng'],
            'latitude' => $location['result']['location']['lat'],
        ];
        if ($district) {
            $data['district_name'] = $district->name;
            $data['district_code'] = $district->code;
        }

        return static::create($data);
    }

    public static function updateByIp($uid, $ip)
    {
        $logPrefix = sprintf('%s, uid: %s, ip: %s', __METHOD__, $uid, $ip);
        $location = (new LBSRepository())->getLocationInfoByIp($ip);
        if ($location['status'] != 0) {
            \log::warning("$logPrefix, getLocationInfoByIp() failed", $location);
            return false;
        }
        return static::create([
            'uid' => $uid,
            'province_name' => $location['content']['address_detail']['province'],
            'city_name' => $location['content']['address_detail']['city'],
            'district_name' => $location['content']['address_detail']['district'],
            'longitude' => $location['point']['x'] / 10000,
            'latitude' => $location['point']['y'] / 10000,
        ]);
    }
}

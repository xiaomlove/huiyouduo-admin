<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserFinanceAccount extends Model
{
    const TYPE_ALIPAY = 'Alipay';
    const TYPE_BC = 'BC';
    const TYPE_ABC = 'ABC';
    const TYPE_ICBC = 'ICBC';
    const TYPE_CMSB = 'CMSB';
    const TYPE_CIB = 'CIB';
    const TYPE_CDB = 'CDB';
    const TYPE_BCCB = 'BCCB';
    const TYPE_HSCB = 'HSCB';
    const TYPE_PBC = 'PBC';
    const TYPE_CEB = 'CEB';
    const TYPE_BOC = 'BOC';
    const TYPE_CCB = 'CCB';
    const TYPE_CMB = 'CMB';
    const TYPE_BCM = 'BCM';

    public static $typeNames = [
        self::TYPE_ALIPAY => '支付宝',
        self::TYPE_BC => '中国银行',
        self::TYPE_ABC => '中国农业银行',
        self::TYPE_ICBC => '中国工商银行',
        self::TYPE_CMSB => '民生银行',
        self::TYPE_CIB => '兴业银行',
        self::TYPE_CDB => '国家开发银行',
        self::TYPE_BCCB => '北京市商业银行',
        self::TYPE_HSCB => '汇丰银行',
        self::TYPE_PBC => '中国人民银行',
        self::TYPE_CEB => '中国光大银行',
        self::TYPE_BOC => '中国银行',
        self::TYPE_CCB => '中国建设银行',
        self::TYPE_CMB => '中国招商银行',
        self::TYPE_BCM => '交通银行',

    ];

    protected $fillable = ['uid', 'type', 'account', 'bind_username', 'bind_organization', 'bind_phone'];

    public function getTypeNameAttribute()
    {
        return self::$typeNames[$this->type] ?? '';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }
}

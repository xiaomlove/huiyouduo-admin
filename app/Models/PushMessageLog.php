<?php

namespace App\Models;

use Earnp\Getui\Getui;
use Illuminate\Database\Eloquent\Model;

class PushMessageLog extends Model
{
    const PUSH_ITEM_TYPE = 1;
    const PUSH_ACT_TYPE = 2;
    protected $table = "push_message_log";
    protected $fillable = [
        "push_type",
        "push_params_id",
        "push_province_code",
        "push_province_name",
        "push_title",
        "push_message",
        "remark",
        "push_id",
    ];

    public function setPushProvinceCodeAttribute($push_province_code)
    {
        if (is_array($push_province_code)) {
            $this->attributes['push_province_code'] = join(",",$push_province_code);
        }
    }

    public function getPushProvinceCodeAttribute($push_province_code)
    {
        return explode(",",$push_province_code);
    }


    public static function ios_push($title,$content,$push_custome_message,$push_province_list)
    {
        if(empty($title) || empty($content)) return false;

        $template = "IGtTransmissionTemplate";
        // 发送内容
        $data = "ios透传";
        // 条件选择
        $choice = array(
            // 发送手机类型
            "phoneTypeList"=>["ANDROID","IPHONE","IPAD"],
            // 发送城市
            "provinceList"=>$push_province_list,
            // 标签选择，为空即可
            "tagList"=>[],
            // 年龄选择
            "age"=>[]
        );

        $config = array(
            "type" => "HIGH",
            "title" => $title,
            "body" => $content,
            "logo"=>"downloadapp.png",
            "logourl"=>"http://huiyouduoapp.com/downloadapp.png"
        );
        $push_result = Getui::pushMessageToApp($template,$config,$data,$choice);
        if(isset($push_result['result']) && $push_result['result'] == 'ok') return true;
        else return false;
    }

    public static function android_push($title,$content,$push_custome_message,$push_province_list)
    {
        // 模版选择
        $template = "IGtNotificationTemplate";
        // 发送内容
        $data = "安卓推送";
        // 条件选择
        $choice = array(
            // 发送手机类型
            "phoneTypeList"=>["ANDROID","IPHONE","IPAD"],
            // 发送城市
            "provinceList"=>$push_province_list,
            // 标签选择，为空即可
            "tagList"=>[],
            // 年龄选择
            "age"=>[]
        );
        $config = array(
            "type" => "HIGH",
            "title" => $title,
            "body" => $content,
            "logo"=>"downloadapp.png",
            "logourl"=>"http://huiyouduoapp.com/downloadapp.png"
        );
        $push_result = Getui::pushMessageToApp($template,$config,$data,$choice);
        if(isset($push_result['result']) && $push_result['result'] == 'ok') return true;
        else return false;
    }

    protected static function boot()
    {
        set_time_limit(0);
        parent::boot();
        // 监听  的创建事件，
        static::creating(function (PushMessageLog $pushMessageLog) {

            $explode_push_province_code = $pushMessageLog->push_province_code;

            $push_province_list = [];
            $push_province_name = [];
            if(!in_array(0,$explode_push_province_code))
            {
                //定向省份
                foreach ($explode_push_province_code as $k=>$v)
                {
                    $distrct_name = District::where("code",$v)->first()->name;
                    array_push($push_province_name,$distrct_name);
                    $explode_push_province_code[$k] = $v."00";
                }
                $push_province_list = $explode_push_province_code;
            }
            else
            {
                $pushMessageLog->push_province_code = [];
            }

            $push_province_name = join(",",$push_province_name);

            $pushMessageLog->push_province_name = $push_province_name;

            $push_custome_message = "payload";

            if($pushMessageLog->push_type == self::PUSH_ITEM_TYPE)
                $push_custome_message = "/gDetail/{$pushMessageLog->push_params_id}";
            elseif ($pushMessageLog->push_type == self::PUSH_ACT_TYPE)
                $push_custome_message =  "/gDetail/{$pushMessageLog->push_params_id}";

            self::ios_push($pushMessageLog->push_title,$pushMessageLog->push_message,$push_custome_message,$push_province_list);
            //self::android_push($pushMessageLog->push_title,$pushMessageLog->push_message,$push_custome_message,$push_province_list);
        });
    }
}

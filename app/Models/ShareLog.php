<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ShareLog extends Model
{
    protected $fillable = ['uid', 'target_type', 'target_id'];

    public static $targetTypes = [
        StoreActivity::class => '店铺活动',
        Topic::class => '帖子',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }

    public function target()
    {
        return $this->morphTo();
    }

}

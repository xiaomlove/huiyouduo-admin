<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LastmonthCaijiLog extends Model
{
    protected $table = "lastmonthcaijilog";
    protected $fillable = [
        "caiji_date",
        "nums",
    ];
}

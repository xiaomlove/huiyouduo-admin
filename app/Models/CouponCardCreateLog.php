<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Redis;

class CouponCardCreateLog extends Model
{
    protected $table = 'coupon_card_create_log';

    protected $fillable = [
        'points_quota_input',
        'create_nums_input',
        'create_admin_uid',
        'create_card_start',
        'create_card_end',
        'name',
        'coupon_card_no_prefix',
    ];



    public function belongsToAdmin()
    {
        return $this->belongsTo(Admin::class, "create_admin_uid");
    }

//    public static function boot()
//    {
//        parent::boot();
//
//        static::creating(function ($model)
//        {
//            $name = $model->name;
//            $coupon_card_no_prefix = $model->coupon_card_no_prefix;
//            $points_quota_input = (int)$model->points_quota_input;
//            $create_nums_input = (int)$model->create_nums_input;
//            $model->create_admin_uid = \Admin::user()->id;
//            if($points_quota_input <= 0 || $create_nums_input <= 0)
//            {
//                admin_toastr(trans('admin.failed'),'请填写正确的积分额度和批量创建数量');
//                return redirect(admin_base_path("couponCardCreateLog"));
//            }
//            if($create_nums_input > 5000)
//            {
//                admin_toastr(trans('admin.failed'),'为了不影响平台性能，一次性最多只能创建5000个优惠卡');
//                return redirect(admin_base_path("couponCardCreateLog"));
//            }
//            $coupon_card_no_redis_key = env('COUPON_CARD_NO_REDIS_KEY');
//            $coupon_card_pub_redis_key = env('COUPON_CARD_PUB_KEY_REDIS_KEY');
//
//            $to_rem_card_no_redis_list = Redis::zrange($coupon_card_no_redis_key,0,$create_nums_input - 1);
//            echo '<pre>';
//            print_r($to_rem_card_no_redis_list);die;
//
//            if(empty($to_rem_card_no_redis_list))
//            {
//                admin_toastr(trans('admin.failed'),'请于定时任务菜单栏中执行generate:manycounpons');
//                return redirect(admin_base_path("couponCardCreateLog"));
//            }
//            Redis::zrem($coupon_card_no_redis_key,...$to_rem_card_no_redis_list);
//            $coupon_card_pub_list = [];
//            for($i=1;$i<=$create_nums_input;$i++)
//            {
//                array_push($coupon_card_pub_list,Redis::spop($coupon_card_pub_redis_key));
//            }
//            if(count($coupon_card_pub_list) < $create_nums_input)
//            {
//                admin_toastr(trans('admin.failed'),'请于定时任务菜单栏中执行generate:manycounpons');
//                return redirect(admin_base_path("couponCardCreateLog"));
//            }
//            $insert_all_list = [];
//            foreach ($to_rem_card_no_redis_list as $k=>$v)
//            {
//                $coupon_card_pri_key = str_random(6);
//                $insert_all_list[] = [
//                    'coupon_card_no'=>$coupon_card_no_prefix.$v,
//                    "name"=>$name,
//                    'coupon_card_pub_key'=>$coupon_card_pub_list[$k],
//                    "coupon_card_pri_key"=>$coupon_card_pri_key,
//                    "points_quota"=>$points_quota_input,
//                    'created_at'=>date('Y-m-d H:i:s'),
//                    'updated_at'=>date('Y-m-d H:i:s'),
//                ];
//            }
//            $create = CouponCard::insert($insert_all_list);
//            if($create)
//            {
//                $model->create_card_start = $to_rem_card_no_redis_list[0];
//                $model->create_card_end = $to_rem_card_no_redis_list[count($to_rem_card_no_redis_list) - 1];
//            }
//        });
//    }
}

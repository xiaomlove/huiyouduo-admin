<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Request;

class CouponCardCreateLogDetail extends Model
{
    public function paginate()
    {
        $perPage = Request::get('per_page', 10);

        $page = Request::get('page', 1);

        $log_id = Request::get('log_id',1);

        $start = ($page-1)*$perPage;

        $data = CouponCardCreateLog::find($log_id) ? CouponCardCreateLog::find($log_id)->card_sets : [];

        if(!empty($data)) $data = json_decode($data, true);

        $model_data = [];

        if(!empty($data))
        {
            $coupon_card_no_list_slice = array_slice($data['coupon_card_no_list'],$start,$perPage);
            $coupon_card_pub_list_slice = array_slice($data['coupon_card_pub_list'],$start,$perPage);
            $coupon_card_pri_list_slice = array_slice($data['coupon_card_pri_list'],$start,$perPage);
            $second_class_agent_ids_slice = array_slice($data['second_class_agent_ids'],$start,$perPage);

            foreach ($coupon_card_no_list_slice as $k=>$v)
            {
                $model_data[] = [
                    'card_no'=>$v,
                    'card_pub'=>$coupon_card_pub_list_slice[$k],
                    'card_pri'=>$coupon_card_pri_list_slice[$k],
                    'first_class_agent_id'=>$data['first_class_agent_id'],
                    'second_class_agent_id'=>$second_class_agent_ids_slice[$k],
                    'created_at'=>CouponCardCreateLog::find($log_id)->created_at,
                ];
            }

        }
        $total = count($data['coupon_card_no_list']);

        $datas = static::hydrate($model_data);

        $paginator = new LengthAwarePaginator($datas, $total, $perPage);

        $paginator->setPath(url()->current());

        return $paginator;
    }

    public static function with($relations)
    {
        return new static;
    }

    public function findOrFail($id)
    {
        $data = file_get_contents("http://api.douban.com/v2/movie/subject/$id");

        $data = json_decode($data, true);

        return static::newFromBuilder($data);
    }

    public function save(array $options = [])
    {
        dd($this->getAttributes());
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ItemsCate extends Model
{
    use SoftDeletes,AdminBuilder, ModelTree {
        ModelTree::boot as treeBoot;
    }

    const RECOMMEND_CATE = [
        '美妆','女装','男装','家居',
        '数码','鞋包','内衣',
        '运动','食品','母婴'
    ];
    const RECOMMEND_CATEIMAGE = [
        '美妆'=>'https://img.alicdn.com/imgextra/i1/2053469401/TB2QybZHL1TBuNjy0FjXXajyXXa-2053469401.png',
        '女装'=>'https://img.alicdn.com/imgextra/i1/2053469401/TB2oX82HL9TBuNjy0FcXXbeiFXa-2053469401.png',
        '男装'=>'https://img.alicdn.com/imgextra/i1/2053469401/TB2n7WyHN9YBuNjy0FfXXXIsVXa-2053469401.png',
        '家居'=>'https://img.alicdn.com/imgextra/i3/2053469401/TB2FCJnHFGWBuNjy0FbXXb4sXXa-2053469401.png',
        '数码'=>'https://img.alicdn.com/imgextra/i3/2053469401/TB230SXHL9TBuNjy1zbXXXpepXa-2053469401.png',
        '鞋包'=>'https://img.alicdn.com/imgextra/i1/2053469401/TB2g79yHN9YBuNjy0FfXXXIsVXa-2053469401.png',
        '内衣'=>'https://img.alicdn.com/imgextra/i3/2053469401/TB2cnZWzfiSBuNkSnhJXXbDcpXa-2053469401.png',
        '运动'=>'https://img.alicdn.com/imgextra/i4/2053469401/TB2rrnsHH9YBuNjy0FgXXcxcXXa-2053469401.png',
        '食品'=>'https://img.alicdn.com/imgextra/i4/2053469401/TB2PFV2HL9TBuNjy0FcXXbeiFXa-2053469401.png',
        '母婴'=>'https://img.alicdn.com/imgextra/i4/2053469401/TB2QMv0ay6guuRjy1XdXXaAwpXa-2053469401.png',
    ];
    protected $table = "items_cate";
    protected $fillable = [
        "title",
        "cate_img",
        "parent_id",
        "order",
    ];

    public function getCateImgAttribute()
    {
        if(empty($this->attributes['cate_img'])) return "";
        // 如果 image 字段本身就已经是完整的 url 就直接返回
        if (Str::startsWith($this->attributes['cate_img'], ['http://', 'https://'])) {
            return $this->attributes['cate_img'];
        }
        return \Storage::disk('public')->url($this->attributes['cate_img']);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushMessageState extends Model
{
    protected $table = "push_message_state";
    protected $fillable = [
        "message_id",
        "is_read",
        "uid",
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFeedBack extends Model
{
    protected $table = "user_feedback";
    protected $fillable = [
        "title",
        "contents",
        "is_audit",
    ];
}

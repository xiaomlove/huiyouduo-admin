<?php

namespace App\Models;

use Encore\Admin\Auth\Database\Menu as AdminMenu;

class Menu extends AdminMenu
{
    /**
     * 默认菜单，要在哪个平台出现，roles 填写上对应平台角色。
     * 若是要限定代理商级别，写那个级别的即可。
     * 为空会添加到官方平台上
     *
     * @var array
     */
    public static $defaults = [
        [
            'title' => '轮播',
            'icon' => 'fa-bars',
            'uri' => 'banner',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '商品分类',
            'icon' => 'fa-certificate',
            'uri' => 'itemscate',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '代理商',
            'icon' => 'fa-google',
            'uri' => 'vendor',
            'roles' => [Role::SLUG_OFFICIAL, Role::SLUG_AGENT_LEVEL_1], //一级代理商和官方人员能看
        ],
        [
            'title' => '店铺',
            'icon' => 'fa-google',
            'uri' => 'store',
            'roles' => [Role::SLUG_OFFICIAL, Role::SLUG_AGENT],
        ],
        [
            'title' => '帖子',
            'icon' => 'fa-google',
            'uri' => 'topic',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '举报',
            'icon' => 'fa-google',
            'uri' => 'complain',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '用户列表',
            'icon' => 'fa-users',
            'uri' => 'user',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '积分记录',
            'icon' => 'fa-bars',
            'uri' => 'score',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '积分赠送',
            'icon' => 'fa-bars',
            'uri' => 'user-low-score-send',
            'roles' => [Role::SLUG_OFFICIAL],
        ],

        [
            'title' => '充值卡管理',
            'icon' => 'fa-bars',
            'uri' => 'couponCard',
            'roles' => [Role::SLUG_OFFICIAL, Role::SLUG_AGENT],
        ],
        [
            'title' => '充值卡创建记录',
            'icon' => 'fa-bars',
            'uri' => 'couponCardCreateLog',
            'roles' => [Role::SLUG_OFFICIAL], //
        ],
        [
            'title' => '充值卡分配记录',
            'icon' => 'fa-bars',
            'uri' => 'couponCardAssignLog',
            'roles' => [Role::SLUG_OFFICIAL, Role::SLUG_AGENT_LEVEL_1], //
        ],
        [
            'title' => '订单列表',
            'icon' => 'fa-bars',
            'uri' => 'tbkOficalOrder',
            'roles' => [Role::SLUG_OFFICIAL, Role::SLUG_AGENT_LEVEL_1, Role::SLUG_AGENT_LEVEL_2],
        ],
        [
            'title' => '订单统计',
            'icon' => 'fa-bars',
            'uri' => 'order-statistical',
            'roles' => [Role::SLUG_OFFICIAL, Role::SLUG_AGENT_LEVEL_1, Role::SLUG_AGENT_LEVEL_2],
        ],
        [
            'title' => '平台收益统计',
            'icon' => 'fa-bars',
            'uri' => 'earnings-statistical',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '代理商收益统计',
            'icon' => 'fa-bars',
            'uri' => 'agent-earnings-statistical',
            'roles' => [Role::SLUG_OFFICIAL,Role::SLUG_AGENT],
        ],
        [
            'title' => '代理商结算',
            'icon' => 'fa-bars',
            'uri' => 'agents-settle',
            'roles' => [Role::SLUG_OFFICIAL,Role::SLUG_AGENT],
        ],
        [
            'title' => '提现审核',
            'icon' => 'fa-bars',
            'uri' => 'withdrawal',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '抽奖管理',
            'icon' => 'fa-anchor',
            'uri' => 'lucky-draw',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '官方通知',
            'icon' => 'fa-anchor',
            'uri' => 'officalMessage',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '消息推送',
            'icon' => 'fa-anchor',
            'uri' => 'push-message-log',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '定时任务',
            'icon' => 'fa-anchor',
            'uri' => 'scheduling',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '全局配置',
            'icon' => 'fa-anchor',
            'uri' => 'config',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
        [
            'title' => '工具',
            'icon' => 'fa-anchor',
            'uri' => 'tool',
            'roles' => [Role::SLUG_OFFICIAL],
        ],
    ];
}

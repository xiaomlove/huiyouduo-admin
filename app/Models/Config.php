<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    const NAME_REGISTER_SCORE = 'register_score';
//    const NAME_SHARE_ITEM_SCORE = 'share_item_score';
    const NAME_SHARE_TOPIC_SCORE = 'share_topic_score';
    const NAME_ABOUT = 'about';
    const NAME_REGISTER_BY_QR_CODE = 'register_by_qr_code';
    const NAME_USER_COMMISSION_PERCENT = 'user_commission_percent';
    const NAME_USER_LOW_SCORE_SEND = 'user_low_score_send';
    const NAME_USER_LOW_SCORE_SEND_NUMS = 'user_low_score_send_nums';
    const NAME_USER_RED_PACKET_NUMS = 'name_user_red_packet_nums';
    const NAME_OFFICE_MOBILE = 'office_mobile';
    const NAME_SIGN_SCORE_NUMS = 'sign_score_nums';

    protected $fillable = ['name', 'name_human', 'value'];

    public static $defaults = [
        self::NAME_REGISTER_SCORE => ['name'=> '注册赠送积分', 'value' => 10],
//        self::NAME_SHARE_ITEM_SCORE => ['name'=> '分享商品积分','value' => 10],
        self::NAME_SHARE_TOPIC_SCORE => ['name'=> '分享话题积分','value' => 10],
        self::NAME_ABOUT => ['name'=> '关于','value' => '这是一款集...于一体的app'],
        self::NAME_REGISTER_BY_QR_CODE => ['name'=> '扫码注册','value' => 10],
        self::NAME_USER_COMMISSION_PERCENT => ['name'=> '用户返佣百分比','value' => 10],
        self::NAME_USER_LOW_SCORE_SEND => ['name'=> '低于多少积分开始一键赠送','value' => 10],
        self::NAME_USER_LOW_SCORE_SEND_NUMS => ['name'=> '一键赠送多少积分','value' => 10],
        self::NAME_USER_RED_PACKET_NUMS => ['name'=> '红包赠送数量','value' => 10],
        self::NAME_OFFICE_MOBILE => ['name'=> '官方客服号码','value' => 15899986592],
        self::NAME_SIGN_SCORE_NUMS => ['name'=> '签到送积分','value' => 10],
    ];

    /**
     * 获取新用户注册所赠送的积分
     *
     * @return mixed
     */
    public static function getRegisterScore()
    {
        return optional((new static())->where('name', self::NAME_REGISTER_SCORE)->first())->value;
    }

    public static function getUserCommissionPercent()
    {
        return optional((new static())->where('name', self::NAME_USER_COMMISSION_PERCENT)->first())->value;
    }

    public static function getShareItemScore()
    {
        return optional((new static())->where('name', self::NAME_SHARE_ITEM_SCORE)->first())->value;
    }

    public static function getShareTopicScore()
    {
        return optional((new static())->where('name', self::NAME_SHARE_TOPIC_SCORE)->first())->value;
    }

    public static function getAbout()
    {
        return optional((new static())->where('name', self::NAME_ABOUT)->first())->value;
    }

    public static function getRegisterByQrCodeScore()
    {
        return optional((new static())->where('name', self::NAME_REGISTER_BY_QR_CODE)->first())->value;
    }

    public static function getUserLowScoreSend()
    {
        return optional((new static())->where('name', self::NAME_USER_LOW_SCORE_SEND)->first())->value;
    }
    public static function getUserLowScoreSendNums()
    {
        return optional((new static())->where('name', self::NAME_USER_LOW_SCORE_SEND_NUMS)->first())->value;
    }
    public static function getUserRedPacketNums()
    {
        return optional((new static())->where('name', self::NAME_USER_RED_PACKET_NUMS)->first())->value;
    }
    public static function getOfficeMobile()
    {
        return optional((new static())->where('name', self::NAME_OFFICE_MOBILE)->first())->value;
    }
    public static function getSignScoreNums()
    {
        return optional((new static())->where('name', self::NAME_SIGN_SCORE_NUMS)->first())->value;
    }
}

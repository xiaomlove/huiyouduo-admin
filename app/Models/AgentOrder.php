<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Request;
use App\Models\TbkOficalOrder;
use App\Libraries\Taobao;
use App\Models\TaobaoPids;
use App\User;

class AgentOrder extends Model
{
    public function paginate()
    {
        $perPage = Request::get('per_page', 10);

        $page = Request::get('page', 1);
        //如果是代理商管理员登陆
        $user = User::all()->toArray();
        $cur_agent_admin = \Admin::user();
        $agents_admin_front_uid = $cur_agent_admin->front_uid;
        $model_data = subtree($user,$agents_admin_front_uid);
        $agents_order = [];
        foreach ($model_data as $kk=>$vv)
        {
            $agents_order = array_merge($agents_order,$this->user_order($vv['id'],$vv['tb_uid']));
        }
        $model_data = $agents_order;

        $total = count($model_data);

        $datas = static::hydrate($model_data);

        $paginator = new LengthAwarePaginator($datas, $total, $perPage);

        $paginator->setPath(url()->current());

        return $paginator;
    }

    public static function with($relations)
    {
        return new static;
    }

    public function findOrFail($id)
    {
        $data = file_get_contents("http://api.douban.com/v2/movie/subject/$id");

        $data = json_decode($data, true);

        return static::newFromBuilder($data);
    }

    public function save(array $options = [])
    {
        dd($this->getAttributes());
    }

    /**
     * @desc 用户的订单列表
     * @param int $uid
     * @return array
     */
    protected function user_order($uid = 0,$tb_uid = 0)
    {
        if(empty($uid)) return;
        $user_order_list = [];
        if(!empty($tb_uid))
        {
            //这个是怎么筛选订单的 uid  和 订单 后6位 匹配
            $user_order_list = TbkOficalOrder::all()->toArray();
            if(!empty($user_order_list))
            {
                foreach ($user_order_list as $k=>$v)
                {
                    if($tb_uid != handle_tbk_trade_parent_id($v['trade_parent_id']))
                    {
                        unset($user_order_list[$k]);
                    }
                }
            }
        }
        else
        {
            //这种是绑定pid的用户
            $tb_pid_info = TaobaoPids::where("uid",$uid)->first();
            if($tb_pid_info)
            {
                $adzone_id = $tb_pid_info->adzone_id;
                $site_id = $tb_pid_info->site_id;
                $user_order_list = TbkOficalOrder::where([
                    "adzone_id"=>$adzone_id,
                    "site_id"=>$site_id,
                ])->get()->toArray();//commission
            }
        }
        return $user_order_list;
    }

}
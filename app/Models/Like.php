<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
    use SoftDeletes;

    const TARGET_TYPE_COMMENT = 1;

    protected $fillable = ['uid'];

    public function user()
    {
        return $this->belongsTo(User::class, "uid");
    }

    public function target()
    {
        return $this->morphTo();
    }
}

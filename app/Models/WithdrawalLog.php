<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class WithdrawalLog extends Model
{
    const CHANNEL_FRONT = 1;
    const CHANNEL_AGENT = 2;

    public static $channelNames = [
        self::CHANNEL_FRONT => '前端用户申请',
        self::CHANNEL_AGENT => '代理商申请',
    ];

    const STATUS_INIT = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAIL = 3;

    public static $statusText = [
        self::STATUS_INIT => '未处理',
        self::STATUS_PROCESSING => '处理中',
        self::STATUS_SUCCESS => '成功',
        self::STATUS_FAIL => '失败',
    ];

    protected $fillable = ['uid', 'channel', 'finance_account_id', 'money', 'status', 'trade_snapshot', 'out_biz_no', 'alipay_trade_no', 'paid_at'];

    public static function boot()
    {
        parent::boot();
        $logPrefix = __METHOD__;
        static::creating(function ($model) use ($logPrefix) {
            //钱数，前端单位元显示，后端存储转变为单位分
            $originMoney = $model->money;
            \Log::info("$logPrefix -> creating", ['originMoney' => $originMoney]);
            $model->money = floor($originMoney * 100);
        });

        static::created(function ($model) use ($logPrefix) {
            $model->out_biz_no = sprintf('WDA%s%s', date('YmdHis'), str_pad($model->id, 8, 0, STR_PAD_LEFT));
            $model->save();
            User::updateMoney($model->uid);
        });

        static::updated(function ($model) use ($logPrefix) {
            if ($model->isDirty('status')) {
                \Log::info("$logPrefix, {$model->id} status changed, going to update user({$model->uid}) money...");
                User::updateMoney($model->uid);
            }
        });
    }

    public function getStatusTextAttribute()
    {
        return self::$statusText[$this->status] ?? '';
    }

    public function getChannelNameAttribute()
    {
        return self::$channelNames[$this->channel] ?? '';
    }

    public function scopeChannelFront($query)
    {
        return $query->where('channel', self::CHANNEL_FRONT);
    }

    public function scopeChannelAgent($query)
    {
        return $query->where('channel', self::CHANNEL_AGENT);
    }

    public function scopeStatusInit($query)
    {
        return $query->where('status', self::STATUS_INIT);
    }

    public function scopeStatusProcessing($query)
    {
        return $query->where('status', self::STATUS_PROCESSING);
    }

    public function scopeStatusSuccess($query)
    {
        return $query->where('status', self::STATUS_SUCCESS);
    }

    public function scopeStatusFail($query)
    {
        return $query->where('status', self::STATUS_FAIL);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }

    public function financeAccount()
    {
        return $this->belongsTo(UserFinanceAccount::class, 'finance_account_id');
    }

    /**
     * 当提现成功处理时，会产生用户资金流水
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function userMoneyWaterLogs()
    {
        return $this->morphMany(UserMoneyWaterLog::class, 'source');
    }



}

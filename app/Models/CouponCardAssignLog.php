<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CouponCardAssignLog extends Model
{
    protected $table = "coupon_card_assign_log";
    protected $fillable = [
        "card_no",
        "assign_admin_uid",
        "assign_admin_at",
        "remark",
    ];
    /**
     * 对应后台用户
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignAdmin()
    {
        return $this->belongsTo(Admin::class, "assign_admin_uid");
    }

}

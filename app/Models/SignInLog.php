<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class SignInLog extends Model
{
    protected $fillable = ['uid'];

    public function user()
    {
        return $this->belongsTo(User::class, 'uid');
    }
}

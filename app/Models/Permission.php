<?php

namespace App\Models;

use Encore\Admin\Auth\Database\Permission as AdminPermission;

class Permission extends AdminPermission
{
    //只添加一些代理商的权限
    const SLUG_BANNER_MANAGEMENT = 'banner_management';
    const SLUG_GOODS_MANAGEMENT = 'goods_management';
    const SLUG_GOODS_CATEGORY_MANAGEMENT = 'goods_category_management';
    const SLUG_AGENT_MANAGEMENT = 'agent_management';
    const SLUG_COUPON_CARD_MANAGEMENT = 'coupon_card_management';
    //用户列表
    const SLUG_USER = 'user_management';
    //订单列表
    const SLUG_ORDER_LIST = 'order_list_management';
    //订单统计
    const SLUG_ORDER_STATISTICAL = 'order_statistical_management';
    //收益统计
    const SLUG_EARN_STATISTICAL = 'order_earn_management';
    //代理商结算
    const SLUG_AGENT_SETTLE = 'agent_settle_management';
    //充值卡分配记录
    const SLUG_COUPON_ASSIGN_CARD = 'agent_coupon_assign_management';
    //店铺
    const SLUG_STORE = 'agent_store_management';
    //根目录
    const SLUG_ROOT = 'agent_root_management';


    public static $defaults = [
        self::SLUG_BANNER_MANAGEMENT => ['name' => '首页轮播管理', 'http_path' => '/banner*'],
        self::SLUG_GOODS_MANAGEMENT => ['name' => '商品管理', 'http_path' => '/items*'],
        self::SLUG_GOODS_CATEGORY_MANAGEMENT => ['name' => '商品分类管理', 'http_path' => '/itemscate*'],
        self::SLUG_AGENT_MANAGEMENT => ['name' => '代理商管理', 'http_path' => '/vendor*'],
        self::SLUG_COUPON_CARD_MANAGEMENT => ['name' => '充值卡管理', 'http_path' => '/couponCard*'],

        self::SLUG_USER => ['name' => '用户列表', 'http_path' => '/user*'],
        self::SLUG_ORDER_LIST => ['name' => '订单列表', 'http_path' => '/tbkOficalOrder*'],
        self::SLUG_ORDER_STATISTICAL => ['name' => '订单统计', 'http_path' => '/order-statistical*'],
        self::SLUG_EARN_STATISTICAL => ['name' => '代理商收益统计', 'http_path' => '/agent-earnings-statistical*'],
        self::SLUG_AGENT_SETTLE => ['name' => '代理商结算', 'http_path' => '/agents-settle*'],
        self::SLUG_COUPON_ASSIGN_CARD => ['name' => '充值卡分配记录', 'http_path' => '/couponCardAssignLog*'],
        self::SLUG_STORE => ['name' => '店铺', 'http_path' => '/store*'],
        self::SLUG_ROOT => ['name' => '首页', 'http_path' => '/'],

    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentsSettle extends Model
{
    protected $table = "agents_settle";
    protected $fillable = [
        "day",
        "settle_total_price",
        "agent_admin_uid",
        "is_all_settled",
    ];
}

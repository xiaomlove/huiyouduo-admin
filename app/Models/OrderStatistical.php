<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Request;

class OrderStatistical extends Model
{
    protected $table = "order_statistical";

    protected $fillable = [
        'day',
        'count',
        'pay_price_count',
        'commission_count',
        'agent_admin_uid',
    ];
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $fillable = [
        'admin_uid', 'name', 'parent_id', 'level', 'remarks', 'commission_percent',
    ];

    /**
     * 对应后台用户
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class, "admin_uid");
    }

    /**
     * 对应父级代理商
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(__CLASS__, "parent_id");
    }

    /**
     * 代理商的门店
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stores()
    {
        return $this->hasMany(AgentStore::class, "agent_id");
    }

}

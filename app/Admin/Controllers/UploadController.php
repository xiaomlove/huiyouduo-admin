<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    protected $imageFieldName = 'image';

    protected $editor = 'wangEditor';

    public function image(Request $request)
    {
        $image = $request->file($this->imageFieldName);
        if (empty($image)) {
            return $this->failResponse("没有字段名为 {$this->imageFieldName} 的图片上传");
        }
        $result = $image->store('');

        if (!$result) {
            return $this->failResponse('图片保存失败');
        }

        $url = \Storage::url($result);

        return $this->successResponse($url);
    }

    private function successResponse($url)
    {
        switch ($this->editor) {
            case 'wangEditor':
                return response()->json(['errno' => 0, 'data' => [$url]]);
            default:
                return api('不支持的编辑器');
        }
    }

    private function failResponse($msg)
    {
        switch ($this->editor) {
            case 'wangEditor':
                return response()->json(['errno' => -1, 'data' => [$msg]]);
            default:
                return api('不支持的编辑器');
        }
    }
}
<?php

namespace App\Admin\Controllers;

use App\Models\District;
use App\Models\PushMessageLog;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class PushMessageLogController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new PushMessageLog);

        $grid->id('Id');
        $grid->push_type('推送类型')->display(function ($push_type){
            return $push_type == PushMessageLog::PUSH_ITEM_TYPE ? "商品推送" : "活动推送";
        });
        $grid->push_params_id('参数id');
//        $grid->push_province_name('Push province name');
        $grid->push_title('推送标题');
        $grid->push_message('推送内容');
        $grid->remark('推送备注');
//        $grid->push_id('Push id');
        $grid->created_at('推送时间');
//        $grid->updated_at('修改时间');

//        $grid->disableCreateButton();
        $grid->disableActions();


        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(PushMessageLog::findOrFail($id));

        $show->id('Id');
        $show->push_type('Push type');
        $show->push_params_id('Push params id');
        $show->push_province_name('Push province name');
        $show->push_title('Push title');
        $show->push_message('Push message');
        $show->remark('Remark');
        $show->push_id('Push id');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        set_time_limit(0);
        $form = new Form(new PushMessageLog);

        $form->select('push_type', '*推送类型')->options([1=>'商品推送',2=>'活动推送'])->rules("required");;
        $form->text('push_params_id', '*推送目标id')->help('商品推送填写商品id,活动推送填写活动id')->rules("required");;
        $form->multipleSelect('push_province_code', '*推送区域')
            ->options(District::where('parent_id',1)->pluck('name','code')->prepend('全国', 0));
        //$form->number('push_uid', 'Push uid')->default(-1);
        //$form->number('push_score', 'Push score');
        $form->hidden("push_province_name");
        $form->text('push_title', '*推送标题')->rules("required");;
        $form->textarea('push_message', '*推送内容')->rules("required");;
        $form->text('remark', '推送备注');
        //$form->text('push_id', 'Push id');

        return $form;
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\Admin;
use App\Models\Agent;
use App\Models\CouponCard;
use App\Models\CouponCardCreateLog;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\Redis;


class CouponCardCreateLogController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CouponCardCreateLog);

        $grid->id('Id');
        $grid->column('belongsToAdmin.name', '创建者');
        $grid->points_quota_input('积分额度');
        $grid->create_nums_input('创建数量');

        $grid->column('create_card_start_end','编号起始')->display(function (){
            return $this->create_card_start."--".$this->create_card_end;
            //return '<a  href="'.admin_base_path('couponCardCreateLogDetail').'?log_id='.$this->id.'" target="_blank">明细</a>';
        });

        $grid->created_at('创建时间');
        $grid->updated_at('更新时间');
        $grid->disableActions();
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        $grid->filter(function ($filter) {
            $filter->where(function ($query) {
                $query->whereHas("belongsToAdmin", function ($query) {
                    $query->where("create_admin_uid", $this->input);
                });
            }, "创建者", "belongsToAdmin")->select(Admin::pluck("name","id"));

            $filter->equal('points_quota_input',"积分额度");
            $filter->equal('create_nums_input',"创建数量");
        });

        return $grid;
    }

    /**
     * @desc get card_sets
     * @param $card_sets
     */
    protected function card_sets_datas($card_sets)
    {
        $card_sets = is_array($card_sets) ? $card_sets : json_decode($card_sets,true);
        $return_data = [];
        $coupon_card_no_list = $card_sets['coupon_card_no_list'];
        $coupon_card_pub_list = $card_sets['coupon_card_pub_list'];
        $coupon_card_pri_list = $card_sets['coupon_card_pri_list'];

        foreach ($coupon_card_no_list as $k=>$v)
        {
            $return_data[] = [
                $v,
                $coupon_card_pub_list[$k],
                $coupon_card_pri_list[$k]
            ];
        }
        return $return_data;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CouponCardCreateLog::findOrFail($id));

        $show->id('Id');
        $show->points_quota_input('Points quota input');
        $show->create_nums_input('Create nums input');
        $show->create_admin_uid('Create admin uid');
        $show->card_sets('Card sets');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        set_time_limit(0);
        $form = new Form(new CouponCardCreateLog);
        $form->text('name', '批次名称')->default('');
        $form->text('coupon_card_no_prefix', '卡批次前三位自定义')->default('000');
        $form->number('points_quota_input', '积分额度')->rules('required|numeric|min:1')->default(1);
        $form->number('create_nums_input', '创建数量')->rules('required|numeric|min:1')->default(1);
        $form->hidden("create_admin_uid","操作管理员uid")->value(\Admin::user()->id);
        $form->hidden("create_card_start","开始");
        $form->hidden("create_card_end","结束");

        $form->saving(function ($form){
            $model = $form;
            $name = $model->name;
            $coupon_card_no_prefix = $model->coupon_card_no_prefix;
            $points_quota_input = (int)$model->points_quota_input;
            $create_nums_input = (int)$model->create_nums_input;
            $model->create_admin_uid = \Admin::user()->id;

            if($points_quota_input <= 0 || $create_nums_input <= 0)
            {
                admin_error('请填写正确的积分额度和批量创建数量');
                return redirect(admin_base_path("couponCardCreateLog"));

            }
            if($create_nums_input > 5000)
            {
                admin_error('为了不影响平台性能，一次性最多只能创建5000个优惠卡');
                return redirect(admin_base_path("couponCardCreateLog"));
            }
            $coupon_card_no_redis_key = env('COUPON_CARD_NO_REDIS_KEY');
            $coupon_card_pub_redis_key = env('COUPON_CARD_PUB_KEY_REDIS_KEY');

            $to_rem_card_no_redis_list = Redis::zrange($coupon_card_no_redis_key,0,$create_nums_input - 1);

            if(empty($to_rem_card_no_redis_list))
            {
                admin_error('请于定时任务菜单栏中执行generate:manycounpons');
                return redirect(admin_base_path("couponCardCreateLog"));
            }
            Redis::zrem($coupon_card_no_redis_key,...$to_rem_card_no_redis_list);
            $coupon_card_pub_list = [];
            for($i=1;$i<=$create_nums_input;$i++)
            {
                array_push($coupon_card_pub_list,Redis::spop($coupon_card_pub_redis_key));
            }
            if(count($coupon_card_pub_list) < $create_nums_input)
            {
                admin_error('请于定时任务菜单栏中执行generate:manycounpons');
                return redirect(admin_base_path("couponCardCreateLog"));
            }
            $insert_all_list = [];
            foreach ($to_rem_card_no_redis_list as $k=>$v)
            {
                $coupon_card_pri_key = str_random(6);
                $insert_all_list[] = [
                    'coupon_card_no'=>$coupon_card_no_prefix.$v,
                    "name"=>$name,
                    'coupon_card_pub_key'=>$coupon_card_pub_list[$k],
                    "coupon_card_pri_key"=>strtolower($coupon_card_pri_key),
                    "points_quota"=>$points_quota_input,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                ];
            }
            $create = CouponCard::insert($insert_all_list);
            if($create)
            {
                $model->create_card_start = $coupon_card_no_prefix.$to_rem_card_no_redis_list[0];
                $model->create_card_end = $coupon_card_no_prefix.$to_rem_card_no_redis_list[count($to_rem_card_no_redis_list) - 1];
            }

        });


        return $form;
    }
}

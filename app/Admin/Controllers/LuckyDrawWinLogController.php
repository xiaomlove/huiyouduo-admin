<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\EntityPrizeVerifyExt;
use App\Models\Agent;
use App\Models\AgentStore;
use App\Models\LuckyDrawPrize;
use App\Models\LuckyDrawWinLog;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class LuckyDrawWinLogController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('中奖')
            ->description('记录')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new LuckyDrawWinLog);
        $prizeId = request('lucky_draw_prize_id');
        $luckyDrawId = request('lucky_draw_id');
        $grid->model()
            ->with(['user', 'luckyDraw', 'luckyDrawPrize'])
            ->when($prizeId, function ($query) use ($prizeId) {
                $query->where('lucky_draw_prize_id', $prizeId);
            })->when($luckyDrawId, function ($query) use ($luckyDrawId) {
                $query->where('lucky_draw_id', $luckyDrawId);
            });

        $grid->id('ID');
        $grid->column('user.username', '用户名');
        $grid->column('user.name', '真实姓名');
        $grid->column('抽奖')->display(function () {
            return $this->luckyDraw->title;
        });
        $grid->column('是否实物')->display(function () {
            if ($this->luckyDrawPrize->is_entity == 1) return "实物" ;
            else return "虚拟物品";
        });
        $grid->is_verify_take_prize('实物是否核销')->display(function ($value) {
            if($this->luckyDrawPrize->is_entity == 1)
            {
                return $value ? "已核销" : "未核销";
            }
            else return "";
        });
        $grid->column('实物核销二维码')->display(function () {
            if ($this->luckyDrawPrize->is_entity == 1)
            {
                //实物二维码
                $url = route('api.verifyprize', ['prizeid' => \Hashids::encode($this->id)]);
                $qrcode = \QrCode::format('png')->size(200)->generate($url);
                return sprintf('<img src="data:image/png;base64,%s"/>', base64_encode($qrcode));
            }
            else
                return "";
        });

        $grid->column('所属店铺-代理商')->display(function () {
            $lucky_draw_prize_id = $this->lucky_draw_prize_id;
            if(!$lucky_draw_prize_id) return "";
            $lucky_draw_prize = LuckyDrawPrize::find($lucky_draw_prize_id);
            if(!$lucky_draw_prize) return "";
            if($lucky_draw_prize->store_id == 0) return "平台";
            $store = AgentStore::find($lucky_draw_prize->store_id);
            if (!$store) return "";
            $agent = Agent::find($store->agent_id);
            $return_name = "【店铺:】{$store->name}";
            if(!$agent) return $return_name;
            $agent_name = $agent->name;
            $return_name .= ",【代理商:】{$agent_name}";
            return $return_name;
        });





        $grid->column('奖品')->display(function () {
            return $this->luckyDrawPrize->title;
        });
        $grid->created_at('中奖时间');

        $grid->disableCreateButton();
        $grid->actions(function ($actions) {
            $actions->disableEdit();
            $actions->disableDelete();
            $actions->disableView();
            $actions->append(new EntityPrizeVerifyExt($actions->getKey()));
        });

        return $grid;
    }

    /**
     * @desc 实物核销
     * @param Request $request
     * @return array
     */
    public function verifyPrize(Request $request)
    {
        $id = $request->id;
        if (empty($id)) return api("参数错误");
        try
        {
            $lucky_draw_prize_log = LuckyDrawWinLog::findOrFail($id);
            $lucky_draw_prize_log->is_verify_take_prize = 1;
            $lucky_draw_prize_log->save();
        }
        catch (\Exception $exception)
        {
            return api("核销错误");
        }

    }
    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(LuckyDrawWinLog::findOrFail($id));



        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new LuckyDrawWinLog);



        return $form;
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\Agent;
use App\Models\AgentStore;
use App\Models\LuckyDraw;
use App\Models\LuckyDrawPrize;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class LuckyDrawPrizeController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('奖品')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new LuckyDrawPrize);
        $grid->model()->with(['luckyDraw', 'store', 'store.agent']);
        $luckyDraw = LuckyDraw::findOrFail(request('lucky_draw_id'));
        $grid->model()->setRelation($luckyDraw->prizes());

        $grid->id('Id');
        $grid->title('标题');
        $grid->cover('封面');
        $grid->column('所属抽奖')->display(function () use ($grid) {
            return $this->luckyDraw->title;
        });
        $grid->column('所属代理商')->display(function () {
            if (!$this->store) {
                return '';
            }
            return sprintf('%s(%s)', $this->store->name, $this->store->agent->name);
        });
        $grid->award_one_day('每天中奖数量限制');
        $grid->award_total('全部可中奖数(库存)');
        $grid->possibility('概率占比');

        $grid->updated_at('修改时间');

        $grid->actions(function ($actions) use ($luckyDraw) {
            $actions->disableEdit();
            $actions->append(sprintf('<a href="%s">&nbsp;&nbsp;编辑</a>', route('admin.lucky-draw-prize.edit', ['id' => $actions->getKey(), 'lucky_draw_id' => $luckyDraw->id])));
            $actions->append(sprintf('<a href="%s" target="_blank">&nbsp;&nbsp;中奖记录</a>', route('admin.lucky-draw-win-log.index', ['lucky_draw_prize_id' => $actions->getKey()])));
        });


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(LuckyDrawPrize::findOrFail($id));

        $show->agent_id('Agent id');
        $show->award_one_day('Award one day');
        $show->award_total('Award total');
        $show->cover('Cover');
        $show->created_at('创建时间');
        $show->description('Description');
        $show->id('Id');
        $show->lucky_draw_id('Lucky draw id');
        $show->possibility('Possibility');
        $show->title('Title');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new LuckyDrawPrize);
        $foreignKey = 'lucky_draw_id';
        $luckyDraw = LuckyDraw::find(request($foreignKey));

        $form->display('所属抽奖')->with(function () use ($luckyDraw) {
            return optional($luckyDraw)->title;
        });
        $form->hidden($foreignKey)->value(optional($luckyDraw)->id);

        $form->select('store_id', '来源代理商(店铺)')->options(AgentStore::selectOptions());
        $form->text('title', '标题')->rules(['required']);
        $form->select('is_entity', '是否实物')->options([1=>'实物',0=>'虚拟物品']);
        $form->image('cover', '封面图');
        $form->textarea('description', '描述信息');
        $form->number('award_one_day', '一天最大中奖数')->default(1)->rules(['required', 'integer']);
        $form->number('award_total', '总共中奖数')->default(1)->rules(['required', 'integer', 'min:' . request('award_one_day')]);
        $form->number('possibility', '中奖概率占比')->help('具体占比 = 该值/所有奖品该值之和')->rules(['integer']);

        $form->saved(function ($form) use ($foreignKey) {
            return redirect()->route('admin.lucky-draw-prize.index', [$foreignKey => $form->{$foreignKey}]);
        });

        return $form;
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\NewUser;
use App\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $isSuperAdmin = \Admin::user()->isSuperAdmin();
        $isOfficial = \Admin::user()->isOfficial();

        if(!$isSuperAdmin && !$isOfficial)
        {
            $grid = new Grid(new NewUser);
            $grid->id('Id');
            $grid->column('avatar', '头像')->display(function () {
                $url = imageUrl($this->avatar);
                return sprintf('<a href="%s" target="_blank"><img src="%s" class="img-sm" /></a>', $url, $url);
            });
            $grid->nickname('用户名');
            $grid->name('真实姓名');
            $grid->phone('手机');
            $grid->column('gender', '性别')->display(function () {
                return $this->genderText;
            });
            $grid->email('Email');
            $grid->created_at('创建时间');

            $grid->disableExport();
            $grid->disableCreation();
            $grid->disableFilter();
            $grid->tools(function ($tools) {
                $tools->batch(function ($batch) {
                    $batch->disableDelete();
                });
            });
            $grid->actions(function ($actions) {
                $actions->disableEdit();
                $actions->disableDelete();
                $actions->disableView();
            });

        }
        else
        {
            $grid = new Grid(new User);
            $grid->id('Id');
            $grid->column('avatar', '头像')->display(function () {
                $url = imageUrl($this->avatar);
                return sprintf('<a href="%s" target="_blank"><img src="%s" class="img-sm" /></a>', $url, $url);
            });
            $grid->nickname('用户名');
            $grid->name('真实姓名');
            $grid->phone('手机');
            $grid->column('gender', '性别')->display(function () {
                return $this->genderText;
            });
            $grid->email('Email');
            $grid->created_at('创建时间');
        }




        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->email('Email');
        $show->email_verified_at('Email verified at');
        $show->password('Password');
        $show->remember_token('Remember token');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->text('name', 'Name');
        $form->email('email', 'Email');
        $form->password('password', 'Password');

        return $form;
    }

    public function selectJson(Request $request)
    {
        $q = $request->q;
        if (!$q)
        {
            return [];
        }
        $NewUsers = User::where(function ($query) use ($q) {
            $query->where("name", "like", "%{$q}%")->orWhere("nickname", "like", "%{$q}%");
        })->paginate(null, ['id', 'name', 'nickname']);

        foreach ($NewUsers as &$NewUser)
        {
            $NewUser->text = sprintf("%s(%s)", $NewUser->name, $NewUser->nickname);
        }

        return $NewUsers;
    }
}

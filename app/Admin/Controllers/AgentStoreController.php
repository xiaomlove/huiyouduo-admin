<?php

namespace App\Admin\Controllers;

use App\Models\Agent;
use App\Models\AgentStore;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AgentStoreController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form($id)->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form(0));
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AgentStore);
        //根据权限过滤列表
        $admin = \Admin::user();
        $isSuperAdmin = $admin->isSuperAdmin();
        $isOfficial = $admin->isOfficial();

        $where_filter_agent_id = 0;
        if(!$isSuperAdmin && !$isOfficial) {
            $where_filter_agent_id = $admin->agent()->first()->id;
        }
        $grid->model()->with(['agent', 'province', 'city', 'district'])
            ->when($where_filter_agent_id,function ($query) use ($where_filter_agent_id) {
                return $query->where("agent_id", $where_filter_agent_id);
            })->orderBy('id', 'desc');

        $grid->id('ID');
        $grid->name('名称');
        $grid->column('agent.name', '所属代理商');

        $grid->cate('店铺分类')->display(function ($value) {
            return AgentStore::$typeTexts[$value];
        });

        $grid->column('province.name', '省');
        $grid->column('city.name', '市');
        $grid->column('district.name', '区');
        $grid->address('详细地址');
        $grid->phone('联系电话');
        $grid->cover('封面')->display(function ($value) {
            $thumb = imageUrl($this->cover, 'resize,w_30,h_30');
            $origin = imageUrl($this->cover);
            return sprintf('<a href="%s" target="_blank"><img src="%s" class="img-sm" /></a>', $origin, $thumb);
        });
        $grid->remarks('备注');

        $grid->filter(function ($filters) {
            $filters->like('name', '名称');

            $admin = \Admin::user();
            if($admin->isSuperior())
            {
                $filters->equal('agent_id', '代理商')->select(Agent::pluck('name', 'id'));
            }

            $filters->equal('cate', '店铺分类')->select(AgentStore::$typeTexts);
        });

        $grid->actions(function ($actions) {
            $actions->disableView();
        });


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AgentStore::findOrFail($id));

        $show->address('Address');
        $show->agent_id('Agent id');
        $show->city_code('City code');
        $show->cover('Cover');
        $show->created_at('创建时间');
        $show->district_code('District code');
        $show->id('Id');
        $show->images('Images');
        $show->latitude('Latitude');
        $show->longitude('Longitude');
        $show->name('Name');
        $show->phone('Phone');
        $show->province_code('Province code');
        $show->remarks('Remarks');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = 0)
    {
        $form = new Form(new AgentStore);

        //根据权限过滤列表
        $admin = \Admin::user();
        $isSuperAdmin = $admin->isSuperAdmin();
        $isOfficial = $admin->isOfficial();
        $isSuperior = $admin->isSuperior();

        if(!$isSuperAdmin && !$isOfficial)
        {
            $agent_id = $admin->agent()->first()->id;
            $stores_ids = AgentStore::where('agent_id',$agent_id)->pluck('id')->toArray();
            if(!empty($id))
            {
                if(!in_array($id,$stores_ids))
                {
                    header("Location: ".url('/').admin_base_path("store"));
                    exit;
                }
            }
        }


        $isSuperAdmin = \Admin::user()->isSuperAdmin();
        $isOfficial = \Admin::user()->isOfficial();

        if(!$isSuperAdmin && !$isOfficial)
        {
            //如果是代理商管理员登陆
            $user = \App\User::all()->toArray();
            $cur_agent_admin = \Admin::user();
            $agents_admin_front_uid = $cur_agent_admin->front_uid;
            $model_data = subtree($user,$agents_admin_front_uid);
        }
        else
        {
            $model_data = \App\User::all()->toArray();
        }
        $form->select('cate', '店铺分类')->options(AgentStore::$typeTexts);
        if ($isSuperior) {
            //官方人员，选择代理商创建
            $form->select('agent_id', '代理商')->options(Agent::pluck('name', 'id'))->rules('required');
        } else {
            $form->hidden('agent_id')->value(0);
        }
        $form->select('store_bind_uid', '店铺绑定前端用户')->options(collect($model_data)->pluck('nickname','id'));
        $form->text('name', '名称')->rules(['required']);
        $form->distpicker(['province_code' => '省', 'city_code' => '市', 'district_code' => '区'], '位置')
            ->attribute('data-value-type', 'code')->rules(['required']);
        $form->text('address', '具体地址')->rules(['required']);
        $form->mobile('phone', '联系手机');
        $form->image('cover', '封面')->uniqueName()->rules(['image']);
        $form->multipleImage('images', '图片')->uniqueName()->removable()->rules(['image']);
        $form->map('latitude', 'longitude', '坐标');

        $form->text('promotion_text', '促销文案');
        $form->editor('recruitment_text', '招聘信息');
        $form->select('zhaopin_tpl_url', '招聘模板')->options(AgentStore::$typeZhaoPinTplTexts)->rules('required');
        $form->textarea('remarks', '备注');

        $form->hasMany('activities', '店铺活动', function (Form\NestedForm $form) {
            $form->text('title', '活动 名称')->rules('required');
            $form->image('cover', '活动 封面')->uniqueName()->rules(['image']);//
            $form->editor('act_desc', '活动 详情描述');

        });

        $form->saving(function ($form) use ($isSuperior) {
            if (!$isSuperior) {
                $form->agent_id = optional(\Admin::user()->agent)->id;
            }
        });

        return $form;
    }
}

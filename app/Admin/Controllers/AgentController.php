<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\AssignCouponCard;
use App\Admin\Extensions\AssignCouponCardToSecondAgent;
use App\Models\Admin;
use App\Models\Agent;
use App\Models\CouponCardAssignLog;
use App\Models\CouponCardCreateLog;
use App\Models\Role;
use App\Http\Controllers\Controller;
use App\Models\CouponCard;
use App\Repositories\AgentRepository;
use App\Repositories\AuthenticateRepository;
use App\User;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Encore\Admin\Auth\Permission;

use Illuminate\Support\MessageBag;
use Encore\Admin\Auth\Permission as Checker;

class AgentController extends Controller
{
    use HasResourceActions;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $admin = \Admin::user();
            if ($admin && $admin->isAgentLevelTwo()) {
                //二级代理只能编辑自己
                $currentRouteName = \Route::currentRouteName();
                $id = \Route::current()->parameter('vendor');
                logger(sprintf('user: %s, currentRouteName: %s, id: %s', $admin->name, $currentRouteName, $id));
                if (!in_array($currentRouteName, ['admin.vendor.edit', 'admin.vendor.update','admin.vendor.show'])) {
                    Permission::error();
                }
                if ($id != $admin->agent->id) {
                    Permission::error();
                }
            }
            return $next($request);
        });

    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('代理商')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form($id)->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Agent);
        //根据权限过滤列表
        $admin = \Admin::user();
        $isSuperAdmin = $admin->isSuperAdmin();
        $isOfficial = $admin->isOfficial();
        $isAgentLevelOne = $admin->isAgentLevelOne();
        $isAgentLevelTwo = $admin->isAgentLevelTwo();

        $where_filter_pid = 0;
        if(!$isSuperAdmin && !$isOfficial && $isAgentLevelOne) {
            $where_filter_pid = $admin->agent()->first()->id;
        }

        $grid->model()->with(['admin', 'admin.roles'])
            ->when($where_filter_pid,function ($query) use ($where_filter_pid) {
                return $query->where("parent_id", $where_filter_pid);
            })
            ->orderBy('id', 'desc');

        $grid->id('ID');
        $grid->name('代理商名称');
        $grid->column('admin.username', '账号用户名');
        $grid->column('admin.id', '账号ID');
        $grid->remarks('备注');

        $grid->created_at('创建时间');

        $grid->actions(function ($actions) use($isSuperAdmin, $isOfficial, $isAgentLevelOne, $where_filter_pid) {
            $agent_stores = Agent::find($actions->getKey())->stores()->select('id','name','agent_id')->get()->toArray();
            if(!$isSuperAdmin && !$isOfficial && $isAgentLevelOne)
            {
                $actions->append(new AssignCouponCardToSecondAgent($actions->getKey(),$agent_stores));
            }
            elseif ($isSuperAdmin || $isOfficial)
            {
                $isSuperAdmin = $actions->row->admin->isSuperAdmin();
                $isOfficial = $actions->row->admin->isOfficial();
                $isAgentLevelOne = $actions->row->admin->isAgentLevelOne();
                if(!$isSuperAdmin && !$isOfficial && $isAgentLevelOne) {
                    $actions->append(new AssignCouponCard($actions->getKey(),$agent_stores));
                }
            }
        });
        return $grid;
    }

    /**
     * @desc 官方或者超级管理员 优惠券分发 给一级代理商
     * @param Request $request
     * @return array|mixed
     */
    public function assignCouponCard(Request $request)
    {
        set_time_limit(0);
        $create_card_start = $request->create_card_start;
        $create_card_end = $request->create_card_end;
        $int_create_card_start = (int)$request->create_card_start;
        $int_create_card_end = (int)$request->create_card_end;
        $agent_id = (int)$request->id;
        if(!$agent_id) return api(-1,'请传入代理商id',[]);
        try
        {
            $agent_data = Agent::findOrFail($agent_id);
        }
        catch (\Exception $exception)
        {
            return api("代理商id参数错误，请刷新页面重试");
        }
        list($ret,$message) = CouponCard::checkValidCardNo($create_card_start,$create_card_end,$agent_id);
        if(!$ret) return api($message);

        $to_update_data = [
            'first_class_agent_id'=>$agent_id,
            'bind_store_id'=>(int)$request->bind_store_id
        ];
        $to_update_data = array_filter($to_update_data);

        $update = CouponCard::whereBetween("coupon_card_no",[$create_card_start,$create_card_end])->update($to_update_data);
        if($update)
        {
            $card_assign_log_insert_all_list = [];
            for ($i=$int_create_card_start;$i<=$int_create_card_end;$i++)
            {
                $card_assign_log_insert_all_list[] = [
                    'card_no'=>str_pad($i,13,0,STR_PAD_LEFT),
                    'assign_admin_uid'=>\Admin::user()->id,
                    'assign_admin_at'=>date('Y-m-d H:i:s'),
                    'remark'=>"官方人员给一级代理商分配优惠卡券",
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                ];
            }
            CouponCardAssignLog::insert($card_assign_log_insert_all_list);
            return api(RET_OK,RET_SUCCESS_MSG,[]);
        }
        else return api("分配错误");
    }

    /**
     * @desc 一级代理商给二级代理商分配
     * @param Request $request
     */
    public function assignCouponCardSecond(Request $request)
    {
        set_time_limit(0);
        $create_card_start = $request->create_card_start;
        $create_card_end = $request->create_card_end;
        $int_create_card_start = (int)$request->create_card_start;
        $int_create_card_end = (int)$request->create_card_end;
        $agent_id = (int)$request->id;
        if(!$agent_id) return api(-1,'请传入代理商id',[]);
        try
        {
            $agent_data = Agent::findOrFail($agent_id);
        }
        catch (\Exception $exception)
        {
            return api("代理商id参数错误，请刷新页面重试");
        }
        list($ret,$message) = CouponCard::checkValidCardNoAgent($create_card_start,$create_card_end,$agent_id);
        if(!$ret) return api($message);

        $to_update_data = [
            'second_class_agent_id'=>$agent_id,
            'bind_store_id'=>(int)$request->bind_store_id
        ];
        $to_update_data = array_filter($to_update_data);
        $update = CouponCard::whereBetween("coupon_card_no",[$create_card_start,$create_card_end])->update($to_update_data);
        if($update)
        {
            $card_assign_log_insert_all_list = [];
            for ($i=$int_create_card_start;$i<=$int_create_card_end;$i++)
            {
                $card_assign_log_insert_all_list[] = [
                    'card_no'=>str_pad($i,13,0,STR_PAD_LEFT),
                    'assign_admin_uid'=>\Admin::user()->id,
                    'assign_admin_at'=>date('Y-m-d H:i:s'),
                    'remark'=>"一级代理商给二级代理商分配优惠卡券",
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                ];
            }
            CouponCardAssignLog::insert($card_assign_log_insert_all_list);
            return api(RET_OK,RET_SUCCESS_MSG,[]);
        }
        else return api("分配错误");

    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Agent::with(['admin', 'admin.frontUser'])->findOrFail($id));

        $this->canAccessAgent($id);

        $show->id('ID');
        $show->name('名称');
        $show->commission_percent('佣金比例');
        $show->remarks('备注');
        $show->admin('后台账号', function ($admin) {
            $admin->id('ID');
            $admin->name('姓名');
            $admin->username('昵称');
            $admin->avatar('头像')->image();
            disable_detail_tools($admin);
            $admin->frontUser('前端账号', function ($user) {
                $user->id('ID');
                $user->name('姓名');
                $user->nickname('昵称');
                $user->phone('手机');
                $user->avatar('头像')->image();
                disable_detail_tools($user);
            });
        });

        $show->qrcode('二维码')->unescape()->as(function () {
            $url = route('api.promotion.qrcode', ['code' => \Hashids::encode($this->id)]);
            $qrcode = \QrCode::format('png')->size(200)->generate($url);
            return sprintf('<img src="data:image/png;base64,%s"/>', base64_encode($qrcode));
        });
        $csrfField = csrf_field();
        $bindFrontUserModal = <<<EOT
<div class="modal fade" id="bind-front-user-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">绑定前端用户</h4>
      </div>
      <div class="modal-body">
        <form id="bind-front-user-form">
          <div class="form-group">
            <label for="recipient-name" class="control-label">前端用户手机号:</label>
            <input type="text" class="form-control" id="phone" name="phone">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">短信验证码:</label>
            <input type="text" class="form-control" id="sms-verify-code" name="sms_verify_code">
          </div>
          <div class="form-group">
            <button type="button" class="btn btn-default" id="get-sms-verify-code">获取验证码</input>
          </div>
          $csrfField
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="submit-bind-front-user">保存</button>
      </div>
    </div>
  </div>
</div>
EOT;
        $getSmsVerifyCodeUrl = route('api.sms-verify-code');
        $submitUrl = route('admin.vendor.bind-front-user');
        $script = <<<EOT
var getCodeBtn = $('#get-sms-verify-code');
var phoneInput = $('#phone');
getCodeBtn.on('click', function () {
    var phone = phoneInput.val();
    if (!phone) {
        alert('请输入手机号');
        return;
    }
    var data = {"phone": phone};
    $.post('$getSmsVerifyCodeUrl', data, function (response) {
        console.log(response);
        if (response.ret == 0) {
            getCodeBtn.attr("disabled", "disabled").text("发送成功，请注意查收，不要重复发送");
        } else {
            alert(response.msg);
        }   
    }, 'json');
});

var submitBtn = $('#submit-bind-front-user');
submitBtn.on('click', function (e) {
    var data = $('#bind-front-user-form').serialize();
    $.post('$submitUrl', data, function (response) {
        console.log(response);
        if (response.ret == 0) {
            window.alert('绑定成功');
            window.location.reload();
        } else {
            alert(response.msg);
        }
        
    }, 'json');
});

EOT;

        if (\Admin::user()->agent->id == $id) {
            \Admin::script($script);
            $show->panel()->tools(function ($tools) use ($bindFrontUserModal) {
                $tools->append('<button type="button" class="btn btn-info" data-toggle="modal" data-target="#bind-front-user-modal" data-whatever="@mdo">绑定前端用户</button>');
                $tools->append($bindFrontUserModal);
            });
        }

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $form = new Form(new Agent);
        if ($id) {
            $this->canAccessAgent($id);
        }

        $form->text('admin.name', '账号名称')->placeholder('如：张三')->rules('required');
        $form->text('admin.username', '账号登录名')->placeholder('如：zhangsan')->rules(['required']);
        $form->password('admin.password', '账号登录密码')->placeholder('如：123456')->rules(['required']);

        if (\Admin::user()->isOfficial() || \Admin::user()->isSuperAdmin())
        {
            //官方人员，添加时可以指定一级代理商
            $form->select('parent_id', '上级代理商')->options(Agent::where('level', 1)->where("id", "!=", $id)->pluck('name', 'id')->prepend('无上级', 0));
        } else {
            $form->hidden('parent_id')->value(0);
        }
        $form->hidden('level', '层级');
        $form->text('name', '代理商名称')->placeholder('如：XXX餐饮有限公司')->rules(['required']);
        $form->number('commission_percent', '佣金比例')->placeholder('佣金比例，0~100')->rules(['integer', 'between:0,100']);

        $form->textarea('remarks', '备注');

        $logPrefix = __METHOD__;
        $form->saving(function (Form $form) use ($id, $logPrefix) {
//            dd($form, $form->admin, $form->model()->admin);
            if ($form->admin['password'] && optional($form->model()->admin)->password != $form->admin['password']) {
                logger(sprintf("%s, input password: %s, model password: %s", $logPrefix, $form->admin['password'], optional($form->model()->admin)->password));
                $inputAdmin = $form->admin;
                $inputAdmin['password'] = bcrypt($inputAdmin['password']);
                $form->admin = $inputAdmin;
            }
            $log = $logPrefix;
            if ($form->parent_id) {
                $log .= ", specific parent_id: " . $form->parent_id;
                //指定了上级，它肯定是二级
                $form->level = 2;
            }else {
                $form->parent_id = 0;
                $form->level = 1;
                $log .= ", not isAgentLevelOne, parent_id: 0";
            }
            $isAgentLevelOne = \Admin::user()->isAgentLevelOne();
            if ($isAgentLevelOne) {
                //一级代理商创建的，自动自己是他的上级，也是二级
                $parentId = \Admin::user()->agent->id;
                $form->parent_id = $parentId;
                $form->level = 2;
                $log .= ", isAgentLevelOne, parent_id: $parentId";
            }
            \Log::info($log);

        });

        $form->saved(function ($form) use ($id, $logPrefix) {
            if (\Route::currentRouteName() != 'admin.vendor.store') {
                if (\Admin::user()->isAgentLevelTwo()) {
                    admin_toastr('保存成功');
                    return back();// 二级代理商编辑个人信息，不能跳转列表
                } else {
                    return;
                }
            }
            //创建之后，用户会自动创建，需要关联一个角色
            $log = sprintf("%s, new agent: %s, assign role to admin_user: %s", $logPrefix, $form->model()->id, $form->model()->admin_uid);
            if ($form->model()->level == 1)
            {
                //为一级代理。
                logger("$log, level 1, role: " . Role::SLUG_AGENT_LEVEL_1);
                $roles = Role::whereIn("slug", [Role::SLUG_AGENT, Role::SLUG_AGENT_LEVEL_1])->get();
            }
            else
            {
                logger("$log, level 2, role: " . Role::SLUG_AGENT_LEVEL_2);
                $roles = Role::whereIn("slug", [Role::SLUG_AGENT, Role::SLUG_AGENT_LEVEL_2])->get();
            }
            $form->model()->admin()->firstOrFail()->roles()->saveMany($roles);
            if (\Admin::user()->isAgentLevelTwo()) {
                admin_toastr('保存成功');
                return back();// 二级代理商编辑个人信息，不能跳转列表
            }

        });

        return $form;
    }

    /**
     * 绑定前端用户
     *
     * @param Request $request
     * @return array
     */
    public function bindFrontUser(Request $request)
    {
        $smsVerifyCode = $request->sms_verify_code;
        $phone = $request->phone;
        $loginAdmin = \Admin::user();
        $checkCodeResult = (new AuthenticateRepository())->checkSmsVerifyCode($smsVerifyCode, $phone);
        if ($checkCodeResult['ret'] != 0) {
            return $checkCodeResult;
        }
        $smsLog = $checkCodeResult['data'];
        $userBindThisPhone = User::where('phone', $phone)->first();
        if (!$userBindThisPhone) {
            return api("没有绑定手机：$phone 的用户");
        }
        $adminBindThisUser = Admin::where('front_uid', $userBindThisPhone->id)->where('id', '!=', $loginAdmin->id)->first();
        if ($adminBindThisUser) {
            return api("已经有后台账号： {$adminBindThisUser->id} 绑定了此前端用户");
        }
        $loginAdmin->front_uid = $userBindThisPhone->id;
        $loginAdmin->save();
        $smsLog->update(['used_at' => now()]);

        return api(RET_OK, RET_SUCCESS_MSG, []);
    }

    /*
    public function store(Request $request)
    {
        if ($validationMessages = $this->form()->validationMessages($request->all())) {
            return back()->withInput()->withErrors($validationMessages);
        }
        dd($request);
        //先将图片上传
        $uploadFiles = $request->file();
        if ($uploadFiles)
        {
            foreach ($uploadFiles as $field => $img)
            {
                $keys = [];
                foreach (array_wrap($img) as $_img)
                {
                    $uploadResult = $_img->store(config('admin.upload.directory.image'));
                    if ($uploadResult)
                    {
                        $keys[] = $uploadResult;
                    }
                }
                $keyStored = is_array($img) ? $keys : $keys[0];
                $request->request->remove($field);
                $request->request->add([$field => $keyStored]);
            }

        }
        $agent = (new AgentRepository())->createAgent($request);

        admin_toastr('创建成功');

        return redirect()->route('vendor.index');
    }

    public function update(Request $request)
    {
        if ($validationMessages = $this->form()->validationMessages($request->all())) {
            return back()->withInput()->withErrors($validationMessages);
        }
        dd($request);
        //先将图片上传
        $uploadFiles = $request->file();
        if ($uploadFiles)
        {
            foreach ($uploadFiles as $field => $img)
            {
                $keys = [];
                foreach (array_wrap($img) as $_img)
                {
                    $uploadResult = $_img->store(config('admin.upload.directory.image'));
                    if ($uploadResult)
                    {
                        $keys[] = $uploadResult;
                    }
                }
                $keyStored = is_array($img) ? $keys : $keys[0];
                $request->request->remove($field);
                $request->request->add([$field => $keyStored]);
            }

        }
        $agent = (new AgentRepository())->createAgent($request);

        admin_toastr('创建成功');

        return redirect()->route('vendor.index');
    }
    */
}

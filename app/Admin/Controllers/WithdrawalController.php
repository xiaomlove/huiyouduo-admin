<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Actions\AuditWithdrawal;
use App\Models\WithdrawalLog;
use App\Http\Controllers\Controller;
use App\Repositories\FinanceRepository;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class WithdrawalController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        $csrf = csrf_field();
        $modal = <<<MODAL
<div id="audit-withdrawal-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">审核</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="audit-withdrawal-form">
        {$csrf}
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">结果</label>
            <div class="col-sm-10">
              <label class="radio-inline">
              <input type="radio" name="status" id="inlineRadio1" value="1"> 通过
                </label>
                <label class="radio-inline">
                  <input type="radio" name="status" id="inlineRadio2" value="0"> 拒绝
                </label>
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">原因</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="status_text" placeholder="拒绝时务必填写"></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="audit-withdrawal-submit">确定</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
MODAL;

        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid())
            ->body($modal);
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WithdrawalLog);
        $grid->model()->with(['user']);

        $grid->id('Id');
        $grid->column('user.name', '用户');
        $grid->channel('渠道')->display(function () {
            return $this->channelName;
        });
        $grid->column('financeAccount.account', '提现到账户');
        $grid->money('金额(元)')->display(function () {
            return $this->money / 100;
        });
        $grid->out_biz_no('商户业务号');
        $grid->created_at('申请时间');
        $grid->alipay_trade_no('支付宝订单号');
        $grid->paid_at('转账时间');
        $grid->status('状态')->display(function () {
            return $this->statusText;
        });

        $grid->disableCreateButton();
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableEdit();
            $actions->disableDelete();
            $actions->append(new AuditWithdrawal($actions->getKey()));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WithdrawalLog::findOrFail($id));

        $show->id('Id');
        $show->uid('Uid');
        $show->channel('Channel');
        $show->finance_account_id('Finance account id');
        $show->money('Money');
        $show->status('Status');
        $show->trade_snapshot('Trade snapshot');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');
        $show->out_biz_no('Out biz no');
        $show->alipay_trade_no('Alipay trade no');
        $show->paid_at('Paid at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WithdrawalLog);

        $form->number('uid', 'Uid');
        $form->switch('channel', 'Channel');
        $form->number('finance_account_id', 'Finance account id');
        $form->number('money', 'Money');
        $form->switch('status', 'Status');
        $form->textarea('trade_snapshot', 'Trade snapshot');
        $form->text('out_biz_no', 'Out biz no');
        $form->text('alipay_trade_no', 'Alipay trade no');
        $form->datetime('paid_at', 'Paid at')->default(date('Y-m-d H:i:s'));

        return $form;
    }

    public function audit(Request $request)
    {
        $withdrawal = WithdrawalLog::findOrFail($request->id);
        if ($request->status === null) {
            return api('请选择通过或拒绝');
        }
        $result = (new FinanceRepository())->withdrawalToAlipay($withdrawal, $request->status, $request->status_text);
        return $result;
    }
}

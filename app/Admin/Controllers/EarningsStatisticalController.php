<?php

namespace App\Admin\Controllers;

use App\Models\EarningsStatistical;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class EarningsStatisticalController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new EarningsStatistical);

        $grid->id('Id');
        $grid->day('日期');
        $grid->income('每日收入');
        $grid->spending('每日支出');
        $grid->remark('备注');
        $grid->created_at('创建时间');
        $grid->updated_at('修改时间');

        $grid->disableCreateButton();
        $grid->disableActions();

        $grid->filter(function($filter){

            // 去掉默认的id过滤器
        });

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(EarningsStatistical::findOrFail($id));

        $show->id('Id');
        $show->day('Day');
        $show->income('Income');
        $show->spending('Spending');
        $show->remark('Remark');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new EarningsStatistical);

        $form->date('day', 'Day')->default(date('Y-m-d'));
        $form->decimal('income', 'Income')->default(0.00);
        $form->decimal('spending', 'Spending')->default(0.00);
        $form->text('remark', 'Remark');

        return $form;
    }
}

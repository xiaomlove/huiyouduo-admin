<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\UserLowScoreSendExt;
use App\Jobs\UserLowScoreSendQueue;
use App\Models\Config;
use App\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class UserLowScoreSendController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);

        $user_low_score_send = Config::getUserLowScoreSend();

        $grid->model()->where('scores','<',$user_low_score_send)->orderBy('id', 'desc');

        $grid->id('Id');
        $grid->name('名称');
        $grid->phone('电话');
        $grid->nickname('昵称');
        $grid->scores('积分');
        $grid->created_at('创建时间');
        $grid->updated_at('更新时间');

        $grid->disableExport();
        $grid->disableCreation();
        $grid->disableFilter();
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
            $tools->append(new UserLowScoreSendExt());
        });
        $grid->actions(function ($actions) {
            $actions->disableEdit();
            $actions->disableDelete();
            $actions->disableView();
//            $actions->append(new AgentSettleExt($actions->getKey()));
        });
        return $grid;
    }

    /**
     * @desc 发送积分
     * @param Request $request
     * @return array
     */
    public function oneKeyUserLowScoreSend(Request $request)
    {
        UserLowScoreSendQueue::dispatch()->onQueue('user_low_score_send');
//        $this->dispatch((new UserLowScoreSendQueue())->onQueue('user_low_score_send'));
        return api(0,'添加任务成功',[]);
    }
    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->email('Email');
        $show->phone('Phone');
        $show->password('Password');
        $show->nickname('Nickname');
        $show->avatar('Avatar');
        $show->gender('Gender');
        $show->province('Province');
        $show->city('City');
        $show->country('Country');
        $show->scores('Scores');
        $show->money('Money');
        $show->deleted_at('Deleted at');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');
        $show->parent_id('Parent id');
        $show->tb_uid('Tb uid');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->text('name', 'Name');
        $form->email('email', 'Email');
        $form->mobile('phone', 'Phone');
        $form->password('password', 'Password');
        $form->text('nickname', 'Nickname');
        $form->image('avatar', 'Avatar');
        $form->switch('gender', 'Gender');
        $form->text('province', 'Province');
        $form->text('city', 'City');
        $form->text('country', 'Country');
        $form->number('scores', 'Scores');
        $form->number('money', 'Money');
        $form->number('parent_id', 'Parent id');
        $form->number('tb_uid', 'Tb uid');

        return $form;
    }
}

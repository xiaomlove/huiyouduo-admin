<?php

namespace App\Admin\Controllers;

use App\Models\Items;
use App\Http\Controllers\Controller;
use App\Models\ItemsCate;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ItemsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('商品管理')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('商品管理')
            ->description('详情')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('商品管理')
            ->description('编辑')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('商品管理')
            ->description('编辑')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Items);

        $grid->id('Id');
        $grid->short_title('短标题');
//        $grid->title('标题');
        $grid->pict_url('商品主图')->display(function ($pic) {
            return empty($pic) ? "----" : "<img style='width: 150px;height: 150px' src='$pic'>";
        });
        $grid->column('oneCate.title',"一级栏目");
        $grid->column('oneLeafCate.title',"二级栏目");
        $grid->reserve_price('原价');
        $grid->zk_final_price('折后价');
        $grid->coupon_info('优惠券信息');
        $grid->coupon_price('优惠券金额(元)');
        $grid->coupon_after_price('券后价(元)');
        $grid->column('优惠券有效时间')->display(function () {
            return $this->coupon_start_time."--".$this->coupon_end_time;
        });
        $grid->item_url("产品地址")->display(function ($item_url) {
            if(empty($item_url)) return "--";
            else return "<a target='_blank' href='$item_url'>产品地址</a>";
        });
        $grid->quan_link("优惠券链接")->display(function ($quan_link) {
            if(empty($quan_link)) return "----";
            else return "<a target='_blank' href='$quan_link'>优惠券链接</a>";
        });
        $grid->user_type('是否天猫')->display(function ($is_tmall) {
            return $is_tmall ? "是" : "否";
        });
        $grid->commission_rate('佣金(包括扣除技术服务费用和税收)')->display(function ($commission_rate) {
            return $this->coupon_after_price * ($commission_rate/10000) * 0.89;
        });
        $grid->disableCreateButton();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Items::findOrFail($id));

        $show->id('Id');
        $show->short_title('Short title');
        $show->title('Title');
        $show->item_description_score('Item description score');
        $show->commission_queqiao('Commission queqiao');
        $show->quan_receive('Quan receive');
        $show->quan_price('Quan price');
        $show->yongjin_type('Yongjin type');
        $show->quan_time('Quan time');
        $show->jihua_link('Jihua link');
        $show->price('Price');
        $show->jihua_shenhe('Jihua shenhe');
        $show->introduce('Introduce');
        $show->cid('Cid');
        $show->cate_leaf_id('Cate leaf id');
        $show->sales_nums('Sales nums');
        $show->quan_link('Quan link');
        $show->is_tmall('Is tmall');
        $show->tb_items_id('Tb items id');
        $show->commission_jihua('Commission jihua');
        $show->que_siteid('Que siteid');
        $show->commission('Commission');
        $show->pic('Pic');
        $show->org_price('Org price');
        $show->quan_m_link('Quan m link');
        $show->quan_id('Quan id');
        $show->quan_condition('Quan condition');
        $show->quan_surplus('Quan surplus');
        $show->seller_id('Seller id');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Items);

        $form->display('short_title', '短标题');
        $form->display('pict_url', '商品主图')->with(function ($data) {
            return empty($data) ? '--' : "<img style='width: 200px;height: 200px' src='$data'>";
        });
        $form->display('cid', '一级栏目')->with(function ($cid) {
            return ItemsCate::find($cid) ? ItemsCate::find($cid)->title : "--";
        });
        $form->display('cate_leaf_id', '二级栏目')->with(function ($data) {
            return ItemsCate::find($data) ? ItemsCate::find($data)->title : "--";
        });

        $form->display('reserve_price', '原价')->default(0.00)->with(function ($data) {
            return $data."元";
        });
        $form->display('zk_final_price', '折后价')->default(0.00)->with(function ($data) {
            return $data."元";
        });
        $form->display('coupon_info', '优惠券信息');
        $form->display('coupon_price', '优惠券金额')->default(0.00)->with(function ($data) {
            return $data."元";
        });
        $form->display('coupon_valid_time', '优惠券有效时间')->with(function ($data) {
            return $this->coupon_start_time."--".$this->coupon_end_time;
        });
        $form->display('item_url', '产品地址')->with(function ($data) {
            if(empty($data)) return "--";
            else return "<a target='_blank' href='$data'>产品地址</a>";
        });
        $form->display('quan_link', '优惠券链接')->with(function ($data) {
            return empty($data) ? "--" : "<a href='$data' target='_blank'>优惠券链接</a>";
        });
        $form->display('user_type', '是否天猫')->with(function ($data) {
            return $data ? "是" : "否";
        });
        $form->display('commission_rate', '佣金')->default(0.00)->with(function ($data) {
            return $this->coupon_after_price * ($data/10000) * 0.89;
        });
        return $form;
    }
}

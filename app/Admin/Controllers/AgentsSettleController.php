<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\AgentSettleExt;
use App\Models\Agent;
use App\Models\AgentsSettle;
use App\Http\Controllers\Controller;
use App\Models\TbkOficalOrder;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use App\User;
use App\Models\Admin;

class AgentsSettleController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AgentsSettle);
        //根据权限过滤列表
        $admin = \Admin::user();
        $isSuperAdmin = $admin->isSuperAdmin();
        $isOfficial = $admin->isOfficial();

        $cur_agent_admin_uid = 0;
        if(!$isSuperAdmin && !$isOfficial) $cur_agent_admin_uid = $admin->id;

        $grid->model()->when($cur_agent_admin_uid,function ($query) use ($cur_agent_admin_uid) {
                return $query->where("agent_admin_uid", $cur_agent_admin_uid);
            })
            ->orderBy('id', 'desc');

        $grid->id('Id');
        $grid->day('结算日期');
        $grid->settle_total_price('结算金额');
        $grid->agent_admin_uid('结算代理商')->display(function ($value){
            $agent = Agent::where('admin_uid',$value)->first();
            if(!$agent) return "--";
            else return $agent->name."({$agent->level}级代理商)";
        });
        $grid->is_all_settled('是否已经结算')->display(function ($value){
            return $value ? "已结算" : "未结算";
        });
        $grid->created_at('创建时间');
        $grid->updated_at('修改时间');

        $grid->disableExport();
        $grid->disableCreation();
        $grid->disableFilter();
        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });
        $grid->actions(function ($actions) {
            $actions->disableEdit();
            $actions->disableDelete();
            $actions->disableView();
            $actions->append(new AgentSettleExt($actions->getKey()));
        });

        return $grid;
    }

    /**
     * 结算
     * @param Request $request
     */
    public function agentsSettleUpdate(Request $request)
    {
        $id = (int)$request->id;
        $settle_total_price = $request->settle_total_price;
        if(empty($id) || empty($settle_total_price)) return api("参数错误");
        try
        {
            $agent_settle = AgentsSettle::findOrFail($id);
            $agent = Agent::where('admin_uid',$agent_settle->agent_admin_uid)->first();
            $start_time = date("Y-m-01 00:00:00", time());
            $end_time = date('Y-m-t 23:59:59', time());
            $user = User::all()->toArray();
            $agents_admin = Admin::find($agent->admin_uid);
            if(!$agents_admin) return api("系统错误");
            $agents_admin_front_uid = $agents_admin->front_uid;
            $agents_admin_front_uid_childrens = subtree($user,$agents_admin_front_uid);
            if(empty($agents_admin_front_uid_childrens)) return api("无需结算");
            $agents_order = [];
            foreach ($agents_admin_front_uid_childrens as $kk=>$vv)
            {
                $agents_order = array_merge($agents_order,$this->user_order($vv['id'],$vv['tb_uid']));
            }
            if(!empty($agents_order))
            {
                foreach ($agents_order as $k=>$v)
                {
                    $order = TbkOficalOrder::find($v['id']);
                    $order->is_settled = 1;
                    $order->updated_at = date('Y-m-d H:i:s');
                    $order->save();
                }
                //
                $agent_settle->is_all_settled = 1;
                $agent_settle->save();
                //添加代理商支出记录-平台

                $has_insert = \App\Models\EarningsStatistical::where([
                    'day'=>date('Y-m-d'),
                ])->exists();
                if($has_insert)
                {
                    \App\Models\EarningsStatistical::where([
                        'day'=>date('Y-m-d'),
                    ])->update([
                        'spending'=>$settle_total_price,
                        'remark'=>'代理商结算',
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }
                else
                {
                    \App\Models\EarningsStatistical::create([
                        'day'=>date('Y-m-d'),
                        'spending'=>$settle_total_price,
                        'remark'=>'代理商结算'
                    ]);
                }
                //添加代理商收益-针对代理商的
                $has_insert = \App\Models\AgentsEarningsStatistical::where([
                    'day'=>date('Y-m-d'),
                ])->exists();
                if($has_insert)
                {
                    \App\Models\AgentsEarningsStatistical::where([
                        'day'=>date('Y-m-d'),
                    ])->update([
                        'agent_admin_uid'=>$agent_settle->agent_admin_uid,
                        'income'=>$settle_total_price,
                        'remark'=>'代理商结算',
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }
                else
                {
                    \App\Models\AgentsEarningsStatistical::create([
                        'agent_admin_uid'=>$agent_settle->agent_admin_uid,
                        'day'=>date('Y-m-d'),
                        'income'=>$settle_total_price,
                        'remark'=>'代理商结算'
                    ]);
                }

            }
            return api("结算成功");
        }
        catch (\Exception $exception)
        {
            return api("参数错误");
        }
    }

    /**
     * @desc 用户的订单列表
     * @param int $uid
     * @return array
     */
    protected function user_order($uid = 0,$tb_uid = 0)
    {
        if(empty($uid)) return;
        $user_order_list = [];
        $start_time = date("Y-m-01 00:00:00", time());
        $end_time = date('Y-m-t 23:59:59', time());
        if(!empty($tb_uid))
        {
            //这个是怎么筛选订单的 uid  和 订单 后6位 匹配
            $user_order_list = TbkOficalOrder::where([
                'tk_status'=>2,
            ])->whereBetween('earning_time',[$start_time,$end_time])
                ->select(
                    'id',
                    'trade_parent_id'
                )->get()->toArray();
            if(!empty($user_order_list))
            {
                foreach ($user_order_list as $k=>$v)
                {
                    if($tb_uid != handle_tbk_trade_parent_id($v['trade_parent_id']))
                    {
                        unset($user_order_list[$k]);
                    }
                    else
                    {
                        unset($user_order_list[$k]['trade_parent_id']);
                    }
                }
            }
        }
        else
        {
            //这种是绑定pid的用户
            $tb_pid_info = TaobaoPids::where("uid",$uid)->first();
            if($tb_pid_info)
            {
                $adzone_id = $tb_pid_info->adzone_id;
                $site_id = $tb_pid_info->site_id;
                $user_order_list = TbkOficalOrder::where([
                    "adzone_id"=>$adzone_id,
                    "site_id"=>$site_id,
                    'tk_status'=>2
                ])->whereBetween('earning_time',[$start_time,$end_time])
                    ->select(
                        'id'
                    )->get()->toArray();//commission
            }
        }
        return $user_order_list;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AgentsSettle::findOrFail($id));

        $show->id('Id');
        $show->day('Day');
        $show->settle_total_price('Settle total price');
        $show->agent_admin_uid('Agent admin uid');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AgentsSettle);

        $form->text('day', 'Day');
        $form->decimal('settle_total_price', 'Settle total price')->default(0.00);
        $form->number('agent_admin_uid', 'Agent admin uid');

        return $form;
    }
}

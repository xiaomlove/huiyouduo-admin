<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\CouponCard;
use App\Models\TaobaoPids;
use App\Models\TbkOficalOrder;
use App\User;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Widgets\InfoBox;
use Encore\Admin\Widgets\Form;


class HomeController extends Controller
{
    public function index(Content $content)
    {
        return Admin::content(function (Content $content) {

            $content->header('简要数据统计');
            $newUserWeek = $this->newUserWeek();

            list($newOrderCountWeek,$newOrderPriceCountWeek) = $this->newOrderWeek();

            $content->row(function (Row $row) use($newUserWeek,$newOrderCountWeek,$newOrderPriceCountWeek) {
                $row->column(4, function (Column $column) use($newUserWeek) {
                    $column->append(new InfoBox('近7日新增用户数', 'book-alt', 'aqua', admin_base_path('user'), $newUserWeek));
                });
                $row->column(4, function (Column $column) use($newOrderCountWeek) {
                    $column->append(new InfoBox('近7日新增订单数', 'book-alt', 'green', admin_base_path('tbkOficalOrder'), $newOrderCountWeek));
                });
                $row->column(4, function (Column $column) use($newOrderPriceCountWeek) {
                    $column->append(new InfoBox('近7日新增订单总金额', 'book-alt', 'yellow', admin_base_path('tbkOficalOrder'), $newOrderPriceCountWeek));
                });

            });
            $user = \Admin::user();
            if(!$user->isSuperior())
            {
                $content->row(function (Row $row)  {
                    $row->column(4, function (Column $column)  {
                        $form = new Form();
                        $form->action(admin_base_path('/'));
                        $form->method('GET');
                        $form->date('date','选择时间')->format('YYYY-MM');
                        $column->append($form);
                    });
                });
                $query_month = $_GET['date'] ?? date('Y-m');
//                dd($query_month);
                list($card_nums_data,$user_nums_data) = $this->current_agent_coupon_count($query_month);

                $content->row(function (Row $row) use($card_nums_data,$user_nums_data) {
                    $row->column(4, function (Column $column) use($card_nums_data) {
                        $column->append(new InfoBox('月份总卡量', 'book-alt', 'aqua', admin_base_path('/'), $card_nums_data['total_card_nums']));
                    });
                    $row->column(4, function (Column $column) use($card_nums_data) {
                        $column->append(new InfoBox('月份已充值(人)', 'book-alt', 'green', admin_base_path('/'), $card_nums_data['has_bind_nums']));
                    });
                    $row->column(4, function (Column $column) use($card_nums_data) {
                        $column->append(new InfoBox('月份未充值(人)', 'book-alt', 'yellow', admin_base_path('/'), $card_nums_data['un_bind_nums']));
                    });

                });
                $content->row(function (Row $row) use($card_nums_data,$user_nums_data) {
                    $row->column(4, function (Column $column) use($user_nums_data) {
                        $column->append(new InfoBox('今日充值人数', 'book-alt', 'aqua', admin_base_path('/'), $user_nums_data['today_bind_nums']));
                    });
                    $row->column(4, function (Column $column) use($user_nums_data) {
                        $column->append(new InfoBox('本周充值人数', 'book-alt', 'green', admin_base_path('/'), $user_nums_data['week_bind_nums']));
                    });
                    $row->column(4, function (Column $column) use($user_nums_data) {
                        $column->append(new InfoBox('本月充值人数', 'book-alt', 'yellow', admin_base_path('/'), $user_nums_data['month_bind_nums']));
                    });

                });


            }



        });
    }
    //近7日的新增用户数
    public function newUserWeek()
    {
        $cur_time = time();
        $start_time = $cur_time - 7 * 24 * 3600;
        $start_date = date('Y-m-d H:i:s',$start_time);
        $end_date = date('Y-m-d H:i:s',$cur_time);

        $isSuperAdmin = \Admin::user()->isSuperAdmin();
        $isOfficial = \Admin::user()->isOfficial();

        $return_count = 0;

        if(!$isSuperAdmin && !$isOfficial)
        {
            //如果是代理商管理员登陆
            $user = User::whereBetween('created_at',[$start_date,$end_date])->get()->toArray();
            $cur_agent_admin = \Admin::user();
            $agents_admin_front_uid = $cur_agent_admin->front_uid;
            $agents_admin_front_uid_childrens = subtree($user,$agents_admin_front_uid);
            $return_count = count($agents_admin_front_uid_childrens);
        }
        else
        {
            $return_count = User::whereBetween('created_at',[$start_date,$end_date])->count();
        }
        return $return_count;

    }

    /**
     * @desc 近7日新增订单总数量+总金额
     */
    public function newOrderWeek()
    {
        $cur_time = time();
        $start_time = $cur_time - 7 * 24 * 3600;
        $start_date = date('Y-m-d H:i:s',$start_time);
        $end_date = date('Y-m-d H:i:s',$cur_time);

        $isSuperAdmin = \Admin::user()->isSuperAdmin();
        $isOfficial = \Admin::user()->isOfficial();

        $return_count = 0;

        if(!$isSuperAdmin && !$isOfficial)
        {
            //如果是代理商管理员登陆
            $user = User::whereBetween('created_at',[$start_date,$end_date])->get()->toArray();
            $cur_agent_admin = \Admin::user();
            $agents_admin_front_uid = $cur_agent_admin->front_uid;
            $agents_admin_front_uid_childrens = subtree($user,$agents_admin_front_uid);

            $agents_order = [];
            foreach ($agents_admin_front_uid_childrens as $k=>$v)
            {
                $agents_order = array_merge($agents_order,$this->user_order($v['id'],$v['tb_uid']));
            }

            if(empty($agents_order)) return[0,0];
            else
            {
                $sum_count = count($agents_order);
                $sum_pay_price_count = array_sum(array_map(create_function('$val', 'return $val["pay_price"];'), $agents_order));
                return [$sum_count,$sum_pay_price_count ? $sum_pay_price_count : 0];
            }
        }
        else
        {
            //平台近7日新增订单
            $newOrderWeek = TbkOficalOrder::whereBetween('created_at',[$start_date,$end_date])
                ->selectRaw('COUNT(*) as count')
                ->selectRaw('SUM(pay_price) as pay_price_count')
                ->first();
            return [$newOrderWeek->count,$newOrderWeek->pay_price_count ? $newOrderWeek->pay_price_count : 0];
        }
    }


    /**
     * @desc 用户的订单列表
     * @param int $uid
     * @return array
     */
    protected function user_order($uid = 0,$tb_uid = 0)
    {
        if(empty($uid)) return;
        $user_order_list = [];
        $cur_time = time();
        $start_time = $cur_time - 7 * 24 * 3600;
        $start_date = date('Y-m-d H:i:s',$start_time);
        $end_date = date('Y-m-d H:i:s',$cur_time);
        if(!empty($tb_uid))
        {
            //这个是怎么筛选订单的 uid  和 订单 后6位 匹配
            $user_order_list = TbkOficalOrder::
            when($start_date, function ($query) use ($start_date) {
                $query->where('create_time','>=', $start_date);
            })->when($end_date, function ($query) use ($end_date) {
                $query->where('create_time','<=', $end_date);
            })->select(
                "pay_price",
                'trade_parent_id'
            )->get()->toArray();
            if(!empty($user_order_list))
            {
                foreach ($user_order_list as $k=>$v)
                {
                    if($tb_uid != handle_tbk_trade_parent_id($v['trade_parent_id']))
                    {
                        unset($user_order_list[$k]);
                    }
                    else
                    {
                        unset($user_order_list[$k]['trade_parent_id']);
                    }
                }
            }
        }
        else
        {
            //这种是绑定pid的用户
            $tb_pid_info = TaobaoPids::where("uid",$uid)->first();
            if($tb_pid_info)
            {
                $adzone_id = $tb_pid_info->adzone_id;
                $site_id = $tb_pid_info->site_id;
                $user_order_list = TbkOficalOrder::where([
                    "adzone_id"=>$adzone_id,
                    "site_id"=>$site_id,
                ])->when($start_date, function ($query) use ($start_date) {
                    $query->where('create_time','>=', $start_date);
                })->when($end_date, function ($query) use ($end_date) {
                    $query->where('create_time','<=', $end_date);
                })->select(
                    "pay_price"
                )->get()->toArray();//
            }
        }
        return $user_order_list;
    }

    public function current_agent_coupon_count($query_month = '')
    {
        $user = \Admin::user();
        $agent = Agent::where('admin_uid',$user->id)->first();
        $agent_id = $agent ? $agent->id : 0;
        if(empty($agent_id)) return [];

        $start_time = $end_time = 0;
        if(!empty($query_month))
        {
            $timestamp = strtotime( $query_month );
            $start_time = date( 'Y-m-1 00:00:00', $timestamp );
            $mdays = date( 't', $timestamp );
            $end_time = date( 'Y-m-' . $mdays . ' 23:59:59', $timestamp );
        }
        $first_agent_id = $second_agent_id = 0;
        if($agent->level == 1) $first_agent_id = $agent_id;
        if($agent->level == 2) $second_agent_id = $agent_id;
        //月份总卡数
        $total_card_nums = CouponCard::when($query_month,function ($query) use ($start_time,$end_time){
            return $query->whereBetween('created_at',[$start_time,$end_time]);
        })->when($first_agent_id,function ($query) use ($first_agent_id){
            return $query->where('first_class_agent_id',$first_agent_id);
        })->when($second_agent_id,function ($query) use ($second_agent_id){
            return $query->where('second_class_agent_id',$second_agent_id);
        })->count();


        $has_bind_nums = CouponCard::when($query_month,function ($query) use ($start_time,$end_time){
            return $query->whereBetween('created_at',[$start_time,$end_time]);
        })->when($first_agent_id,function ($query) use ($first_agent_id){
            return $query->where('first_class_agent_id',$first_agent_id);
        })->when($second_agent_id,function ($query) use ($second_agent_id){
            return $query->where('second_class_agent_id',$second_agent_id);
        })->where('bind_uid','>',0)->count();
        $un_bind_nums = $total_card_nums - $has_bind_nums;

        //今日-本周-本月
        $today_bind_nums = CouponCard::whereBetween('used_at',[date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59')])
            ->when($first_agent_id,function ($query) use ($first_agent_id){
                return $query->where('first_class_agent_id',$first_agent_id);
            })->when($second_agent_id,function ($query) use ($second_agent_id){
                return $query->where('second_class_agent_id',$second_agent_id);
            })->where('bind_uid','>',0)->count();
        $week_start_time = date('Y-m-d 00:00:00', strtotime("this week Monday", time()));
        $week_end_time = date('Y-m-d 23:59:59',strtotime(date('Y-m-d', strtotime("this week Sunday", time()))) + 24 * 3600 - 1);
        $week_bind_nums = CouponCard::whereBetween('used_at',[$week_start_time,$week_end_time])
            ->when($first_agent_id,function ($query) use ($first_agent_id){
                return $query->where('first_class_agent_id',$first_agent_id);
            })->when($second_agent_id,function ($query) use ($second_agent_id){
                return $query->where('second_class_agent_id',$second_agent_id);
            })->where('bind_uid','>',0)->count();
        $month_start_time = date('Y-m-d 00:00:00',mktime(0, 0, 0, date('m'), 1, date('Y')));
        $month_end_time = date('Y-m-d 23:59:59',mktime(23, 59, 59, date('m'), date('t'), date('Y')));

        $month_bind_nums = CouponCard::whereBetween('used_at',[$month_start_time,$month_end_time])
            ->when($first_agent_id,function ($query) use ($first_agent_id){
                return $query->where('first_class_agent_id',$first_agent_id);
            })->when($second_agent_id,function ($query) use ($second_agent_id){
                return $query->where('second_class_agent_id',$second_agent_id);
            })->where('bind_uid','>',0)->count();

        $card_nums_data = [
            'total_card_nums'=>$total_card_nums,
            'has_bind_nums'=>$has_bind_nums,
            'un_bind_nums'=>$un_bind_nums,
        ];
        $user_nums_data = [
            'today_bind_nums'=>$today_bind_nums,
            'week_bind_nums'=>$week_bind_nums,
            'month_bind_nums'=>$month_bind_nums,
        ];
        return [$card_nums_data,$user_nums_data];
    }

    public function lgb()
    {
        //113.981528,22.541105
       $r = Admin::find(2)->stores()->get()->pluck('name', 'id');
        dump($r);
    }
}

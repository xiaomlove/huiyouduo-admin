<?php

namespace App\Admin\Controllers;

use App\Models\Complain;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ComplainController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('举报')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Complain);
        $grid->model()->with(['user']);

        $grid->id('Id');
        $grid->column('user.name', '用户');
        $grid->target_type('举报目标类型')->display(function () {
            return $this->targetTypeName;
        });
        $grid->target_id('举报目标ID');
        $grid->status('状态')->display(function () {
            return $this->statusText;
        });
        $grid->content('内容');
        $grid->images('图片')->display(function () {
            if (!$this->images) {
                return '';
            }
            return array_map(function ($item) {
                return imageUrl($item);
            }, $this->images);
        })->image();
        $grid->created_at('时间');

        $grid->disableCreateButton();
        $grid->disableExport();

        $grid->actions(function ($actions) {
            $actions->disableEdit();
            $actions->disableView();
            $actions->disableDelete();
        });

        $grid->filter(function ($filter) {
            $filter->equal('status', '状态')->select(Complain::$statusText);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Complain::findOrFail($id));

        $show->id('Id');
        $show->uid('Uid');
        $show->target_type('Target type');
        $show->target_id('Target id');
        $show->status('Status');
        $show->content('Content');
        $show->images('Images');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Complain);

        $form->number('uid', 'Uid');
        $form->text('target_type', 'Target type');
        $form->number('target_id', 'Target id');
        $form->number('status', 'Status');
        $form->textarea('content', 'Content');
        $form->text('images', 'Images');

        return $form;
    }
}

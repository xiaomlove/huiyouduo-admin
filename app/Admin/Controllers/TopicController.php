<?php

namespace App\Admin\Controllers;

use App\Http\Requests\TopicRequest;
use App\Models\StoreActivity;
use App\Models\Topic;
use App\Models\Comment;
use App\Http\Controllers\Controller;
use App\Repositories\TopicRepository;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\MessageBag;

class TopicController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Topic);
        $grid->model()->with(['user', 'first_comment']);

        $grid->id('Id');
        $grid->column('类型')->display(function () {
            return $this->typeName;
        });
        $grid->column('user.name', '作者');
        $grid->column('first_comment.content', '内容')->display(function () {
            return mb_strimwidth(strip_tags($this->first_comment->content), 0, 100, '...', 'utf-8');
        });
        $grid->updated_at('修改时间');

        $grid->actions(function ($actions) {
            $actions->append(sprintf('<a href="%s" target="_blank">&nbsp;&nbsp;回复</a>', route('admin.comment.index', ['tid' => $actions->getKey()])));
        });

        $grid->filter(function ($filters) {
            $filters->equal('type', '类型')->select(Topic::$typeNames);
        });

        $grid->disableCreateButton();


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Topic::findOrFail($id));

        $show->created_at('创建时间');
        $show->deleted_at('Deleted at');
        $show->id('Id');
        $show->type('Type');
        $show->uid('Uid');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Topic);
        $form->select('type', '类型')->options(Topic::$typeNames)->rules(['required']);
        $form->hidden('uid', 'uid')->value(0);
        $form->multipleImage('first_comment.images', '配图')->removable()->uniqueName();
        $form->editor('first_comment.content', '内容文本')->rules(['required']);
        $form->hidden('first_comment.uid', 'uid')->value(0);
        $form->hidden('first_comment.target_type', 'target_type')->value(Topic::class);
        $form->hidden('first_comment.floor_num', 'floor_num')->value(1);

        $form->select('activity.store_id', '所属店铺')
            ->options(\Admin::user()->stores()->get()->pluck('name', 'id'))
            ->rules([sprintf('required_if:type,%s', Topic::TYPE_ACTIVITY)])
            ->help('当类型为活动信息或招聘时需要');
        $form->text('activity.title', '活动标题')->help('当类型为活动信息时需要');
        $form->image('activity.cover', '活动封面')->help('当类型为活动信息时需要');

        $form->saving(function ($form) {
            $admin = \Admin::user();
            $user = $admin->frontUser;
            if (!$user) {
                $error = new MessageBag(['title' => '业务错误', 'message' => '没有绑定前端用户，不能发帖']);
                return back()->with(compact('error'));
            }
            $form->uid = $user->id;
            $firstComment = $form->first_comment;
            $firstComment['uid'] = $user->id;
            $form->first_comment = $firstComment;
        });
        $form->saved(function ($form) {
            if ($form->type != Topic::TYPE_ACTIVITY) {
                $topic = $form->model();
                StoreActivity::where('tid', $topic->id)->delete();
            }
        });

        return $form;
    }

}

<?php

namespace App\Admin\Controllers;

use App\Models\Score;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ScoreController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Score);

        $grid->model()->orderBy('id', 'desc');

        $grid->id('Id');
        $grid->column('类型')->display(function () {
            return $this->typeText;
        });
        $grid->column('user.name', '用户');
        $grid->score('分值');
        $grid->total('合计');
        $grid->remarks('备注');
        $grid->updated_at('修改时间');

        $grid->disableActions();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Score::findOrFail($id));

        $show->created_at('创建时间');
        $show->id('Id');
        $show->info('Info');
        $show->remarks('Remarks');
        $show->score('Score');
        $show->total('Total');
        $show->type('Type');
        $show->uid('Uid');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Score);

        $form->textarea('info', 'Info');
        $form->text('remarks', 'Remarks');
        $form->number('score', 'Score');
        $form->number('total', 'Total');
        $form->number('type', 'Type');
        $form->number('uid', 'Uid');

        return $form;
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\Admin;
use App\Models\AgentsSettle;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AgentEarningsStatisticalController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AgentsSettle);

        //根据权限过滤列表
        $admin = \Admin::user();
        $isSuperAdmin = $admin->isSuperAdmin();
        $isOfficial = $admin->isOfficial();

        $cur_agent_admin_uid = 0;
        if(!$isSuperAdmin && !$isOfficial) $cur_agent_admin_uid = $admin->id;

        $grid->model()->when($cur_agent_admin_uid,function ($query) use ($cur_agent_admin_uid) {
            return $query->where([
                'is_all_settled'=>1,
                'agent_admin_uid'=>$cur_agent_admin_uid,
            ]);
        })->orderBy('id', 'desc');
        $grid->id('Id');
        $grid->day('日期');
        $grid->settle_total_price('每日收入');
        $grid->created_at('创建时间');
        $grid->updated_at('更新时间');

        $grid->disableCreateButton();
        $grid->disableActions();

        $grid->filter(function($filter){

            // 去掉默认的id过滤器
        });

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->disableDelete();
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AgentsSettle::findOrFail($id));

        $show->id('Id');
        $show->day('Day');
        $show->settle_total_price('Settle total price');
        $show->agent_admin_uid('Agent admin uid');
        $show->is_all_settled('Is all settled');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AgentsSettle);

        $form->text('day', 'Day');
        $form->decimal('settle_total_price', 'Settle total price')->default(0.00);
        $form->number('agent_admin_uid', 'Agent admin uid');
        $form->switch('is_all_settled', 'Is all settled');

        return $form;
    }
}

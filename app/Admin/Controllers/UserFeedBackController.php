<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\BatchHandle;
use App\Models\UserFeedBack;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class UserFeedBackController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UserFeedBack);

        $grid->id('Id');
        $grid->title('标题');
        $grid->contents('内容');
        $grid->is_audit("是否通过")->display(function ($is_audit) {
            $color = $is_audit ? "green" : "red";
            $is_audit_name = $is_audit ? "通过" : "拒绝";
            return "<span class=\"badge bg-$color\">$is_audit_name</span>";
        });
        $grid->created_at('创建时间');
        $grid->updated_at('修改时间');

        $grid->tools(function ($tools) {
            $tools->batch(function ($batch) {
                $batch->add('批量通过', new BatchHandle("UserFeedBack","is_audit",1,"batchHandle"));
                $batch->add('批量拒绝', new BatchHandle("UserFeedBack","is_audit",0,"batchHandle"));
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UserFeedBack::findOrFail($id));

        $show->id('Id');
        $show->title('Title');
        $show->contents('Contents');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UserFeedBack);

        $form->text('title', '标题');
        $form->textarea('contents', '内容');
        $form->radio("is_audit","是否通过")->options(['1' => '通过', '0'=> '拒绝'])->default('0');
        return $form;
    }
}

<?php

namespace App\Admin\Controllers;

use App\Models\Comment;
use App\Http\Controllers\Controller;
use App\Models\Topic;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CommentController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('帖子')
            ->description('回复列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Comment);
        $grid->model()->with(['user'])
            ->where('target_id', request('tid'))
            ->where('target_type', Topic::class)
            ->withTrashed()
            ->orderBy('id', 'asc');

        $grid->id('Id');
        $grid->column('user.name', '作者');
        $grid->column('content', '内容')->display(function () {
            return mb_strimwidth($this->content, 0, 100, '...', 'utf-8');
        });
        $grid->like_count('点赞数');
        $grid->floor_num('楼层');

        $grid->created_at('添加时间');

        $grid->actions(function ($actions) {
            $actions->disableEdit();
        });

        $grid->disableCreateButton();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Comment::with(['user'])->findOrFail($id));
        $show->id('ID');
        $show->created_at('发布时间');
        $show->user('作者', function ($user) {
            $user->setResource('admin/user');
            $user->id('UID');
            $user->name('真实姓名');
            $user->nickname('昵称');
            $user->scores('积分');
            $user->money('金钱');
            $user->panel()->tools(function ($tools) {
                $tools->disableDelete();
                $tools->disableList();
                $tools->disableEdit();
            });
        });
        $show->floor_num('楼层');
        $show->content('内容')->unescape();
        $show->images('配图')->unescape()->as(function ($images) {
            $out = '';
            foreach ((array)$images as &$img) {
                $out .= sprintf('<a href="%s" target="_blank"><img src="%s" /></a>', imageUrl($img), imageUrl($img, 'resize,w_100,h_100'));
            }
            return $out;
        });
        $show->like_count('点赞数');
        $show->reply_count('回复数');
        $show->share_count('分享数');
        $show->view_count('浏览数');
        $show->updated_at('更新时间');
        $show->deleted_at('删除时间')->label('danger');

        $show->panel()->tools(function ($tools) {
            $tools->disableList();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Comment);

        $form->textarea('content', 'Content');
        $form->number('floor_num', 'Floor num')->default(-1);
        $form->textarea('images', 'Images');
        $form->number('like_count', 'Like count');
        $form->number('pid', 'Pid');
        $form->number('reply_count', 'Reply count');
        $form->number('share_count', 'Share count');
        $form->number('target_id', 'Target id');
        $form->text('target_type', 'Target type');
        $form->number('uid', 'Uid');
        $form->number('view_count', 'View count');

        return $form;
    }
}

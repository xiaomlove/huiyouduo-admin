<?php

namespace App\Admin\Controllers;

use App\Models\CouponCardAssignLog;
use App\Http\Controllers\Controller;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CouponCardAssignLogController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CouponCardAssignLog);

        $isSuperAdmin = \Admin::user()->isSuperAdmin();
        $isOfficial = \Admin::user()->isOfficial();
        $isAgentLevelOne = \Admin::user()->isAgentLevelOne();
        $isAgentLevelTwo = \Admin::user()->isAgentLevelTwo();
        //一级代理商
        if(!$isSuperAdmin && !$isOfficial && $isAgentLevelOne) $grid->model()->where("assign_admin_uid",\Admin::user()->id);

        $grid->id('Id');
        $grid->card_no('优惠卡编号');
        $grid->column('assignAdmin.name',"分配管理员");
        $grid->assign_admin_at('分配时间');
        $grid->remark('备注');
        $grid->created_at('创建时间');
        $grid->updated_at('修改时间');

        $grid->filter(function ($filters) {
            $filters->equal('card_no', '优惠卡编号');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CouponCardAssignLog::findOrFail($id));

        $show->id('Id');
        $show->card_no('Card no');
        $show->assign_admin_uid('Assign admin uid');
        $show->assign_admin_at('Assign admin at');
        $show->remark('Remark');
        $show->created_at('创建时间');
        $show->updated_at('修改时间');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CouponCardAssignLog);

        $form->text('card_no', 'Card no');
        $form->number('assign_admin_uid', 'Assign admin uid');
        $form->datetime('assign_admin_at', 'Assign admin at')->default(date('Y-m-d H:i:s'));
        $form->text('remark', 'Remark');

        return $form;
    }
}

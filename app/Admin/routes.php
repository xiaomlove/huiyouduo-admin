<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as' => 'admin.',
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->get('lgb', 'HomeController@lgb');
    $router->get('tool', 'ToolController@index');
    $router->post('upload/image', 'UploadController@image')->name('upload.image');
    $router->post('batchHandle/batchHandle', 'BatchHandleController@batchHandle')->name("batchhandle.batchHandle");//批量操作
    $router->post('tool/token', 'ToolController@token');
    $router->resource('itemscate', 'ItemsCateController');
    $router->resource('items', 'ItemsController');
    $router->resource('user', 'UserController');
    $router->get('user/select/json', 'UserController@selectJson');
    $router->resource('topic', 'TopicController');
    $router->resource('comment', 'CommentController');
    $router->resource('couponCard', 'CouponCardController');
    $router->resource('vendor', 'AgentController');
    $router->resource('store', 'AgentStoreController');
    $router->resource('activity', 'StoreActivityController');
    $router->post('vendor-assigncard', 'AgentController@assignCouponCard')->name("vendor.assigncard");//平台分发优惠点卡给一级代理
    $router->post('vendor-assigncardsecond', 'AgentController@assignCouponCardSecond')->name("vendor.assigncardsecond");//一级代理商分发优惠点卡给二级代理
    $router->resource('banner', 'BannerController');
    $router->resource('couponCardCreateLog', 'CouponCardCreateLogController');
    $router->resource('couponCardAssignLog', 'CouponCardAssignLogController');
    $router->resource('couponCardCreateLogDetail', 'CouponCardCreateLogDetailController');
    $router->resource('officalMessage', 'OfficalMessageController');
    $router->resource('tbkOficalOrder', 'TbkOficalOrderController');
    $router->resource('config', 'ConfigController');
    $router->resource('score', 'ScoreController');
    $router->resource('lucky-draw', 'LuckyDrawController');
    $router->resource('lucky-draw-prize', 'LuckyDrawPrizeController');
    $router->resource('lucky-draw-win-log', 'LuckyDrawWinLogController');
    $router->post('lucky-draw-win-log-verify-prize', 'LuckyDrawWinLogController@verifyPrize');
    $router->resource('order-statistical', 'OrderStatisticalController');
    $router->resource('push-message-log', 'PushMessageLogController');
    $router->resource('user-feed-back', 'UserFeedBackController');
    $router->resource('complain', 'ComplainController');
    $router->resource('agents-settle', 'AgentsSettleController');
    $router->resource('earnings-statistical', 'EarningsStatisticalController');
    $router->resource('agent-earnings-statistical', 'AgentEarningsStatisticalController');
    $router->resource('user-low-score-send', 'UserLowScoreSendController');
    $router->post('agents-settle-update', 'AgentsSettleController@agentsSettleUpdate');
    $router->post('agents-settle-update-onekey', 'UserLowScoreSendController@oneKeyUserLowScoreSend');
    $router->post('vendor-bind-front-user', 'AgentController@bindFrontUser')->name('vendor.bind-front-user');

    $router->resource('withdrawal', 'WithdrawalController');
    $router->post('withdrawal-audit', 'WithdrawalController@audit')->name('withdrawal.audit');

});

<?php

namespace App\Admin\Extensions\Fields;

use Encore\Admin\Form\Field\Text;

class SmsVerifyCode extends Text
{
    protected $view = 'sms-verify-code';

    protected $script = <<<EOT
var getCodeBtn = $('#get-sms-verify-code');
var phoneInput = $('#front_user_phone');
getCodeBtn.on('click', function () {
    var phone = phoneInput.val();
    if (!phone) {
        alert('请输入手机号');
        return;
    }
    var data = {"phone": phone};
    $.post('/api/auth/sms-verify-code', data, function (response) {
        console.log(response);
        if (response.ret == 0) {
            getCodeBtn.attr("disabled", "disabled").text("发送成功，请注意查收，不要重复发送");
        } else {
            alert(response.msg);
        }   
    }, 'json');
})
EOT;

}
<?php

namespace App\Admin\Extensions\Tools;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;

class UserLowScoreSendExt extends AbstractTool
{


    public function __construct()
    {
    }
    public function render()
    {
        $url = admin_base_path('agents-settle-update-onekey');
        $csrf_token = csrf_token();
        return <<<EOT

<div id="userlowscoresend" onclick="userlowscoresend('$url','$csrf_token')" class="btn-group pull-right" style="margin-right: 10px">
    <a href="javascript:;" class="btn btn-sm btn-success">
        <i class="fa fa-save"></i>&nbsp;&nbsp;一键赠送积分
    </a>
</div>
<script>
    function userlowscoresend(url,csrf_token)
    {
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            headers: {
            'X-CSRF-TOKEN': csrf_token
        },
            success: function (data) {
                   console.log(data)
                   alert(data.msg);
                   if(data.state == 0) window.location.reload();
            }
        });
    }
</script>
EOT;
    }
    public function __toString()
    {
        return $this->render();
    }
}
<?php

namespace App\Admin\Extensions\Tools;

use Encore\Admin\Grid\Tools\BatchAction;

class BatchHandle extends BatchAction
{
    protected $model_name;
    protected $method;
    protected $set_field;
    protected $set_value;
    protected $is_delete;

    public function __construct($model_name = "",$set_field = "",$set_value = "",$method = "batchHandle",$is_delete = false)
    {
        $this->model_name = $model_name;
        $this->method = $method;
        $this->set_field = $set_field;
        $this->set_value = $set_value;
        $this->is_delete = $is_delete;
    }

    public function script()
    {
        $post_url = url("admin/batchHandle/{$this->method}");

        return <<<EOT

$('{$this->getElementClass()}').on('click', function() {
    $.ajax({
        method: 'post',
        url: '$post_url',
        data: {
            _token:LA.token,
            ids: selectedRows(),
            set_field: '{$this->set_field}',
            set_value: '{$this->set_value}',
            model_name:'{$this->model_name}',
            is_delete:'{$this->is_delete}',
        },
        success: function (data) {
            $.pjax.reload('#pjax-container');
            if(data.ret == 0) toastr.success(data.msg);
            else toastr.error(data.msg);
        }
    });
});

EOT;

    }
}
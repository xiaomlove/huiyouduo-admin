<?php

namespace App\Admin\Extensions\Actions;

use Encore\Admin\Admin;

class AuditWithdrawal
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function script()
    {
        $url = route('admin.withdrawal.audit');
        return <<<SCRIPT
var modal = $('#audit-withdrawal-modal');
var id = null;
var form = $('#audit-withdrawal-form');
$('.audit-withdrawal').on('click', function () {
    id = $(this).data('id');
    modal.modal({
        "backdrop": "static",
        "keyboard": false
    });
});
$('#audit-withdrawal-submit').on('click', function (e) {
    var data = form.serialize();
    data += "&id=" + id;
    $.ajax({
        "url": '$url',
        "method": "post",
        "data": data,
    }).done(function (response) {
        console.log(response);
        alert(response.msg);
    }).always(function (xhr, statusText) {
        console.log(xhr, statusText);
        modal.modal('hide');
    })
})
SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());

        return "<a class='btn btn-xs btn-success audit-withdrawal' data-id='{$this->id}'>审核</a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: plusev
 * Date: 2018/3/19
 * Time: 16:14
 */
namespace App\Admin\Extensions;
use App\Models\LuckyDrawPrize;
use App\Models\LuckyDrawWinLog;
use Encore\Admin\Admin;

class EntityPrizeVerifyExt
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function script()
    {
        return <<<SCRIPT

$('.grid-updatestatus-row').on('click', function () {
    var id = $(this).data('id');
    var post_url = $(this).data('post-url');
    var csrf_token = $(this).data('csrf');
    if(id <= 0)
    {
        alert('ID参数获取失败,刷新页面！');
        return false;
    }
    if(post_url == "")
    {
        alert('链接参数获取失败,刷新页面！');
        return false;
    }
    $.ajax({
        type: 'POST',
        url: post_url,
        dataType: 'json',
        data: {id:id},
        headers: {
            'X-CSRF-TOKEN': csrf_token
        },
        success: function (data) {
               console.log(data)
               alert(data.msg);
               if(data.state == 0) window.location.reload();
        }
    });
    

});
SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());
        $prize_log = LuckyDrawWinLog::find($this->id);
        if(is_null($prize_log)) return "";
        $lucky_draw_prize = LuckyDrawPrize::find($prize_log->lucky_draw_prize_id);
        if(is_null($lucky_draw_prize)) return "";
        if($lucky_draw_prize->store_id != 0) return "";//非平台才会后台核销
        if ($lucky_draw_prize->is_entity == 0) return "";
        $post_url = admin_base_path('lucky-draw-win-log-verify-prize');
        $csrf_token = csrf_token();
        return "<a class='btn btn-xs btn-danger grid-updatestatus-row' data-csrf='$csrf_token' data-post-url='$post_url'  data-id='{$this->id}'>实物核销</a>";

    }

    public function __toString()
    {
        return $this->render();
    }


}
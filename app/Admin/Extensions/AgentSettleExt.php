<?php
/**
 * Created by PhpStorm.
 * User: plusev
 * Date: 2018/3/19
 * Time: 16:14
 */
namespace App\Admin\Extensions;
use App\Models\AgentsSettle;
use Encore\Admin\Admin;

class AgentSettleExt
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function script()
    {
        return <<<SCRIPT

$('.grid-updatestatus-row').on('click', function () {
    var id = $(this).data('id');
    var settle_total_price = $(this).data('settle-total-price);
    var post_url = $(this).data('post-url');
    var csrf_token = $(this).data('csrf');
    if(id <= 0)
    {
        alert('ID参数获取失败,刷新页面！');
        return false;
    }
    if(post_url == "")
    {
        alert('链接参数获取失败,刷新页面！');
        return false;
    }
    $.ajax({
        type: 'POST',
        url: post_url,
        dataType: 'json',
        data: {id:id,settle_total_price:settle_total_price},
        headers: {
            'X-CSRF-TOKEN': csrf_token
        },
        success: function (data) {
               console.log(data)
               alert(data.msg);
               if(data.state == 0) window.location.reload();
        }
    });
    

});
SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());
        $agent_settle = AgentsSettle::find($this->id);
        $is_all_settled = $agent_settle->is_all_settled;
        if($is_all_settled) return "";
        else
        {
            $settle_total_price = $agent_settle->settle_total_price;
            $post_url = admin_base_path('agents-settle-update');
            $csrf_token = csrf_token();
            return "<a class='btn btn-xs btn-danger grid-updatestatus-row' data-csrf='$csrf_token' data-post-url='$post_url' data-settle-total-price='$settle_total_price'  data-id='{$this->id}'>结算</a>";
        }
    }

    public function __toString()
    {
        return $this->render();
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: plusev
 * Date: 2018/3/19
 * Time: 16:14
 */
namespace App\Admin\Extensions;
use Encore\Admin\Admin;

/**
 * @desc 给二级代理商分配一级代理商的优惠卡
 * Class AssignCouponCardToSecondAgent
 * @package App\Admin\Extensions
 */
class AssignCouponCardToSecondAgent
{
    protected $id;
    protected $agent_stores;

    public function __construct($id,$agent_stores)
    {
        $this->id = $id;
        $this->agent_stores = $agent_stores;
    }


    protected function script()
    {
        return <<<SCRIPT
$('.assign_coupon_cardpost_post').on('click', function () {

    var id = $(this).data('id');
    var post_url = $(this).data('post-url');
    var csrf_token = $(this).data('csrf');
    var create_card_start = $("#assign_coupon_card_"+id).find('.create_card_start').val();
    var create_card_end = $("#assign_coupon_card_"+id).find(".create_card_end").val();
    var bind_store_id = $("#assign_coupon_card_"+id).find(".bind_store_id").val();

    if(id <= 0)
    {
        alert('ID参数获取失败,刷新页面！');
        return false;
    }
    if(post_url == "")
    {
        alert('链接参数获取失败,刷新页面！');
        return false;
    }
    if(create_card_start <= 0 || create_card_end <= 0)
    {
        alert('请填写正确的优惠卡编号！');
        return false;
    }
    
    $.ajax({
        type: 'POST',
        url: post_url,
        dataType: 'json',
        data: {id:id,create_card_start:create_card_start,create_card_end:create_card_end,bind_store_id:bind_store_id},
        headers: {
            'X-CSRF-TOKEN': csrf_token
        },
        success: function (data) {
            $.pjax.reload('#pjax-container');
            if(data.ret == 0) toastr.success(data.msg);
            else toastr.error(data.msg);
            
            
            $(".modal-backdrop.fade.in").remove();
            
        }
    });
    

});
SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());

        //预约-未服务
        $style_class = "btn-danger";
        $show_name = "分配优惠卡券";
        $post_url = route('admin.vendor.assigncardsecond');
        $csrf_token = csrf_token();

        $select_store = '';
        foreach ($this->agent_stores as $store)
        {
            $select_store .= "<option value='{$store['id']}'>{$store['name']}</option>";
        }

        $appedhtml = <<<EOT
        <style>.form-group{height: 50px;}</style>
        <input type="hidden" id="max_create_nums_limit" value="0">
<div class="modal fade" id="assign_coupon_card_{$this->id}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="myModalLabel">
					$show_name <span style="font-size: 12px;color: red">(请填写连续的未分配过的编号，可充值卡管理查看)</span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
                    <label for="points_quota_input" class="col-sm-3 control-label">优惠卡开始编号</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-pencil fa-fw"></i>
                            </span>
                            <input type="text"  name="admin[name]" value="" class="form-control admin_name_ create_card_start" placeholder="如：0000000000001">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="create_nums_input" class="col-sm-3 control-label">优惠卡结束编号</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-pencil fa-fw"></i>
                            </span>
                            <input type="text" id="admin_name" name="admin[name]" value="" class="form-control admin_name_ create_card_end" placeholder="如：0000000000008">
                        </div>
                    </div>
                </div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭
				</button>
				<button type="button" data-csrf='$csrf_token' data-post-url='$post_url'  data-id='{$this->id}'  style="background:#3c8dbc" class="btn btn-primary assign_coupon_cardpost_post">
					提交
				</button>
			</div>	
			<script type="text/javascript">
            
            </script>
			
		</div>
	</div>
</div>
<a class='btn btn-xs grid-assign_coupon_card-row'   data-toggle="modal" data-target="#assign_coupon_card_{$this->id}" style="background:silver;color:#fff;font-weight:bold;">$show_name</a>
EOT;
        return $appedhtml;

    }

    public function __toString()
    {
        return $this->render();
    }


}
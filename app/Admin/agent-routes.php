<?php

use Illuminate\Routing\Router;

//自带路由
$attributes = [
    'prefix'     => config('admin.route.prefix') . '/' . config('admin.route.agent_prefix'),
    'middleware' => config('admin.route.middleware'),
    'as' => 'agent.',
];

app('router')->group($attributes, function ($router) {

    /* @var \Illuminate\Routing\Router $router */
    $router->namespace('Encore\Admin\Controllers')->group(function ($router) {

        /* @var \Illuminate\Routing\Router $router */
        $router->resource('auth/users', 'UserController');
        $router->resource('auth/roles', 'RoleController');
        $router->resource('auth/permissions', 'PermissionController');
        $router->resource('auth/menu', 'MenuController', ['except' => ['create']]);
        $router->resource('auth/logs', 'LogController', ['only' => ['index', 'destroy']]);
    });

    $authController = config('admin.auth.controller', AuthController::class);

    /* @var \Illuminate\Routing\Router $router */
    $router->get('auth/login', $authController.'@getLogin');
    $router->post('auth/login', $authController.'@postLogin');
    $router->get('auth/logout', $authController.'@getLogout');
    $router->get('auth/setting', $authController.'@getSetting');
    $router->put('auth/setting', $authController.'@putSetting');
});

//自定义开发路由

Route::group([
    'prefix'        => config('admin.route.prefix') . '/' . config('admin.route.agent_prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as' => 'agent.',
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('user', 'UserController');
    $router->resource('couponCard', 'AgentCouponCardController');
    $router->resource('vendor', 'AgentController');
});


<?php
use App\Admin\Extensions\Column\ExpandRow;
use Encore\Admin\Grid\Column;
use Encore\Admin\Form;
use App\Admin\Extensions\Fields\SmsVerifyCode;

/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */

//Encore\Admin\Form::forget(['map', 'editor']);

app('view')->prependNamespace('admin', resource_path('views/admin'));
Column::extend('expand', ExpandRow::class);
Admin::css('/css/admin.css');
Form::extend('smsVerifyCode', SmsVerifyCode::class);


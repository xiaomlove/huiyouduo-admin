<?php

namespace App\Repositories;

use App\Events\UserGeoGot;
use App\Models\AgentStore;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\District;
use Illuminate\Support\Facades\Redis;

class LBSRepository
{
    const REDIS_GEO_KEY = 'store_geo';

    /**
     * 更新店铺在 redis 位置
     */
    public static function updateStoreRedisGeo($storeId, $longitude, $latitude)
    {
        return Redis::geoAdd(self::REDIS_GEO_KEY, $longitude, $latitude, $storeId);
    }

    /**
     * 列出某点（经纬度）周边的门店
     *
     * @param $longitude 经度
     * @param $latitude 纬度
     * @param $radius 半径，默认50000
     * @param string $unit 单位，默认米
     * @param array $options
     * @return mixed
     */
    public static function listStoresAround($longitude, $latitude, $radius = 50000, $unit = 'm', $options = ['WITHDIST', 'ASC', 'count' => 10])
    {
        return Redis::geoRadius(self::REDIS_GEO_KEY, $longitude, $latitude, $radius, $unit, $options);
    }

    /**
     * 根据经纬度计算两地距离，单位 km
     *
     * @param $lat1
     * @param $lng1
     * @param $lat2
     * @param $lng2
     * @return float
     */
    public static function caleDistance($lat1, $lng1, $lat2, $lng2)
    {
        $radLat1 = deg2rad($lat1);//deg2rad()函数将角度转换为弧度
        $radLat2 = deg2rad($lat2);
        $radLng1 = deg2rad($lng1);
        $radLng2 = deg2rad($lng2);
        $a = $radLat1 - $radLat2;
        $b = $radLng1 - $radLng2;
        $s = 2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6371;
        return round($s,1);

    }


    /**
     * 获取位置信息
     *
     * @see http://lbsyun.baidu.com/index.php?title=webapi/guide/webservice-geocoding-abroad
     *
     * @param $latitude 纬度
     * @param $longitude 经度
     * @return mixed
     */
    public function getLocationInfoByGeo($latitude, $longitude)
    {
        $ak = config('lbs.baidu_ak');
        $sk = config('lbs.baidu_sk');
        if (empty($ak) || empty($sk)) {
            throw new \RuntimeException('没有设置 lbs.baidu_ak 或 lbs.baidu_sk');
        }
        $uri = "/geocoder/v2/";
        $url = "http://api.map.baidu.com{$uri}?location=%s&output=%s&ak=%s&sn=%s";
        $location = "$latitude,$longitude";
        $output = 'json';
        $queryStringArr = compact('location', 'output', 'ak');
        $sn = $this->calculateAkSn($ak, $sk, $uri, $queryStringArr);
        $target = sprintf($url, urlencode($location), $output, $ak, $sn);
        $http = new Client();
        $result = (string)$http->get($target)->getBody();
        \Log::debug(sprintf("%s, lat: %s, lng: %s, result: %s", __METHOD__, $latitude, $longitude, $result));
        $resultArr = json_decode($result, true);
        return $resultArr;
    }

    /**
     * 根据 IP 获取地址信息
     *
     * @param $ip IP地址
     * @return mixed
     */
    public function getLocationInfoByIp($ip)
    {
        $ak = config('lbs.baidu_ak');
        $sk = config('lbs.baidu_sk');
        if (empty($ak) || empty($sk)) {
            throw new \RuntimeException('没有设置 lbs.baidu_ak 或 lbs.baidu_sk');
        }
        $uri = "/location/ip";
        $url = "http://api.map.baidu.com{$uri}?ip=%s&ak=%s&sn=%s";
        $queryStringArr = compact('ip','ak');
        $sn = $this->calculateAkSn($ak, $sk, $uri, $queryStringArr);

        $target = sprintf($url, $ip, $ak, $sn);
        $http = new Client();
        $result = (string)$http->get($target)->getBody();
        \Log::debug(sprintf("%s, ip: %s, result: %s", __METHOD__, $ip, $result));
        $resultArr = json_decode($result, true);
        return $resultArr;

    }

    private function calculateAkSn($ak, $sk, $url, $queryStringArr, $method = 'GET')
    {
        if (strtoupper($method) == 'POST') {
            ksort($queryStringArr);
        }
        $queryString = http_build_query($queryStringArr);
        return md5(urlencode("{$url}?{$queryString}{$sk}"));
    }

    /**
     * @desc 根据ip获取省份
     */
    public function getUserProvinceByIp(Request $request)
    {
        $ip = $request->getClientIp();
        if(env('APP_ENV') == 'local') $ip = '116.24.152.161';
        $api_url = sprintf("http://api.map.baidu.com/location/ip?ak=TtKbapQnIC91HVqy3itK9gM86rH7bB4B&ip=%s",$ip);
        $client = new Client();
        $res = $client->request('GET', $api_url,[
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'     => 'application/json',
            ]
        ]);
        $body = (string)$res->getBody();
        $json = \GuzzleHttp\json_decode($body,true);
        if(@$json['status'] == 0)
        {
            return @$json['content']['address_detail']['province'];
        }
        return "";
    }

    /**
     * 获取用户当前所在城市。依据以下顺序：1：城市code, 2：经纬度，3：IP，4：默认城市
     *
     * @param Request $request
     * @return null
     */
    public function getUserCity(Request $request)
    {
        $latitude = $request->get('latitude');
        $longitude = $request->get('longitude');
        $code = $request->get('code');
        $defaultCityCode = config('lbs.default_city_code');
        $logPrefix = sprintf("%s, user: %s, request: %s", __METHOD__, \Auth::id(), json_encode($request->all()));
        $city = $cityCode = null;
        if ($code) {
            //传递了 code ,使用传递的
            $cityCode = $code;
            \Log::warning("$logPrefix, pass code: $code");

        } elseif (!empty($latitude) && !empty($longitude)) {
            //传递了 latitude 和 longitude 获取之
            $locationInfo = $this->getLocationInfoByGeo($latitude, $longitude);
            if (!empty($locationInfo['result']['addressComponent']['adcode'])) {
                $adCode = $locationInfo['result']['addressComponent']['adcode'];
                \Log::warning(sprintf("%s, pass latitude and longitude and success get locationInfo, adCode: %s", $logPrefix, $adCode));
                $cityCode = $adCode;
            }
        } else {
            //没传递 code 也没有 latitude 或 longitude, 先依据IP来定位
            $ip = $request->getClientIp();
            $locationInfo = $this->getLocationInfoByIp($request->getClientIp($ip));
            if (!empty($locationInfo['content']['address_detail']['city'])) {
                //这个直接是城市名
                $cityName = $locationInfo['content']['address_detail']['city'];
                \Log::warning(sprintf("%s, get city from ip: %s, cityName: %s", $logPrefix, $ip, $cityName));
                $city = District::where('name', $cityName)->first();
            }
        }

        if (!empty($city)) {
            return $city;
        }

        //这个code有可能是区的编码或者城市的编码，先假定为城市编码
        $someCity = District::where('code', $cityCode)->first();
        if (empty($someCity)) {
            //code 无效
            \Log::warning(sprintf("%s, code: %s invalid", $logPrefix, $cityCode));
        } else {
            //code 有效，判断是区还是市
            $hasDistrict = $someCity->districts()->count();
            if ($hasDistrict) {
                //是市
                $city = $someCity;
                \Log::warning("$logPrefix, code: $cityCode is city code");
            } else {
                //是区
                $city = $someCity->city;
                \Log::warning("$logPrefix, code: $cityCode is district code, get city code: {$city->code}");
                $cityCode = $city->code;
            }
        }

        if (empty($city) && $cityCode != $defaultCityCode) {
            \Log::warning("$logPrefix, finally, city: $cityCode not exists, use default: $defaultCityCode");
            $city = District::where('code', $defaultCityCode)->first();
        }

        return $city;
    }

}
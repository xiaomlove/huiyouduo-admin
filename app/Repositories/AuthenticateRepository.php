<?php

namespace App\Repositories;

use App\Models\SmsSendLog;
use Carbon\Carbon;

class AuthenticateRepository
{
    public function checkSmsVerifyCode($smsVerifyCode, $phone)
    {
        $smsLog = SmsSendLog::where("code", $smsVerifyCode)->whereNull('used_at')->isSuccess()->first();
        if (empty($smsLog))
        {
            return api("验证码 $smsVerifyCode 无效");
        }
        $now = Carbon::now()->toDateTimeString();
        if ($smsLog->expires_at < $now)
        {
            return api("验证码 $smsVerifyCode 已经过期");
        }
        if ($smsLog['phone'] != $phone)
        {
            return api("手机号 $phone 与验证码 $smsVerifyCode 不匹配");
        }

        return api(RET_OK, RET_SUCCESS_MSG, $smsLog);
    }
}
<?php

namespace App\Repositories;

use App\Models\Config;
use App\Models\WithdrawalLog;
use App\User;
use App\Models\SignInLog;
use App\Models\Score;

class UserRepository
{
    /**
     * 进行签到
     *
     * @param User $user
     * @return mixed
     */
    public function signIn(User $user)
    {
        $result = \DB::transaction(function () use ($user) {
            //插入签到记录
            $signInLog = $user->signInLogs()->create();
            //送积分
            $score = Score::assignForSignIn($user->id,Config::getSignScoreNums());
            return $signInLog;
        });

        return $result;
    }

}
<?php

namespace App\Repositories;

use App\Models\Admin;
use App\Models\Agent;
use App\Models\Role;
use Illuminate\Http\Request;

class AgentRepository
{
    public function createAgent(Request $request)
    {
        $agent = \DB::transaction(function () use ($request) {
            //先创建后台账号
            $admin = Admin::create([
                'name' => $request->admin_name,
                'username' => $request->admin_username,
                'password' => bcrypt($request->admin_password),
            ]);
            //给予代理商权限
            $admin->roles()->save(Role::agent()->firstOrFail());
            //再创建代理商
            $agentData = [
                'name' => $request->name,
                'province_code' => $request->province_code,
                'city_code' => $request->city_code,
                'district_code' => $request->district_code,
                'address' => $request->address,
                'phone' => $request->phone,
                'cover' => $request->request->get('cover'),
                'images' => $request->request->get('images', []),
                'longitude' => $request->longitude,
                'latitude' => $request->latitude,
            ];
            $agent = $admin->agent()->create($agentData);
            return $agent;
        });

        return $agent;
    }
}
<?php

namespace App\Repositories;

use App\Models\AgentStore;
use App\Models\Score;
use App\Models\StoreActivity;
use App\Models\Topic;
use App\User;

class ShareRepository
{
    public function createForTopic(Topic $topic, User $user)
    {
        $result = \DB::transaction(function () use ($topic, $user) {
            $log = $topic->shareLogs()->create([
                'uid' => $user->id,
            ]);
            Score::assignForShareTopic($user->id);
            return $log;
        });

        return $result;
    }

    public function createForStoreActivity(StoreActivity $activity, User $user)
    {
        $result = \DB::transaction(function () use ($activity, $user) {
            $log = $activity->shareLogs()->create([
                'uid' => $user->id,
            ]);
            Score::assignForShareStoreActivity($user->id);
            return $log;
        });

        return $result;
    }

    public function createForAgentStore(AgentStore $store, User $user)
    {
        $result = \DB::transaction(function () use ($store, $user) {
            $log = $store->shareLogs()->create([
                'uid' => $user->id,
            ]);
            Score::assignForShareAgentStore($user->id);
            return $log;
        });

        return $result;
    }
}
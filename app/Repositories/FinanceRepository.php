<?php

namespace App\Repositories;

use App\Models\WithdrawalLog;

class FinanceRepository
{
    public function withdrawalToAlipay(WithdrawalLog $withdrawalLog, $status, $statusText = '')
    {
        $logPrefix = __METHOD__;
        if ($withdrawalLog->status == WithdrawalLog::STATUS_SUCCESS) {
            return api(RET_OK, RET_SUCCESS_MSG, []);
        }
        if (!in_array($status, [0, 1])) {
            return api("非法参数 status: $status");
        }
        if ($status == 0) {
            $this->setFail($withdrawalLog, [], $statusText);
            return api(RET_OK, RET_SUCCESS_MSG, []);
        }

        //状态先统一设置为处理中
        $withdrawalLog->update(['status' => WithdrawalLog::STATUS_PROCESSING]);
        $config = config('pay.alipay');
        $alipay = \Pay::alipay($config);
        \Log::info("$logPrefix, alipay config: " . json_encode($config));

        $logPrefix .= ", id: " . $withdrawalLog->id;
        if ($withdrawalLog->financeAccount->uid != $withdrawalLog->uid) {
            \Log::info("$logPrefix, finance_account: {$withdrawalLog->finance_account_id} not belongs to: {$withdrawalLog->uid}");
            $statusText = '提现账户不属于申请用户';
            $this->setFail($withdrawalLog, [], $statusText);
            return api($statusText);
        }
        if ($withdrawalLog->financeAccount->type != UserFinanceAccount::TYPE_ALIPAY) {
            \Log::info("$logPrefix, financeAccount type != " . UserFinanceAccount::TYPE_ALIPAY);
            $statusText = '提现到的目标账号不是支付宝';
            $this->setFail($withdrawalLog, [
                'fail_reason' => $statusText,
                'finance_account_type' => $withdrawalLog->financeAccount->type,
            ], $statusText);
            return api($statusText);
        }

        $userMoney = $withdrawalLog->user->money;
        $withdrawalLogMoney = $withdrawalLog->money;
        if ($userMoney < $withdrawalLogMoney) {
            \Log::info("$logPrefix, user money: {$userMoney} < {$withdrawalLogMoney}");
            $statusText = '用户金额不足';
            $this->setFail($withdrawalLog, [
                'fail_reason' => $statusText,
                'user_money' => $userMoney,
                'withdrawal_log_money' => $withdrawalLogMoney,
            ], $statusText);
            return api($statusText);
        }

        try {
            $order = [
                'out_biz_no' => $withdrawalLog->out_biz_no,
                'payee_type' => 'ALIPAY_LOGONID',
                'payee_account' => $withdrawalLog->financeAccount->account,
                'amount' => $withdrawalLog->money / 100, //提现申请中的单位是分，支付宝传递的是元
            ];
            $transferResult = $alipay->transfer($order);
        } catch (\Exception $e) {
            $statusText = $e->getMessage();
            $context = [
                'exception' => get_class($e),
                'msg' => $statusText,
            ];

            \Log::info($logPrefix, $context);
            $this->setFail($withdrawalLog, $context, $statusText);
            return api($statusText);
        }
        //至此，没有异常，一般是转账成功，同步得到结果
        $result = $transferResult->toArray();
        \Log::info("$logPrefix, transferResult: " . json_encode($result));
        if (is_array($result) && isset($result['code']) && $result['code'] == 10000) {
            $this->setSuccess($withdrawalLog, $result);
            return api(RET_OK, RET_SUCCESS_MSG, []);
        } else {
            $statusText = '未知原因，转账失败';
            $this->setFail($withdrawalLog, (array)$result, $statusText);
            return api($statusText);
        }
    }

    private function setFail(WithdrawalLog $withdrawalLog, array $tradeSnapshot = [], $statusText = '')
    {
        $withdrawalLog->status = WithdrawalLog::STATUS_FAIL;
        $withdrawalLog->trade_snapshot = json_encode($tradeSnapshot, 336);
        $withdrawalLog->status_text = $statusText;
        return $withdrawalLog->save();
    }

    private function setSuccess(WithdrawalLog $withdrawalLog, array $tradeSnapshot = [], $statusText = '')
    {
        $logPrefix = __METHOD__;
        $result = \DB::transaction(function () use ($withdrawalLog, $tradeSnapshot, $statusText, $logPrefix) {
            //插入用户资金流水
            $userMoneyWaterLog = $withdrawalLog->userMoneyWaterLogs()->create([
                'uid' => $withdrawalLog->uid,
                'money' => -$withdrawalLog->money,
            ]);
            //改状态为已完成
            $withdrawalLog->update([
                'status' => WithdrawalLog::STATUS_SUCCESS,
                'status_text' => $statusText,
                'trade_snapshot' => json_encode($tradeSnapshot, 336),
                'paid_at' => $tradeSnapshot['pay_date'] ?? null,
                'alipay_trade_no' => $tradeSnapshot['order_id'] ?? null,
            ]);
            //添加用户支出记录
            $has_insert = \App\Models\EarningsStatistical::where([
                'day'=>date('Y-m-d'),
            ])->exists();
            if($has_insert)
            {
                \App\Models\EarningsStatistical::where([
                    'day'=>date('Y-m-d'),
                ])->update([
                    'spending'=>$withdrawalLog->money,
                    'remark'=>'代理商结算',
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
            }
            else
            {
                \App\Models\EarningsStatistical::create([
                    'day'=>date('Y-m-d'),
                    'spending'=>$withdrawalLog->money,
                    'remark'=>'代理商结算'
                ]);
            }
            return $userMoneyWaterLog;
        });
        \Log::info("$logPrefix, withdrawal: {$withdrawalLog->id} all done!");
        return $result;
    }
}
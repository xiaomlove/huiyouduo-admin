<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/5
 * Time: 10:19
 */
namespace App\Libraries;
use App\Models\Config;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class Taobao
{
    /**
     * @desc 超级搜索接口
     * @param string $params
     */
    public static function superSearch($params = "",$build_querys = [])
    {
        $api_url = sprintf('%svekey=%s&para=%s&pid=%s',config('vetbk.super_base_api'),config('vetbk.vetbk_key'),$params,config('vetbk.tb_official_default_pid'));
        if(!empty($build_querys))
        {
            $build_querys_params = http_build_query($build_querys);
            $api_url  = $api_url."&".$build_querys_params;
        }
        $client = new Client();
        $res = $client->request('GET', $api_url,[
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'     => 'application/json',
            ]
        ]);
        $body = (string)$res->getBody();
        $json = \GuzzleHttp\json_decode($body,true);
        if(@$json['error'] == 0)
        {
            if(@$json['data'])
            {
                $json['data']['volume'] = format_number($json['data']['volume']);
                return $json['data'];
            }
            if(@$json['result_list'])
            {
                array_walk($json['result_list'], function (&$value, $key) {
                    $value['volume'] = format_number($value['volume']);
                });
                return $json['result_list'];
            }
        }
        return [];
    }

    /**
     * @desc 淘宝热词
     * @return string
     */
    public static function getTaobaoHotKeywords()
    {
        $api_url = 'https://suggest.taobao.com/sug?area=sug_hot&wireless=2&code=utf-8&nick=&sid=null';
        $client = new Client();
        $res = $client->request('GET', $api_url,[
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'     => 'application/json',
            ]
        ]);
        $body = (string)$res->getBody();
        $json = \GuzzleHttp\json_decode($body,true);
        $key = 'taobao:hotkeyword';
        if(Redis::exists($key)) return Redis::get($key);
        else
        {
            $res_data = isset($json['querys']) ? $json['querys'][0] : '';
            if(!empty($res_data))
            {
                Redis::set($key,$res_data);
                Redis::expire($key,10*60);
            }
            return $res_data;
        }
    }

    /**
     * @desc 首页热销综合产品
     * @return array
     */
    public static function getHotProducts($page = 1,$pagesize = 20,$cate_title = '' ,$topcate = '热销',$ptype = 'all',$sort = '')
    {
        $page = max(1,$page);
        $pagesize = min($pagesize,100) ?? 20;
        if(empty($cate_title))
            $api_url = sprintf('%svekey=%s&topcate=%s&page=%s&pagesize=%s',config('vetbk.vetbk_products_api_url'),config('vetbk.vetbk_key'),$topcate,$page,$pagesize);
        else
            $api_url = sprintf('%svekey=%s&topcate=%s&subcate=%s&&page=%s&pagesize=%s',config('vetbk.vetbk_products_api_url'),config('vetbk.vetbk_key'),$topcate,$cate_title,$page,$pagesize);

        if(in_array($ptype,[0,1]) && $ptype !== 'all')
        {
            $api_url .= '&ptype='.$ptype;
        }
        //添加排序
        if(!empty($sort))
        {
            $api_url .= '&sort='.$sort;
        }
        $client = new Client();
        $res = $client->request('GET', $api_url,[
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'     => 'application/json',
            ]
        ]);
        $body = (string)$res->getBody();
        $json = \GuzzleHttp\json_decode($body,true);
        if(@$json['error'] == 0)
        {
            if(@$json['data'])
            {
                array_walk($json['data'], function (&$value, $key) {
                    $value['volume'] = format_number($value['volume']);
                });
                return $json['data'];
            }
        }
        return [];
    }
    /**
     * @desc 首页热销综合产品
     * @return array
     */
    public static function getPinTuan($page = 1,$pagesize = 20)
    {
        $page = max(1,$page);
        $pagesize = min($pagesize,100) ?? 20;
        $api_url = sprintf('%svekey=%s&pid=%s&page_no=%s&page_size=%s',config('vetbk.vetbk_pintuan_api_url'),config('vetbk.vetbk_key'),config('vetbk.tb_official_default_pid'),$page,$pagesize);
        $client = new Client();
        $res = $client->request('GET', $api_url,[
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'     => 'application/json',
            ]
        ]);
        $body = (string)$res->getBody();
        $json = \GuzzleHttp\json_decode($body,true);
        if(@$json['error'] == 0)
        {
            if(@$json['data'])
            {
                array_walk($json['data'], function (&$value, $key) {
                    $value['stock'] = format_number($value['stock']);
                });
                return $json['data'];
            }
        }
        return [];
    }

    /**
     * @desc 获取高佣金链接
     * @param int $item_id
     */
    public static function hcapiItem($item_id = 0,$pid = 0)
    {
        $api_uri = sprintf("%svekey=%s&para=%s&pid=%s&notkl=1&noshortlink=1",config('vetbk.vetbk_hcapi_api_url'),config('vetbk.vetbk_key'),$item_id,$pid);
        $client = new Client();
        $res = $client->request('GET', $api_uri,[
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'     => 'application/json',
            ]
        ]);
        $body = (string)$res->getBody();
        $json = \GuzzleHttp\json_decode($body,true);
        if(@$json['error'] == 0)
        {
            if(@$json['data']) return $json['data'];
        }
        return [];

    }

    /**
     * @desc 根据优惠券正则提取优惠券金额
     * @param string $coupon_info
     */
    public static function getCouponPrice($coupon_info = "")
    {
        $preg = '/.*?减(\d+)元/';
        preg_match_all($preg,$coupon_info,$match);
        return $match[1][0] ?? 0;
    }
    /**
     * @desc 获取淘宝详情图片数据
     * @param int $goods_id
     */
    public static function getGoodsDetail($goods_id = 0)
    {
        if(!$goods_id) return "";
        $api_list_map = [
            'getGoodsDetailByACS',
            'getGoodsDetailByMdetail',
            'getGoodsDetailByH5api',
            'getGoodsDetailByHws',
            'getGoodsDetailByHuge98'
        ];
        $api_switch = mt_rand(0,2);
        $api_method = $api_list_map[$api_switch];
        return self::$api_method($goods_id);
//        return self::getGoodsDetailByHuge98($goods_id);
    }
    /**
     * 获取淘宝详情图片数据
     * @param $goods_id
     */
    private static function getGoodsDetailByHuge98($goods_id)
    {
        $url = 'http://w.huge98.top/addons/bsht_tbk/iidpic.php?&iid='.$goods_id;
        $response = file_get_contents($url);
        $body = json_decode($response,true);
        if($body['ret'] != -1)
        {
            $data = @$body['data']['images'] ?? [];
            if($data && is_array($data))
            {
                foreach ($data as $k=>$v)
                {
                    if(strpos($v,".jpg") === false) unset($data[$k]);
                }
            }
            return empty($data) ? "" : json_encode(array_values($data));
        }
        else
            return "";
    }

    /**
     * 获取淘宝详情图片数据
     * @param $goods_id
     */
    private static function getGoodsDetailByHws($goods_id)
    {
        $url = 'http://hws.m.taobao.com/cache/wdesc/5.0?id='.$goods_id;
        $body = file_get_contents($url);
        if(!empty($body))
        {
            $preg = '/.*?tfsContent.*?\'(.*?),.*?/';
            preg_match_all($preg,$body,$match);
            $pcDescContent = isset($match[1][0]) ? $match[1][0] : "";
            if(empty($pcDescContent)) return "";
            else
            {
                $preg = '/.*?src="(.*?)".*?align="absmiddle".*?/';
                preg_match_all($preg,$pcDescContent,$match);
                $detail_pic_arr = $match[1];
                if($detail_pic_arr && is_array($detail_pic_arr))
                {
                    foreach ($detail_pic_arr as $k=>$v)
                    {
                        if(strpos($v,".jpg") === false) unset($detail_pic_arr[$k]);
                    }
                }
                return empty($detail_pic_arr) ? "" : json_encode(array_values($detail_pic_arr));
            }

        }
        else
            return "";
    }

    /**
     * @desc 获取淘宝详情图片数据
     * @param int $goods_id
     */
    private static function getGoodsDetailByH5api($goods_id)
    {
        $url = 'http://h5api.m.taobao.com/h5/mtop.taobao.detail.getdesc/6.0/?data={%22id%22:%22#goods_id%22}';
        $api_url = str_replace("#goods_id",$goods_id,$url);
        $response = file_get_contents($api_url);
        $body = json_decode($response,true);
        if($body['ret'] != -1)
        {
            $pcDescContent = isset($body['data']['pcDescContent'])?$body['data']['pcDescContent'] : "";
            if(empty($pcDescContent)) return "";

            $preg = '/.*?src="(.*?)".*?align="absmiddle".*?/';
            preg_match_all($preg,$pcDescContent,$match);
            $detail_pic_arr = $match[1];
            if($detail_pic_arr && is_array($detail_pic_arr))
            {
                foreach ($detail_pic_arr as $k=>$v)
                {
                    if(strpos($v,".jpg") === false) unset($detail_pic_arr[$k]);
                }
            }
            return empty($detail_pic_arr) ? "" : json_encode(array_values($detail_pic_arr));
        }
        else
            return "";

    }

    /**
     * @desc 根据mdetail 获取详情数据
     * @param $goods_id
     */
    private static function getGoodsDetailByMdetail($goods_id)
    {
        $detail_pic_arr = [];
        $url = 'https://mdetail.tmall.com/templates/pages/desc?id='.$goods_id;
        $body = file_get_contents($url);
        if(!empty($body))
        {
            if(strpos($body,"newWapDescJson") !== false)
                $preg = "/.*?descUrl\":\"(.*?)\",\"newWapDescJson.*?/";
            else
                $preg = "/.*?descUrl\":\"(.*?)\"}}.*?/";
            preg_match_all($preg,$body,$match);
            $pic_url_data = $match[1][0] ?? "";
            if($pic_url_data)
            {
                $pic_url_data = file_get_contents(str_replace("//dsc","http://dsc",$pic_url_data));
                if($pic_url_data)
                {
                    $preg = '/.*?src="(.*?)".*?align="absmiddle".*?/';
                    preg_match_all($preg,$pic_url_data,$match);
                    $detail_pic_arr = $match[1];
                    if($detail_pic_arr && is_array($detail_pic_arr))
                    {
                        foreach ($detail_pic_arr as $k=>$v)
                        {
                            if(strpos($v,".jpg") === false) unset($detail_pic_arr[$k]);
                        }
                    }
                }
            }
        }
        return empty($detail_pic_arr) ? "" : json_encode(array_values($detail_pic_arr));
    }
    /**
     * @desc 根据acs获取详情数据
     */
    private static function getGoodsDetailByACS($goods_id)
    {
        $url = 'https://acs.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?data={%22itemNumId%22%3A%22#goods_id%22}';
        $api_url = str_replace("#goods_id",$goods_id,$url);
        $response = file_get_contents($api_url);
        $body = json_decode($response,true);
        if($body['ret'] != -1)
        {
            $detail_pic_arr = [];
            $moduleDescUrl = @$body['data']['item']['moduleDescUrl'];
            if($moduleDescUrl)
            {
                //tmall goods
                $moduleDescUrlData = file_get_contents(str_replace("//hws","http://hws",$moduleDescUrl));
                $moduleDescUrlData = json_decode($moduleDescUrlData,true);
                if($moduleDescUrlData['ret'] != -1)
                {
                    $children_data = $moduleDescUrlData['data']['children'];
                    if(!empty($children_data))
                    {
                        foreach ($children_data as $k=>$v)
                        {
                            if(strpos($v['ID'],"detail_pic_") !== false && isset($v['params']['picUrl']))
                            {
                                array_push($detail_pic_arr,$v['params']['picUrl']);
                            }
                        }
                    }
                }
            }
            else
                $detail_pic_arr = self::getGoodsDetailByMdetail($goods_id);
            //
            return empty($detail_pic_arr) ? "" : (is_array($detail_pic_arr) ? json_encode($detail_pic_arr) : $detail_pic_arr);
        }
        return "";

    }

    /**
     * @desc 用户的预估佣金
     * @param int $zk_final_price
     * @param int $commission_rate
     */
    public static function forecastCommission($zk_final_price = 0,$commission_rate = 0)
    {
        if(empty($zk_final_price)) return 0;
        else
        {
            return sprintf("%.2f",$zk_final_price * $commission_rate * 0.89 * (Config::getUserCommissionPercent() / 100));
        }
    }
}
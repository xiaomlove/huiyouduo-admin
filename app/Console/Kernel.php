<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        //$schedule->command('vegoods:crawler')->timezone('Asia/Shanghai')->dailyAt('00:00');
        $schedule->command('Agents:Settle')->timezone('Asia/Shanghai')->monthlyOn(25,'00:00');//每月25号 0 晨运行
        $schedule->command('downorder:minute')->timezone('Asia/Shanghai')->everyMinute();//每分钟采集订单
        $schedule->command('Earnings:Statistical')->timezone('Asia/Shanghai')->monthlyOn(25,'2:00');//每月25号 2点运行
        $schedule->command('generate:manycounpons')->timezone('Asia/Shanghai')->dailyAt('00:00');//每天0晨
        $schedule->command('Order:Statistical')->timezone('Asia/Shanghai')->hourly();
        $schedule->command('update:yestody:order')->timezone('Asia/Shanghai')->cron('*/20 * * * *');;//每20分钟
        $schedule->command('create:pid')->timezone('Asia/Shanghai')->dailyAt('00:00');//每天0晨
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

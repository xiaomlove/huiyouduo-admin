<?php

namespace App\Console\Commands;

use App\Models\LastmonthCaijiLog;
use App\Models\TbkOficalOrder;
use Illuminate\Console\Command;
use Cache;
use Carbon\Carbon;
use GuzzleHttp\Client;

class UpdateLastMonthTbkOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:lastmonthorder
        {--id=      : 当前编号}

        {--max=     : 最大线程}

        {--sleep=   : 休眠多少毫秒}

        {--debug=   : 是否调试模式}
    ';
    protected $id;
    protected $max;
    protected $sleep;
    protected $debug;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update tbk lasmonth order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date("d");
//        if($date < 22 || $date > 28) return;

        $this->id       = $this->option('id') ?? '00';

        $this->max      = $this->option('max') ?? 1;

        $this->sleep    = $this->option('sleep') ?? 12000;

        $this->debug    = $this->option('debug') ?? false;

//        if ($this->id > $this->max) return true;

        while (true) $this->doRun($date);

    }

    /**
     *
     * @param int $taskId
     * @return boolean
     */

    protected function doRun($date)
    {
        //$time   = (new Carbon)->format('Y-m-d H:i:s.u');
//        if($date < 22 || $date > 28) return;
        $this->update_last_month_order();

        $this->wait($this->sleep);

    }

    //22--28 12秒查一次 一分钟查五次
    protected function update_last_month_order()
    {
        $last_month_first = strtotime(date('Y-m-31 00:00:00', strtotime('-1 month')));
//        $last_month_last =  strtotime(date('Y-m-d',$last_month_first)."+1 month -1 day");

//        $diff_time = time() - strtotime(date("Y-m-22 00:00:00"));
//
//        if($diff_time < 0) return;
//
//        $nums = $diff_time / 12; //这个次数还是以存数据库为准  时间来判断不准

        $this_month_has_caiji = LastmonthCaijiLog::where("caiji_date",date("Y-m"))->first();
        if(!$this_month_has_caiji)
        {
            $nums = 0;
            LastmonthCaijiLog::create([
                'caiji_date'=>date("Y-m"),
                'nums'=>1
            ]);
        }
        else
        {
            $nums = $this_month_has_caiji->nums;
            $this_month_has_caiji->increment("nums",1);
        }

//        echo date("Y-m-d H:i:s",$last_month_first + $nums * 20 * 60)."\r\n";

        $start_date = urlencode(date("Y-m-d H:i:s",$last_month_first + $nums * 1200));
        $is_continue = 0;
        $page = 1;
        $to_insert_data = [];
        $to_update_data = [];

        do {
            $api_url = sprintf('http://apiorder.vephp.com/order?vekey=%s&start_time=%s&page_no=%s',env('VETBK_KEY'),$start_date,$page);
            $this->info($api_url);
            $client = new Client();
            $res = $client->request('GET', $api_url,[
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept'     => 'application/json',
                ]
            ]);
            $body = (string)$res->getBody();
            $json = \GuzzleHttp\json_decode($body,true);

            //如果data数据==100 继续第二页
            if(@$json['error'] == 0)
            {
                if($json['data'])
                {
                    if(count($json['data']) == 100)
                    {
                        $is_continue = 1;
                        $page++;
                    }
                    //判断该记录是否在库里 如果在 直接更新数据
                    foreach ($json['data'] as $k=>$v)
                    {
                        $this_item_exists_in_table = TbkOficalOrder::where([
                            "num_iid"=>$v['num_iid'],
                            "trade_id"=>$v['trade_id'],
                            "trade_parent_id"=>$v['trade_parent_id'],
                        ])->exists();
                        if($this_item_exists_in_table)
                        {
                            //更新数据
                            $v['updated_at'] = date('Y-m-d H:i:s');
                            $to_update_data[] = $v;
                        }
                        else
                        {
                            $v['created_at'] = date('Y-m-d H:i:s');
                            $v['updated_at'] = date('Y-m-d H:i:s');
                            $to_insert_data[] = $v;
                        }
                    }
                }
            }

        } while ($is_continue);

        if(!empty($to_insert_data)) TbkOficalOrder::insert($to_insert_data);
        if(!empty($to_update_data)) app(TbkOficalOrder::class)->updateBatch($to_update_data);
    }

    /**
     * 毫秒
     * @param string $time
     */

    protected function wait($time)
    {
        $wait = $time * 1000;
        usleep($wait);
    }



    protected function logger($message)
    {
        if($this->debug)
        {
            $time   = (new Carbon)->format('Y-m-d H:i:s.u');
            $this->line(array_get($message, 'message') .' - '. $time);
        }
        logger()->stack(['task'])->debug(null, $message);
    }
}

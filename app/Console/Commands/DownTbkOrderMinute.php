<?php

namespace App\Console\Commands;

use App\Models\TbkOficalOrder;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class DownTbkOrderMinute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'downorder:minute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每分钟同步订单数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $before_twenty_time = urlencode(date('Y-m-d H:i:s',time() - 20 * 60));
        if(env('APP_ENV') == 'local') $before_twenty_time = '2018-11-16+14%3a10%3a00';

        $is_continue = 0;
        $page = 1;
        $to_insert_data = [];
        $to_update_data = [];

        do {
            $api_url = sprintf('http://apiorder.vephp.com/order?vekey=%s&start_time=%s&page_no=%s',env('VETBK_KEY'),$before_twenty_time,$page);
            $client = new Client();
            $res = $client->request('GET', $api_url,[
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept'     => 'application/json',
                ]
            ]);
            $body = (string)$res->getBody();
            $json = \GuzzleHttp\json_decode($body,true);
            //如果data数据==100 继续第二页
            if(@$json['error'] == 0)
            {
                if($json['data'])
                {
                    if(count($json['data']) == 100)
                    {
                        $is_continue = 1;
                        $page++;
                    }
                    //判断该记录是否在库里 如果在 直接更新数据
                    foreach ($json['data'] as $k=>$v)
                    {
                        $this_item_exists_in_table = TbkOficalOrder::where([
                            "num_iid"=>$v['num_iid'],
                            "trade_id"=>$v['trade_id'],
                            "trade_parent_id"=>$v['trade_parent_id'],
                        ])->exists();
                        if($this_item_exists_in_table)
                        {
                            //更新数据
                            $v['updated_at'] = date('Y-m-d H:i:s');
                            $to_update_data[] = $v;
                        }
                        else
                        {
                            $v['created_at'] = date('Y-m-d H:i:s');
                            $v['updated_at'] = date('Y-m-d H:i:s');
                            $to_insert_data[] = $v;
                        }
                    }
                }
            }

        } while ($is_continue);

        if(!empty($to_insert_data)) TbkOficalOrder::insert($to_insert_data);
        if(!empty($to_update_data)) app(TbkOficalOrder::class)->updateBatch($to_update_data);

        $this->info("excute success");
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TbkOficalOrder;

class EarningsStatistical extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Earnings:Statistical';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //平台的收益收入 + 支出 (就是结算花费的)
        $start_time = date("Y-m-01 00:00:00", time());
        $end_time = date('Y-m-t 23:59:59', time());
        $data = TbkOficalOrder::where(['tk_status'=>2])
            ->selectRaw('DATE_FORMAT(earning_time,"%Y-%m-%d") as day')
            ->selectRaw('SUM(commission * 0.89) as income')
            ->whereBetween('earning_time',[$start_time,$end_time])
            ->groupBy('day')
            ->get()->toArray();
        if(!empty($data))
        {
            foreach ($data as $k=>$v)
            {
                $has_insert = \App\Models\EarningsStatistical::where([
                    'day'=>$v['day'],
                ])->exists();
                if($has_insert)
                {
                    $v['updated_at'] = date('Y-m-d H:i:s');
                    \App\Models\EarningsStatistical::where([
                        'day'=>$v['day'],
                    ])->update($v);
                }
                else
                {
                    $v['remark'] = '平台收入';
                    \App\Models\EarningsStatistical::create($v);
                }
            }
        }
    }
}

<?php

namespace App\Console\Commands;

use App\Models\TaobaoPids;
use Illuminate\Console\Command;

class CreateTbPidsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:pid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create pid';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for ($i=1;$i<=1000;$i++)
        {
            TaobaoPids::create([
                'adzone_id'=>74608550379,
                'site_id'=>242400174,
                'pid'=>"mm_279000022_242400174_74608550379",
            ]);
        }
        $this->info("excute success");
    }
}

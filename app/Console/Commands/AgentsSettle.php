<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Admin;
use App\Models\Agent;
use App\Models\TbkOficalOrder;
use App\User;
use App\Libraries\Taobao;
use App\Models\TaobaoPids;

class AgentsSettle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Agents:Settle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //代理商结算 结算上个月1-30
        $start_time = date("Y-m-01 00:00:00", time());
        $end_time = date('Y-m-t 23:59:59', time());
        $user = User::all()->toArray();
        $agents = Agent::all()->toArray();
        if(!empty($agents))
        {
            foreach ($agents as $k=>$v)
            {
                //代理商没有绑定uid 无法关联用户订单
                $agents_admin = Admin::find($v['admin_uid']);
                if(!$agents_admin) continue;
                $agents_admin_front_uid = $agents_admin->front_uid;
                //获取代理商的子孙uid
                $agents_admin_front_uid_childrens = subtree($user,$agents_admin_front_uid);
                if(empty($agents_admin_front_uid_childrens)) continue;
                $agents_order = [];
                foreach ($agents_admin_front_uid_childrens as $kk=>$vv)
                {
                    $agents_order = array_merge($agents_order,$this->user_order($vv['id'],$vv['tb_uid']));
                }
                if(!empty($agents_order))
                {
                    $sum_commission = array_sum(array_map(create_function('$val', 'return $val["commission_cal"];'), $agents_order));
                    if($v['level'] == 1) $sum_commission = $sum_commission * $v['commission_percent'];
                    elseif ($v['level'] == 2)
                    {
                        $parent_agent = Agent::find($v['parent_id']);
                        $sum_commission = $sum_commission * $parent_agent->commission_percent * $v['commission_percent'];
                    }
                    $has_insert = \App\Models\AgentsSettle::where([
                        'day'=>date('Y-m'),
                        'agent_admin_uid'=>$v['admin_uid']
                    ])->exists();
                    if($has_insert)
                    {
                        \App\Models\AgentsSettle::where([
                            'day'=>date('Y-m'),
                            'agent_admin_uid'=>$v['admin_uid']
                        ])->update([
                            'day'=>date('Y-m-d'),
                            'settle_total_price'=>$sum_commission,
                            'agent_admin_uid'=>$v['admin_uid']
                        ]);
                    }
                    else
                    {
                        \App\Models\AgentsSettle::create([
                            'day'=>date('Y-m'),
                            'commission_count'=>$sum_commission,
                            'agent_admin_uid'=>$v['admin_uid']
                        ]);
                    }

                }
            }
        }
    }

    /**
     * @desc 用户的订单列表
     * @param int $uid
     * @return array
     */
    protected function user_order($uid = 0,$tb_uid = 0)
    {
        if(empty($uid)) return;
        $user_order_list = [];
        $start_time = date("Y-m-01 00:00:00", time());
        $end_time = date('Y-m-t 23:59:59', time());
        if(!empty($tb_uid))
        {
            //这个是怎么筛选订单的 uid  和 订单 后6位 匹配
            $user_order_list = TbkOficalOrder::where([
                'tk_status'=>2,
            ])->whereBetween('earning_time',[$start_time,$end_time])
              ->select(
                \DB::raw('commission * 0.89  as commission_cal'),
                'trade_parent_id'
              )->get()->toArray();
            if(!empty($user_order_list))
            {
                foreach ($user_order_list as $k=>$v)
                {
                    if($tb_uid != handle_tbk_trade_parent_id($v['trade_parent_id']))
                    {
                        unset($user_order_list[$k]);
                    }
                    else
                    {
                        unset($user_order_list[$k]['trade_parent_id']);
                    }
                }
            }
        }
        else
        {
            //这种是绑定pid的用户
            $tb_pid_info = TaobaoPids::where("uid",$uid)->first();
            if($tb_pid_info)
            {
                $adzone_id = $tb_pid_info->adzone_id;
                $site_id = $tb_pid_info->site_id;
                $user_order_list = TbkOficalOrder::where([
                    "adzone_id"=>$adzone_id,
                    "site_id"=>$site_id,
                    'tk_status'=>2
                ])->whereBetween('earning_time',[$start_time,$end_time])
                 ->select(
                    \DB::raw('commission * 0.89 as commission_cal')
                )->get()->toArray();//commission
            }
        }
        return $user_order_list;
    }
}

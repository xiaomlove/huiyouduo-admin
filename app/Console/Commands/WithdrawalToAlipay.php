<?php

namespace App\Console\Commands;

use App\Models\UserFinanceAccount;
use App\Repositories\UserRepository;
use Illuminate\Console\Command;
use App\Models\WithdrawalLog;
use App\User;

class WithdrawalToAlipay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'withdrawal:alipay';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '处理前端用户的提现申请';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logPrefix = __METHOD__;
        $withdrawalLogs = WithdrawalLog::with('financeAccount', 'user')
            ->channelFront()->statusInit()->get();
        if ($withdrawalLogs->isEmpty()) {
            \Log::info("$logPrefix, no results to handle.");
            return;
        }
        $ids = $withdrawalLogs->pluck('id');
        \Log::info("$logPrefix, going to handle: " . $ids->implode(','));
        //状态先统一设置为处理中
        WithdrawalLog::whereIn('id', $ids->toArray())->update(['status' => WithdrawalLog::STATUS_PROCESSING]);
        $config = config('pay.alipay');
        $alipay = \Pay::alipay($config);
        \Log::info("$logPrefix, alipay config: " . json_encode($config));
        $withdrawalLogs->each(function ($withdrawalLog) use ($logPrefix, $alipay) {
            $logPrefix .= ", id: " . $withdrawalLog->id;
            if ($withdrawalLog->financeAccount->uid != $withdrawalLog->uid) {
                \Log::info("$logPrefix, finance_account: {$withdrawalLog->finance_account_id} not belongs to: {$withdrawalLog->uid}");
                return true;
            }
            $userMoney = $withdrawalLog->user->money;
            $withdrawalLogMoney = $withdrawalLog->money;
            if ($userMoney < $withdrawalLogMoney) {
                \Log::info("$logPrefix, user money: {$userMoney} < {$withdrawalLogMoney}");
                $this->setFail($withdrawalLog, [
                    'fail_reason' => "用户金额不足",
                    'user_money' => $userMoney,
                    'withdrawal_log_money' => $withdrawalLogMoney,
                ]);
                return true;
            }
            if ($withdrawalLog->financeAccount->type != UserFinanceAccount::TYPE_ALIPAY) {
                \Log::info("$logPrefix, financeAccount type != " . UserFinanceAccount::TYPE_ALIPAY);
                $this->setFail($withdrawalLog, [
                    'fail_reason' => "提现到的目标账号不是支付宝",
                    'finance_account_type' => $withdrawalLog->financeAccount->type,
                ]);
                return true;
            }
            try {
                $order = [
                    'out_biz_no' => $withdrawalLog->out_biz_no,
                    'payee_type' => 'ALIPAY_LOGONID',
                    'payee_account' => $withdrawalLog->financeAccount->account,
                    'amount' => $withdrawalLog->money / 100, //提现申请中的单位是分，支付宝传递的是元
                ];
                $transferResult = $alipay->transfer($order);
            } catch (\Exception $e) {
                $context = [
                    'exception' => get_class($e),
                    'msg' => $e->getMessage(),
                ];
                \Log::info($logPrefix, $context);
                $this->setFail($withdrawalLog, $context);
                return true;
            }
            //至此，没有异常，一般是转账成功，同步得到结果
            $result = $transferResult->toArray();
            \Log::info("$logPrefix, transferResult: " . json_encode($result));
            if (is_array($result) && isset($result['code']) && $result['code'] == 10000) {
                $this->setSuccess($withdrawalLog, $result);
            } else {
                $this->setFail($withdrawalLog, (array)$result);
                return true;
            }

        });
    }

    private function setFail(WithdrawalLog $withdrawalLog, array $tradeSnapshot = [])
    {
        $withdrawalLog->status = WithdrawalLog::STATUS_FAIL;
        $withdrawalLog->trade_snapshot = json_encode($tradeSnapshot, 336);
        return $withdrawalLog->save();
    }

    private function setSuccess(WithdrawalLog $withdrawalLog, array $tradeSnapshot = [])
    {
        $logPrefix = __METHOD__;
        $result = \DB::transaction(function () use ($withdrawalLog, $tradeSnapshot, $logPrefix) {
            //插入用户资金流水
            $userMoneyWaterLog = $withdrawalLog->userMoneyWaterLogs()->create([
                'uid' => $withdrawalLog->uid,
                'money' => -$withdrawalLog->money,
            ]);
            //改状态为已完成
            $withdrawalLog->update([
                'status' => WithdrawalLog::STATUS_SUCCESS,
                'trade_snapshot' => json_encode($tradeSnapshot, 336),
                'paid_at' => $tradeSnapshot['pay_date'] ?? null,
                'alipay_trade_no' => $tradeSnapshot['order_id'] ?? null,
            ]);

            return $userMoneyWaterLog;
        });
        \Log::info("$logPrefix, withdrawal: {$withdrawalLog->id} all done!");
        return $result;
    }

}

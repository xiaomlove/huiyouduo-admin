<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class GenerateManyCounponNumbers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:manycounpons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate many unique counpon numbers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coupon_card_no_redis_key = env('COUPON_CARD_NO_REDIS_KEY');
        $coupon_card_pub_redis_key = env('COUPON_CARD_PUB_KEY_REDIS_KEY');

        $coupon_card_no_redis_data_length = Redis::zcard($coupon_card_no_redis_key);
        $coupon_card_pub_redis_data_length = Redis::scard($coupon_card_pub_redis_key);
        $max_create_num = 100000;//100000 充足的量 不必提前创建随机值
        $create_num_per = 50000;
        //$max_create_num = 100;//100000 充足的量 不必提前创建随机值
        //$create_num_per = 20;
        //批次有顺序的
        if($coupon_card_no_redis_data_length < $max_create_num)
        {
            if($coupon_card_no_redis_data_length == 0)
            {
                //第一次创建
                $muilt_card_no_list = [];
                for ($i=1;$i<=$create_num_per;$i++)
                {
                    $muilt_card_no_list[] = $i;
                    $muilt_card_no_list[] = str_pad($i,10,0,STR_PAD_LEFT);
                }
                Redis::zadd($coupon_card_no_redis_key,...$muilt_card_no_list);
            }
            else
            {
                //第二次创建
                $last_end_value = Redis::zrange($coupon_card_no_redis_key,$coupon_card_no_redis_data_length - 1,-1);
                if(empty($last_end_value))
                {
                    $this->info("excute error");
                    return false;
                }
                $last_end_value = $last_end_value[0];
                $start = $last_end_value + 1;
                $end = $start + $create_num_per;
                $muilt_card_no_list = [];
                for ($i=$start;$i<$end;$i++)
                {
                    $muilt_card_no_list[] = $i;
                    $muilt_card_no_list[] = str_pad($i,10,0,STR_PAD_LEFT);
                }
                Redis::zadd($coupon_card_no_redis_key,...$muilt_card_no_list);
            }
        }
        //dd(Redis::zrange($coupon_card_no_redis_key,0,-1));
        if($coupon_card_pub_redis_data_length < $max_create_num)
        {
            $muilt_card_pub_list = [];
            for ($i=0;$i<$max_create_num;$i++) $muilt_card_pub_list[] = generate_random_int(9);
            Redis::sadd($coupon_card_pub_redis_key,...$muilt_card_pub_list);
        }
        $this->info("excute success");

    }
}

<?php

namespace App\Listeners;

use App\Events\UserLogin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class DeleteOldToken
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogin  $event
     * @return void
     */
    public function handle(UserLogin $event)
    {
        $user = $event->user;
        //删除旧token
        $oneMonthAgo = Carbon::now()->addMonths(-1)->toDateTimeString();
        $user->tokens()->where("created_at", "<", $oneMonthAgo)->delete();//删除一个月前的token
    }
}

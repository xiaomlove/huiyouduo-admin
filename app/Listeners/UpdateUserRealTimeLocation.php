<?php

namespace App\Listeners;

use App\Events\UserLogin;
use App\Models\UserRealTimeLocation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateUserRealTimeLocation implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogin  $event
     * @return void
     */
    public function handle($event)
    {
        $user = $event->user;
        \Log::info(__METHOD__, $user->toArray());
        if ($user->longitude && $user->latitude) {
            UserRealTimeLocation::updateByGeo($user->id, $user->longitude, $user->latitude);
        } elseif ($user->login_ip) {
            UserRealTimeLocation::updateByIp($user->id, $user->login_ip);
        }
    }
}

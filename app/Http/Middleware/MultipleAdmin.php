<?php

namespace App\Http\Middleware;

use App\Models\Admin;
use App\Models\Agent;
use Closure;
use Illuminate\Support\Str;

class MultipleAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Admin::user();
        //代理商强制绑定前端用户 //
        if($user)
        {
            if(empty($user->front_uid) && (strpos($request->url(),'auth/logout') === false) && !$user->isSuperior() &&  !in_array(\Route::currentRouteName(),['admin.vendor.bind-front-user','admin.vendor.show']))
            {
                //http://veykoo.com:7777/admin/vendor/4
                $agent = Agent::where('admin_uid',$user->id)->first();
                if($agent)
                {
                    admin_error('代理商发券前，请务必绑定您的APP前端用户，否则无法计算收益');
                    return redirect(admin_base_path('vendor').'/'.$agent->id);
                }
            }
        }

        return $next($request);
    }
}

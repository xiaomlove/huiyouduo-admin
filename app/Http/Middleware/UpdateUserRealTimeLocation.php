<?php

namespace App\Http\Middleware;

use App\Events\UserGeoGot;
use App\User;
use Closure;

class UpdateUserRealTimeLocation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $user = $request->user();
        \Log::info(__METHOD__, [$request->fullUrl(), $user instanceof User ? $user->toArray() : null, $request->all(), $response->getContent()]);
        if ($user && $request->longitude && $request->latitude) {
            $user->longitude = $request->longitude;
            $user->latitude = $request->latitude;
            event(new UserGeoGot($user));
        }
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LuckyDrawWinLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->whenLoaded('user')),
            'prize' => new LuckyDrawPrizeResource($this->whenLoaded('luckyDrawPrize')),
            'created_at' => $this->created_at->format('m/d H:i'),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserFinanceAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'account' => $this->account,
            'bind_username' => $this->bind_username,
            'bind_phone' => substr_replace($this->bind_phone, '****', 3, 4),
            'bind_organization' => (string)$this->bind_organization,
        ];
    }
}

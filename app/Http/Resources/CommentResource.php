<?php

namespace App\Http\Resources;

use App\Models\Topic;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $shouldCutRoutes = ['api.topic.index'];
        if (in_array(\Route::currentRouteName(), $shouldCutRoutes)) {
            $content = mb_strimwidth(strip_tags($this->content), 0, 100, '...', 'utf-8');
        } else {
            $content = $this->content;
        }
        $data = [
            'id' => $this->id,
            'content' => $content,
            'images' => $this->getImagesUrl($this->images),
            'like_count' => $this->like_count,
            'reply_count' => $this->reply_count,
            'view_count' => $this->view_count,
            'floor_num' => $this->floor_num,
            'user' => new UserResource($this->whenLoaded('user')),
            'created_at' => createdAt($this->created_at),
            'target_id' => $this->target_id,
            'apply_job' => new ApplyJobResource($this->whenLoaded('applyJob')),
            'is_liked' => $this->when(isset($this->is_liked), $this->is_liked),
        ];
        return $data;
    }

    private function getImagesUrl($imageJson)
    {
        $images = [];
        foreach ((array)$imageJson as $item)
        {
            $images[] = imageUrl($item);
        }
        return $images;
    }

}

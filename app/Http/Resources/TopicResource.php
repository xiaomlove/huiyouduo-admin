<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TopicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'first_comment' => new CommentResource($this->whenLoaded('first_comment')),
            'uid'=>$this->uid,
            'is_followed'=>$this->is_followed
        ];
    }

}

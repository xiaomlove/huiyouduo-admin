<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'cover' => imageUrl($this->cover),
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'topic' => $this->whenLoaded('topic', new TopicResource($this->topic)),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isRouteDetail = \Route::currentRouteName() == 'api.store.show';
        $isRouteList = \Route::currentRouteName() == 'api.store.index';
        $coverOptions = $isRouteDetail ? '' : 'resize,h_100,w_100';
        return [
            'id' => $this->id,
            'name' => $this->name,
            'store_cat_name' => $this->store_cat_name,
            'cover' => imageUrl($this->cover, $coverOptions),
            'images' => $this->when($isRouteDetail, $this->getImagesUrl($this->images)),
            'address' => $this->address,
            'phone' => $this->phone,
            'promotion_text' => (string)$this->promotion_text,
            'recruitment_text' => mb_strimwidth((string)$this->recruitment_text, 0, 200, '...', 'utf-8'),
            'activities' => ActivityResource::collection($this->whenLoaded('activities')),
            'distance' => $this->when($this->distance, $this->formatDistance($this->distance)),
            'zhaopin_tpl_url'=>$this->zhaopin_tpl_url
        ];
    }

    private function getImagesUrl($images)
    {
        $out = [];
        if (empty($images)) {
            return $out;
        }
        foreach ($images as $value) {
            $out[] = imageUrl($value);
        }
        return $out;
    }

    private function formatDistance($distance)
    {
        if ($distance > 1000) {
            return sprintf('%.1f km', $distance / 1000);
        }
        return round($distance). ' m';
    }
}

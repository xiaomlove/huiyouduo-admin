<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LuckyDrawResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'cover' => imageUrl($this->cover),
            'consume_score' => $this->consume_score,
            'begin_time' => $this->begin_time,
            'end_time' => $this->end_time,
            'prizes' => LuckyDrawPrizeResource::collection($this->whenLoaded('prizes')),
            'win_logs' => LuckyDrawWinLogResource::collection($this->whenLoaded('winLogs')),
        ];
    }
}

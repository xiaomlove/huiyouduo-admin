<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplyJobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'intention' => $this->intention,
            'phone' => $this->phone,
            'province' => new DistrictResource($this->whenLoaded('province')),
            'city' => new DistrictResource($this->whenLoaded('city')),
            'district' => new DistrictResource($this->whenLoaded('district')),
            'created_at' => createdAt($this->created_at, 'Y-m-d'),
            'comment' => new CommentResource($this->whenLoaded('comment')),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isRouteProfile = \Route::currentRouteName() == 'api.user.profile';
        return [
            'id' => $this->id,
            'name' => $this->name,
            'nickname' => $this->nickname,
            'phone' => substr_replace($this->phone, '****', '3', '4'),
            'scores' => $this->scores,
            'avatar' => empty($this->avatar) ? asset('img/avatar.png') : imageUrl($this->avatar),
//            'has_sign_in' => $this->when($isRouteProfile, $this->hasSignInToday()),
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Topic;

class TopicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $type = $this->request->get('type');
        if ($this->is('admin*')) {
            return true;//后台啥都能发
        } elseif ($this->is('api*')) {
            $user = $this->user('api');
        } else {
            return false;
        }
        if (in_array($type, [Topic::TYPE_RECRUITMENT, Topic::TYPE_ACTIVITY]))
        {
            //招聘和活动，只有商家能发
            $adminUser = $user->adminUser;
            if ($adminUser && $adminUser->isRole(Role::SLUG_AGENTL))
            {
                return true;
            }
        }
        else
        {
            //趣事、求职，都能发
            return true;
        }
        return false;
    }

    public function validator()
    {
        $v = \Validator::make($this->all(), [
            'type' => [Rule::in(array_keys(Topic::$typeNames))],
            'content' => ['required'],
            'images' => ['array']
        ]);
        $applyJobFields = [
            'apply_job.intention', 'apply_job.phone', 'apply_job.username',
            'apply_job.province_code', 'apply_job.city_code', 'apply_job.district_code'
        ];
        $v->sometimes($applyJobFields, "required", function ($input) {
            return $input->type == Topic::TYPE_APPLY_JOB;
        });

        return $v;
    }
}

<?php

namespace App\Http\Requests;

use App\Models\UserFinanceAccount;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserFinanceAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', Rule::in(array_keys(UserFinanceAccount::$typeNames))],
            'account' => 'required',
            'bind_username' => 'required',
            'bind_phone' => 'phone',
        ];
    }
}

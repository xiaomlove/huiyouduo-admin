<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WithdrawalLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function validator()
    {
        $validator = \Validator::make($this->all(), [
            'finance_account_id' => 'required|integer',
            'money' => 'required|numeric|min:0.1',
        ]);
        $validator->after(function ($validator) {
            $user = \Auth::user();
            if (!$user->financeAccounts()->where('id', $this->get('finance_account_id'))->first()) {
                $validator->errors()->add('finance_account_id', '账户错误');
            }
            if ($user->money < floor($this->get('money') * 100)) {
                $validator->errors()->add('money', '资金不足');
            }
        });
        return $validator;
    }
}

<?php

namespace App\Http\Requests;

use App\Models\District;

trait CheckDistrict
{
    protected function checkDistrict()
    {
        $province = District::where('code', request('province_code'))->first();
        if (!$province) {
            return api('缺少 province_code');
        }
        $city = $province->cities()->where('code', request('city_code'))->first();
        if (!$city) {
            return api('缺少 city_code 或 city_code 非法');
        }
        $district = $city->districts()->where('code', request('district_code'))->first();
        if (!$district) {
            return api('缺少 district_code 或 district_code 非法');
        }
        return api(RET_OK, RET_SUCCESS_MSG, []);
    }
}
<?php

namespace App\Http\Requests;

use App\Models\SmsSendLog;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SmsVerifyCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function validator()
    {
        $validator = \Validator::make($this->request->all(), [
            'sms_verify_code' => [
                'required',
                Rule::exists((new SmsSendLog())->getTable(), 'code')->whereNull('used_at')->isSuccess(),
            ],
            'phone' => 'required|phone',
        ]);

        return $validator;
    }
}

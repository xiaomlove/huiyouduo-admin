<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\LuckyDrawResource;
use App\Models\LuckyDraw;
use App\Models\LuckyDrawWinLog;
use App\Repositories\LBSRepository;
use App\Repositories\LuckyDrawRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class LuckyDrawController extends Controller
{
    public function show($id)
    {
        $logPrefix = sprintf('%s, user: %s', __METHOD__, \Auth::id());
        $city = (new LBSRepository())->getUserCity(request());
        if ($city) {
            //取官方以及这个城市的
            \Log::info("$logPrefix, has city: {$city->name}, get official or this city's");
            $luckyDraw = LuckyDraw::with([
                'prizes' => function ($query) use ($city) {
                    return $query->where(function ($query) use ($city) {
                        return $query->where('store_id', 0)->orWherehas('store', function ($query) use ($city) {
                            return $query->where('city_code', $city->code);
                        });
                    });
                },
                'winLogs',
                'winLogs.user',
                'winLogs.luckyDrawPrize',
            ])->findOrFail($id);
        } else {
            //只取官方的
            \Log::info("$logPrefix, no city, get official");
            $luckyDraw = LuckyDraw::with([
                'prizes' => function ($query) {return $query->where('store_id', 0);},
                'winLogs',
                'winLogs.user',
                'winLogs.luckyDrawPrize',
            ])->findOrFail($id);
        }

        if ($luckyDraw->prizes->isNotEmpty()) {
            $prizeIds = $luckyDraw->prizes->pluck('id');
            $cacheResult = Cache::put($this->prizeCacheKey($id), $prizeIds->toArray(), 10);
            \Log::info("$logPrefix, prizes: " . $prizeIds->implode(',') . ", cache Result: " . var_export($cacheResult, true));
        }

        $resource = new LuckyDrawResource($luckyDraw);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);

    }

    private function prizeCacheKey($id)
    {
        return sprintf('%s:%s:%s', __METHOD__, \Auth::id(), $id);
    }

    public function action($id)
    {
        $cacheKey = $this->prizeCacheKey($id);
        $prizes = Cache::get($cacheKey);
        if (is_null($prizes)) {
            return api('已经过期，请刷新抽奖信息页面');
        }
        if (empty($prizes)) {
            return api('没有奖品，不要抽');
        }
        $logPrefix = sprintf('%s, user: %s, cache prizes: %s', __METHOD__, \Auth::id(), implode(',', $prizes));
        $result = (new LuckyDrawRepository())->award($id);
        if ($result['ret'] != 0) {
            return $result;
        }
        $winPrize = $result['data'];
        $logPrefix .= ", win prize: " . $winPrize->id;
        if (!in_array($winPrize->id, $prizes)) {
            \Log::info("$logPrefix, not valid");
            return api('谢谢参与');
        }
        $win_message = "";
        if($winPrize->is_entity == 1)
        {
            $win_message = '恭喜您中了"'.$winPrize->title.'",请到我的个人信息填写收货地址';
        }
        else
        {
            $win_message =  '恭喜您中了"'.$winPrize->title.'"';
        }
        return api(RET_OK, $win_message, $winPrize);
    }

    /**
     * @desc 代理商商家核销
     * @param Request $request
     */
    public function verifyprize(Request $request)
    {
        $code = $request->prizeid;
        $codeData = \Hashids::decode($code);
        if (empty($codeData)) return api("非法code: $code");
        $prizeid = $codeData[0];
        try
        {
            $lucky_prizea_log = LuckyDrawWinLog::find($prizeid);
            $lucky_prizea_log->is_verify_take_prize = 1;
            $lucky_prizea_log->save();
            echo '<script>alert("核销成功")</script>';
        }
        catch (\Exception $exception)
        {
            echo '<script>alert("未知错误")</script>';
        }
    }

}

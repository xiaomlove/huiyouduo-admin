<?php

namespace App\Http\Controllers\Api;

use App\Models\District;
use Hashids\Hashids;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Config;

class OtherController extends Controller
{
    public function about()
    {
        $about = Config::getAbout();

        $data = [
            'content' => (string)$about,
        ];

        return api(RET_OK, RET_SUCCESS_MSG, $data);
    }

    /**
     * @desc 扫码注册
     */
    public function promotionQrCode()
    {
        return view("home.qrcode_register");
    }
    /**
     * @desc 扫码注册成功
     */
    public function promotionQrCodeSuccess()
    {
        $qr_register_score = Config::getRegisterByQrCodeScore();
        return view("home.qrcode_register_success",compact("qr_register_score"));
    }

    public function feedbacklist()
    {
        return view("home.feedbacklist");
    }
    public function feedbackshow()
    {
        return view("home.feedbackshow");
    }

    /**
     * 获取oss上传凭证
     *
     * @see https://help.aliyun.com/document_detail/28791.html
     */
    public function ossUploadToken()
    {
        include app_path('Libraries/aliyun-php-sdk-core/Config.php');
        define("REGION_ID", \config('sts.region_id'));
        define("ENDPOINT", \config('sts.endpoint'));
        // 只允许子用户使用角色
        \DefaultProfile::addEndpoint(REGION_ID, REGION_ID, "Sts", ENDPOINT);
        $iClientProfile = \DefaultProfile::getProfile(REGION_ID, \config('sts.access_key_id'), \config('sts.access_key_secret'));
        $client = new \DefaultAcsClient($iClientProfile);
        // 角色资源描述符，在RAM的控制台的资源详情页上可以获取
        $roleArn = \config('sts.role_arn');
        // 在扮演角色(AssumeRole)时，可以附加一个授权策略，进一步限制角色的权限；
        // 详情请参考《RAM使用指南》
        // 此授权策略表示读取所有OSS的只读权限
        $policy = <<<POLICY
{
    "Statement": [
        {
            "Action": "oss:*",
            "Effect": "Allow",
            "Resource": "*"
        }
    ],
    "Version": "1"
}
POLICY;
        $request = new \Sts\Request\V20150401\AssumeRoleRequest();
        // RoleSessionName即临时身份的会话名称，用于区分不同的临时身份
        // 您可以使用您的客户的ID作为会话名称
        $request->setRoleSessionName("hui_you_duo");
        $request->setRoleArn($roleArn);
        $request->setPolicy($policy);
        $request->setDurationSeconds(3600);
        $response = $client->getAcsResponse($request);
        $out = (array)$response->Credentials;
        $out['region'] = strstr(\config('filesystems.disks.oss.endpoint'), '.', true);
        $out['bucket'] = \config('filesystems.disks.oss.bucket');
        return api(RET_OK, RET_SUCCESS_MSG, $out);
    }

    public function districtJson()
    {
        $districts = District::all()->toArray();
        $results = $this->buildNested($districts);
        return api(RET_OK, RET_SUCCESS_MSG, $results);
    }

    private function buildNested($allNodes, $parentId = 0)
    {
        $out = [];
        foreach ($allNodes as $node) {
            if ($node['parent_id'] == $parentId) {
                $item = [
                    'name' => $node['name'],
                    'code' => $node['code'],
                ];
                $sub = $this->buildNested($allNodes, $node['id']);
                if (!empty($sub)) {
                    $item['sub'] = $sub;
                }
                $out[] = $item;
            }
        }
        return $out;
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Libraries\Taobao;
use App\Models\CollectAndViewhistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CollectAndViewhistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pagesize = (int)$request->pagesize ? (int)$request->pagesize : 20;
        $type = (int)$request->type;
        $uid = \Auth::user()->id;
        if(empty($type)) return api("请求错误");
        $list = CollectAndViewhistory::where(['type'=>$type,'uid'=>$uid])
            ->orderBy('updated_at','desc')
            ->paginate($pagesize)
            ->toArray();
        if(!empty($list))
        {
            $list = $list['data'];
            foreach ($list as $k=>$v)
            {
                $item_data = Taobao::superSearch($v['item_id']);
                if(!empty($item_data))
                {
                    $list[$k]['items']['pict_url'] = $item_data['pict_url'];
                    $list[$k]['items']['user_type'] = $item_data['user_type'];
                    $list[$k]['items']['title'] = $item_data['title'];
                    $list[$k]['items']['volume'] = $item_data['volume'];
                    $list[$k]['items']['zk_final_price'] = $item_data['zk_final_price'];
                    if(!isset($item_data['coupon_info']))
                        $coupon_amount = 0;
                    else
                        $coupon_amount = Taobao::getCouponPrice($item_data['coupon_info']);
                    $list[$k]['items']['coupon_name'] = $coupon_amount."元劵";
                    $list[$k]['items']['coupon_after_price'] = max(0,sprintf('%.2f',$item_data['zk_final_price'] - $coupon_amount));
                    $list[$k]['items']['forecast_commission'] = Taobao::forecastCommission($item_data['zk_final_price'],$item_data['commission_rate'] / 10000);
                }
            }
        }

        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$list]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = (int)$request->type;
        $uid = \Auth::user()->id;
        $item_id = trim($request->item_id);
        if(empty($type) || empty($item_id)) return api("请求错误");
        $model = CollectAndViewhistory::where([
            'uid'=>$uid,'item_id'=>$item_id,'type'=>$type
        ])->first();

        if(!$model)
            CollectAndViewhistory::create([
                'uid'=>$uid,'item_id'=>$item_id,'type'=>$type
            ]);
        else
        {
            //有数据 如果是收藏的 可以取消收藏 即删除这条记录
            if($type == CollectAndViewhistory::COLLECT_LOG_TYPE) $model->delete();
            elseif ($type == CollectAndViewhistory::VIEW_HISTORY_LOG_TYPE)
            {
                //如果是浏览记录可以更新接口
                $model->updated_at = date("Y-m-d H:i:s");
                $model->save();
            }
        }
        $new_model_exists = CollectAndViewhistory::where([
            'uid'=>$uid,'item_id'=>$item_id,'type'=>$type
        ])->exists();
        //浏览的同时 判断当前是否有收藏过
        $has_liked_item = CollectAndViewhistory::where([
            'uid'=>$uid,'item_id'=>$item_id,'type'=>1
        ])->exists();
        return api(RET_OK,RET_SUCCESS_MSG,['is_exists'=>$new_model_exists,'load_page_has_liked'=>$has_liked_item]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

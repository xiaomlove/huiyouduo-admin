<?php

namespace App\Http\Controllers\Api;

use App\Models\Admin;
use App\Models\Agent;
use App\Models\CouponCard;
use App\Models\Score;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CouponCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uid = \Auth::user()->id;
        $list = Score::where(['uid'=>$uid,'type'=>Score::TYPE_COUPON_CARD_TOP_UP])->orderBy('created_at','desc')->get();
        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $coupon_card_pub_key = trim($request->coupon_card_pub_key);
        $coupon_card_pri_key = trim($request->coupon_card_pri_key);
        if(empty($coupon_card_pub_key) || empty($coupon_card_pri_key)) return api("账号和密码不能为空");
        //密码不区分大小写
        $data = CouponCard::where(['coupon_card_pub_key'=>$coupon_card_pub_key,'coupon_card_pri_key'=>strtolower($coupon_card_pri_key)])->first();
        if(!$data) return api("请输入正确的账号和密码");
        if($data->is_used == 1) return api('无效卡号');
        DB::beginTransaction();
        try
        {
            $uid = \Auth::user()->id;
            //兑换充值卡积分
            Score::assignForCouponCardTopUp($uid,$data->points_quota,'充值卡激活',$data->toArray());
            //更新优惠卡使用状态
            $data->is_used = 1;
            $data->bind_uid = $uid;
            $data->used_at = date('Y-m-d H:i:s');
            $data->save();
            //用户 -- 代理商用户 绑定上下级关系 如果我的上级代理商没有在后台绑定前端用户 那么我属于平台的
            $this->bindUserAgent($uid,$data->first_class_agent_id,$data->second_class_agent_id);
            DB::commit();
            return api(RET_OK,RET_SUCCESS_MSG,[]);

        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return api('兑换失败');
        }
    }

    /**
     *
     * @param $uid
     * @param $first_agent_id
     * @param $second_agent_id
     */
    protected function bindUserAgent($uid = 0,$first_agent_id = 0,$second_agent_id = 0)
    {
        //判断当前uid是否绑定过代理商 绑定过 不再重新绑定
        if(!$uid || !$first_agent_id || !$second_agent_id) return false;
        $has_bind_parent = User::where('parent_id','>',0)->where('id',$uid)->first();
        if($has_bind_parent) return false;//绑定过父级代理商
        //绑定parent
        $first_agent_front_uid = $this->getFrontUidByAgentId($first_agent_id);
        $second_agent_front_uid = $this->getFrontUidByAgentId($second_agent_id);
        //平台-一级-二级-用户  或者 平台-一级-用户
        if(!$first_agent_front_uid) return false;
        if(empty($second_agent_front_uid))
        {
            //平台-一级-用户
            User::where('id',$uid)->update([
                'parent_id'=>$first_agent_front_uid,
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
        }
        else
        {
            //平台-一级-二级-用户
            User::where('id',$uid)->update([
                'parent_id'=>$second_agent_front_uid,
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
            User::where('id',$second_agent_front_uid)->update([
                'parent_id'=>$first_agent_front_uid,
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
        }
        return true;
    }

    /**
     * 根据代理商id获取代理商的前端uid
     * @param int $agent_id
     */
    protected function getFrontUidByAgentId($agent_id = 0)
    {
        try
        {
            $agent = Agent::findOrFail($agent_id);
            $agent_admin_front = Admin::findOrFail($agent->admin_uid);
            $agent_admin_front_uid = $agent_admin_front->front_uid;
            return $agent_admin_front_uid;
        }
        catch (\Exception $exception)
        {
            return 0;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

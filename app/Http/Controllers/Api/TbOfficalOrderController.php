<?php

namespace App\Http\Controllers\Api;

use App\Libraries\Taobao;
use App\Models\Config;
use App\Models\TaobaoPids;
use App\Models\TbkOficalOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TbOfficalOrderController extends Controller
{
    /**is_settled
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_size = (int)$request->page_size ? (int)$request->page_size : 20;
        $user = \Auth::user();
        $tb_uid = $user->tb_uid;
        $user_order_list = [];
        $order_state = (int)$request->order_state;//0:全部;1:已经付款;2:已结算
        if(!empty($order_state))
        {
            if(!in_array($order_state,[1,2]))
                return api("参数错误");
        }

        if(!empty($tb_uid))
        {
            //这个是怎么筛选订单的
            $user_order_list = TbkOficalOrder::
            when($order_state == 1, function ($query) use ($order_state) {
                $query->whereIn('tk_status', [12,3,14]);
            })->when($order_state == 2, function ($query) use ($order_state) {
                $query->where('is_settled', 1);
            })->select(
                "alipay_total_price",
                'commission',
                'create_time',
                'item_title',
                'num_iid',
                'order_type',
                'trade_parent_id',
                'tk_status',
                'is_settled'
            )->orderBy('create_time','desc')->get()->toArray();
            if(!empty($user_order_list))
            {
                foreach ($user_order_list as $k=>$v)
                {
                    if($tb_uid != handle_tbk_trade_parent_id($v['trade_parent_id']))
                    {
                        unset($user_order_list[$k]);
                    }
                    $cur_order_goods_data = Taobao::superSearch($v['num_iid']);
                    $user_order_list[$k]['pict_url'] = $cur_order_goods_data['pict_url'];
                    $tk_status = "";
                    switch ($v['tk_status'])
                    {
                        case 3:
                            $tk_status = "订单结算";
                            break;
                        case 12:
                            $tk_status = "订单付款";
                            break;
                        case 13:
                            $tk_status = "订单失效";
                            break;
                        case 14:
                            $tk_status = "订单成功";
                            break;
                    }
                    $user_order_list[$k]['tk_status'] = $tk_status;
                    $user_order_list[$k]['forecast_commission'] =  sprintf("%.2f",$v['commission'] * 0.89 * (Config::getUserCommissionPercent()/100));
                    unset($user_order_list[$k]['commission']);
                }
            }
        }
        else
        {
            //这种是绑定pid的用户
            $tb_pid_info = TaobaoPids::where("uid",$user->id)->first();
            if($tb_pid_info)
            {
                $adzone_id = $tb_pid_info->adzone_id;
                $site_id = $tb_pid_info->site_id;
                $user_order_list = TbkOficalOrder::where([
                    "adzone_id"=>$adzone_id,
                    "site_id"=>$site_id,
                ])->when($order_state == 1, function ($query) use ($order_state) {
                    $query->whereIn('tk_status', [12,3,14]);
                })->when($order_state == 2, function ($query) use ($order_state) {
                    $query->where('is_settled', 1);
                })->select(
                    "alipay_total_price",
                    'commission',
                    'create_time',
                    'item_title',
                    'num_iid',
                    'order_type',
                    'trade_parent_id',
                    'tk_status',
                    'is_settled'
                )->orderBy('create_time','desc')->paginate($page_size)->toArray();
                if(!empty($user_order_list))
                {
                    $user_order_list = $user_order_list['data'];
                    foreach ($user_order_list as $k=>$v)
                    {
                        $cur_order_goods_data = Taobao::superSearch($v['num_iid']);
                        if(empty($cur_order_goods_data)) continue;
                        $user_order_list[$k]['pict_url'] = $cur_order_goods_data['pict_url'];
                        $tk_status = "";
                        switch ($v['tk_status'])
                        {
                            case 3:
                                $tk_status = "订单结算";
                                break;
                            case 12:
                                $tk_status = "订单付款";
                                break;
                            case 13:
                                $tk_status = "订单失效";
                                break;
                            case 14:
                                $tk_status = "订单成功";
                                break;
                        }
                        $user_order_list[$k]['tk_status'] = $tk_status;
                        $user_order_list[$k]['forecast_commission'] = sprintf("%.2f",$v['commission'] * 0.89 * (Config::getUserCommissionPercent()/100));//
                        unset($user_order_list[$k]['commission']);
                    }
                }
            }
        }
        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$user_order_list]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

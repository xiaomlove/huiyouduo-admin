<?php

namespace App\Http\Controllers\Api;

use App\Models\PushMessageLog;
use App\Models\PushMessageState;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserPushMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //获取用户的省份
        $user_province = \Auth::user()->province;
        $push_message_log_count = PushMessageLog::where(function($query) use ($user_province) {
            $query->whereRaw('FIND_IN_SET(?,push_province_name)', [$user_province]);
            $query->orWhere('push_province_name',"");
        })->count();

        $has_read_count = PushMessageState::where([
            'uid'=>\Auth::user()->id,
            "is_read"=>1
        ])->count();

        $un_read_count = max(0,$push_message_log_count - $has_read_count);

        return api(RET_OK,RET_SUCCESS_MSG,['count'=>$un_read_count]);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ActivityResource;
use App\Models\Agent;
use App\Models\AgentStore;
use App\Models\StoreActivity;
use App\Repositories\LBSRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\District;
use App\Http\Resources\StoreResource;
use App\Http\Resources\DistrictResource;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $log = sprintf("%s, user: %s, request: %s", __METHOD__, \Auth::id(), json_encode($request->all()));
        $city = null;
        $cate_index = (int)$request->cate_index;
        $kw = $request->get('kw');
        if ($request->longitude && $request->latitude) {
            $log .= "has longitude and latitude";
            //传递了经纬度，取这个点的周边
            $stores = $this->listStoresFromRedis($request->longitude, $request->latitude,$cate_index,$kw);
            $locationInfo = (new LBSRepository())->getLocationInfoByGeo($request->latitude, $request->longitude);
            if (!empty($locationInfo['result']['addressComponent']['adcode'])) {
                $city = District::getCityByCode($locationInfo['result']['addressComponent']['adcode']);
                $log .= ", can get adcode, get city: " . $city->name;
            }

        } else {
            //取特定城市的
            $code = $request->get('code', config('lbs.default_city_code'));
            $city = District::getCityByCode($code);
            $log .= ", get specific city: $code, city name: " . $city->name;
            $stores = $city->stores()->when($cate_index,function ($query) use ($cate_index){
                return $query->where('cate',$cate_index);
            })
                ->when($kw, function ($query) use ($kw) {
                return $query->where('name', 'like', "%{$kw}%");
            })->orderBy('updated_at', 'desc')->paginate();
        }
        $resource = StoreResource::collection($stores);
        $cur_select_store_cate_name = AgentStore::$typeTexts[$cate_index] ?? "全部";
        $resource->additional([
            'city' => new DistrictResource($city),
            'header_cate_list'=>AgentStore::$headCateList,
            'cur_select_store_cate_name'=>$cur_select_store_cate_name
        ]);
        \Log::info($log);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    private function listStoresFromRedis($longitude, $latitude,$cate_index,$kw)
    {
        $storesFromRedis = LBSRepository::listStoresAround($longitude, $latitude, 100000);
        $distanceMap = [];
        foreach ($storesFromRedis as $value) {
            $distanceMap[$value[0]] = $value[1];
        }
        $stores = AgentStore::when($cate_index,function ($query) use ($cate_index){
            return $query->where('cate',$cate_index);
        })->when($kw,function ($query) use ($kw){
            return $query->where('name', 'like', "%{$kw}%");
        })->whereIn('id', array_keys($distanceMap))->get()->each(function (&$item) use ($distanceMap) {
            $item->distance = $distanceMap[$item->id];
        });
        return $stores;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = AgentStore::with([
            'activities' => function ($query) {return $query->orderBy('id', 'desc')->take(1);}
        ])->findOrFail($id);
        $resource = new StoreResource($result);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function recruitment($id)
    {
        $result = AgentStore::findOrFail($id);
        $zhaopin_tpl_type = 0;
        if($result['zhaopin_tpl_url'] == AgentStore::TYPE_ZHAOPIN_TPL_STYLE1_URL)
            $zhaopin_tpl_type = 1;
        elseif ($result['zhaopin_tpl_url'] == AgentStore::TYPE_ZHAOPIN_TPL_STYLE2_URL)
            $zhaopin_tpl_type = 2;
        return api(RET_OK, RET_SUCCESS_MSG, [
            'recruitment_text' => $result['recruitment_text'],
            'zhaopin_tpl_url' => $result['zhaopin_tpl_url'],
            'zhaopin_tpl_type'=>$zhaopin_tpl_type
        ]);
    }

    public function activity($id)
    {
        $result = StoreActivity::with(['topic', 'topic.first_comment'])->findOrFail($id);

        $resource = new ActivityResource($result);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);

    }

}

<?php

namespace App\Http\Controllers\Api;

use App\Events\UserLogin;
use App\Http\Requests\SmsVerifyCodeRequest;
use App\Http\Resources\TopicResource;
use App\Models\Admin;
use App\Models\Agent;
use App\Models\PushMessageState;
use App\Models\Score;
use App\Models\Topic;
use App\Models\UserInviteLog;
use App\Models\UserThirdPartyBind;
use App\Repositories\AuthenticateRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Overtrue\EasySms\EasySms;
use Overtrue\EasySms\Exceptions\NoGatewayAvailableException;
use App\Models\SmsSendLog;
use GuzzleHttp\Client;
use Laravel\Passport\Client as PassportClient;

class AuthenticateController extends Controller
{
    /**
     * 微信登录
     * @param Request $request
     * @return array
     */
    public function loginByWeixin(Request $request)
    {
        $code = $request->code;
        $phone = $request->phone;
        $smsVerifyCode = $request->sms_verify_code;
        $name = $request->name;
        if (!$code)
        {
            return api("缺少code");
        }
        $miniApp = \EasyWeChat::miniProgram();
        $session = $miniApp->auth->session($code);
        if (!$session->openid)
        {
            return api(json_encode($session));
        }
        $user = User::where("openid", $session->openid)->first();
        if (!$user)
        {
            if (!$phone || !$smsVerifyCode || !$name)
            {
                return api(RET_BIND_PHONE, "新用户，请携带手机号、短信验证码和真实姓名一起登录", $request->all());
            }
            $smsLog = SmsSendLog::where("code", $smsVerifyCode)->whereNull('used_at')->isSuccess()->first();
            if (empty($smsLog))
            {
                return api("验证码 $code 无效");
            }
            $now = Carbon::now()->toDateTimeString();
            if ($smsLog->expires_at < $now)
            {
                return api("验证码 $code 已经过期");
            }
            if ($smsLog['phone'] != $phone)
            {
                return api("手机号 $phone 与验证码 $code 不匹配");
            }
            $smsLog->update(['used_at' => $now]);

            //通过手机查找已存在用户
            $user = User::where("phone", $phone)->first();
            if ($user)
            {
                $user->update([
                    'name' => $name,
                    'openid' => $session->openid,
                    'session_key' => $session->session_key,
                ]);
            }
            else
            {
                $user = User::create([
                    'phone' => $phone,
                    'name' => $name,
                    'openid' => $session->openid,
                    'session_key' => $session->session_key,
                ]);
            }

            if ($request->_u && $request->_c)
            {
                $this->insertUserInviteLog($request->_u, $user->id, $request->_c);
            }
        }
        else
        {
            $update = ['session_key' => $session->session_key];
            if (!$user['phone'] && $phone)
            {
                $update['phone'] = $phone;
            }
            if (!$user['name'] && $name)
            {
                $update['name'] = $name;
            }

            $user->update($update);
        }
        //删除旧token
        $oneMonthAgo = Carbon::now()->addMonths(-1)->toDateTimeString();
        $user->tokens()->where("created_at", "<", $oneMonthAgo)->delete();//删除一个月前的token

        $tokenResult = $user->createToken("微信登录");

        $log = sprintf(
            "%s, params: %s, user: %s, access_token: %s",
            __METHOD__, json_encode($request->all(), 336), $user->id, $tokenResult->accessToken
        );
        \Log::info($log);

        $user->auth = [
            'access_token' => $tokenResult->accessToken,
            'expires_at' => (string)$tokenResult->token->expires_at,
        ];

        return api(0, "OK", $user);
    }

    /**
     * 登录时添加邀请记录，绑定关系用于计算佣金
     *
     * @param $uidSendInvite 发出邀请者
     * @param $invitee 被邀请者
     * @param $channel 渠道
     */
    private function insertUserInviteLog($uidSendInvite, $invitee, $channel)
    {
        $log = sprintf("%s, channel: %s, invitee: %s, uidSendInvite: %s", __METHOD__, $channel, $invitee, $uidSendInvite);
        $userSendInvite = User::find($uidSendInvite);
        if (empty($userSendInvite))
        {
            $log .= ", uidSendInvite not exists";
            \Log::info($log);
            return;
        }

        $userSendInvite->inviteLogs()->create([
            'invitee' => $invitee,
            'channel' => $channel,
        ]);
        \Log::info($log);
    }

    /**通过微信授权更新用户信息
     *
     * @param Request $request
     * @return array
     */
    public function updateByWeixin(Request $request)
    {
        $iv = $request->iv;
        $encryptedData = $request->encryptedData;
        if (empty($iv) || empty($encryptedData))
        {
            return api("require iv + encryptedData");
        }
        $user = \Auth::user();
        $miniApp = \EasyWeChat::miniProgram();
        $decryptedData = $miniApp->encryptor->decryptData($user->session_key, $iv, $encryptedData);
        $update = [
            'nickname' => $decryptedData['nickName'],
            'gender' => $decryptedData['gender'],
            'city_code' => $decryptedData['city_code'],
            'province_code' => $decryptedData['province_code'],
            'country_code' => $decryptedData['country_code'],
            'avatar' => $decryptedData['avatarUrl'],
            'unionid' => $decryptedData['unionid'] ?? "",
        ];
        $user->update($update);
        return api(0, "OK", $user);
    }

    /**
     * 微信开放平台登录
     *
     * @param Request $request
     * @return array
     * @todo 去掉不用，用新版
     */
    public function loginByWeixinOpen(Request $request)
    {
        $info = $request->info;
        $client_id = $request->clientid;
        if (empty($info) || empty($info['openid'])) {
            return api('缺少 info 或者 info.openid');
        }
        $openid = $info['openid'];
        $thirdPartyInfo = UserThirdPartyBind::platformWeixinOpen()->where('openid', $openid)->first();
        if ($thirdPartyInfo) {
            //用户经存在
            $user = $thirdPartyInfo->user()->firstOrFail();
        } else {
            //创建用户，并绑定
            $user = User::create([
                'nickname' => $info['nickname'] ?? '微信用户_' . $info['openid'],
                'name' => $info['nickname'] ?? '微信用户_' . $info['openid'],
                'avatar' => $info['headimgurl'] ?? '',
                'gender' => $info['sex'] ?? 0,
                'register_source' => User::REGISTER_SOURCE_WEIXIN_OPEN,
                'clientid'=>$client_id
            ]);
            $thirdPartyInfo = $user->thirdPartyBinds()->create([
                'platform' => UserThirdPartyBind::PLATFORM_WEIXIN_OPEN,
                'openid' => $openid,
                'unionid' => $info['unionid'] ?? '',
                'nickname' => $info['nickname'] ?? '',
                'info' => $info,
            ]);
        }

        return $this->doLogin($user, __METHOD__);
    }

    /**
     * 新版微信登录
     *
     * @param Request $request
     * @return array
     */
    public function loginByWeixinOpen2(Request $request)
    {
        $info = $request->info;
        $client_id = $request->clientid;
        $platform = UserThirdPartyBind::PLATFORM_WEIXIN_OPEN;
        if (empty($info) || empty($info['openid'])) {
            return api('缺少 info 或者 info.openid');
        }
        $openid = $info['openid'];
        $thirdPartyInfo = UserThirdPartyBind::PlatformWeixinOpen()->where('openid', $openid)->first();
        if ($thirdPartyInfo && $thirdPartyInfo->user) {
            return $this->doLogin($thirdPartyInfo->user, __METHOD__);
        }

        if (!$thirdPartyInfo) {
            $info['client_id'] = $client_id;
            $thirdPartyInfo = UserThirdPartyBind::createByWeixinOpen($info);

        }

        return api(RET_BIND_THIRD_PARTY, '跳转绑定第三方账号页面', [
            'platform' => $platform,
            'openid' => $openid,
        ]);

    }

    public function loginByQQ(Request $request)
    {
        $info = $request->info;
        $client_id = $request->clientid;
        $platform = UserThirdPartyBind::PLATFORM_QQ;
        if (empty($info) || empty($info['openid'])) {
            return api('缺少 info 或者 info.openid');
        }
        $openid = $info['openid'];
        $thirdPartyInfo = UserThirdPartyBind::platformQQ()->where('openid', $openid)->first();
        if ($thirdPartyInfo && $thirdPartyInfo->user) {
            return $this->doLogin($thirdPartyInfo->user, __METHOD__);
        }

        if (!$thirdPartyInfo) {
            $info['client_id'] = $client_id;
            $thirdPartyInfo = UserThirdPartyBind::createByQQ($info);

        }

        return api(RET_BIND_THIRD_PARTY, '跳转绑定第三方账号页面', [
            'platform' => $platform,
            'openid' => $openid,
        ]);


    }


    /**
     * 绑定QQ
     *
     * @param SmsVerifyCodeRequest $request
     * @return array
     */
    public function bindQQ(Request $request)
    {
        $info = $request->info;
        if (empty($info) || empty($info['openid'])) {
            return api('缺少 info 或者 info.openid');
        }
        $user = \Auth::user();
        $info['uid'] = $user->id;
        $thirdPartyInfo = UserThirdPartyBind::createByQQ($info);

        return api(RET_OK, RET_SUCCESS_MSG, $thirdPartyInfo);
    }

    /**
     * 绑定微信开放平台
     *
     * @param Request $request
     * @return array
     */
    public function bindWeixinOpen(Request $request)
    {
        $info = $request->info;
        if (empty($info) || empty($info['openid'])) {
            return api('缺少 info 或者 info.openid');
        }
        $user = \Auth::user();
        $info['uid'] = $user->id;
        $thirdPartyInfo = UserThirdPartyBind::createByWeixinOpen($info);

        return api(RET_OK, RET_SUCCESS_MSG, $thirdPartyInfo);
    }

    /**
     * 获取手机验证码
     *
     * @param Request $request
     * @return array
     */
    public function smsVerifyCode(Request $request)
    {
        $phone = $request->phone;
        if (empty($phone) || !preg_match('/1[\d]{10}/', $phone))
        {
            return api("手机号" . (string)$phone . "不正确");
        }
        $user = \Auth::user();
        $config = config('sms');
        $easySms = new EasySms($config);
        $code = random_int(100000, 999999);
        $logData = [
            'uid' => $user ? $user->id : 0,
            'phone' => $phone,
            'code' => $code,
            'expires_at' => Carbon::now()->addSeconds($config['lifetime'])->toDateTimeString(),
        ];
        $action = $request->get('action', 'login');
        if (empty($config["template_id_{$action}"])) {
            \Log::warning(sprintf("%s, invalid action: %s", __METHOD__, $action));
            $action = 'login';
        }

        try
        {
            $result = $easySms->send($phone, [
                'content'  => '您的验证码为: ' . $code,
                'template' => $config["template_id_{$action}"],
                'data' => [
                    'code' => $code
                ],
            ]);
            $logData['send_result'] = SmsSendLog::SEND_RESULT_SUCCESS;
            $logData['send_result_info'] = json_encode($result, 336);
            SmsSendLog::create($logData);
            return api(0, "OK", $request->all());
        }
        catch (NoGatewayAvailableException $e)
        {
            $logData['send_result'] = SmsSendLog::SEND_RESULT_FAIL;
            $logData['send_result_info'] = json_encode($e->getResults(), 336);
            SmsSendLog::create($logData);
            return api("获取验证码失败，请稍候重试");
        }

    }

    /**
     * 绑定手机
     *
     * @param Request $request
     * @return array
     */
    public function bindPhone(Request $request)
    {
        $user = \Auth::user();
        $code = $request->code;
        $phone = $request->phone;
        $name = $request->name;

        $smsLog = SmsSendLog::where("code", $code)->where("uid", $user->id)
            ->whereNull('used_at')->isSuccess()->first();
        if (empty($smsLog))
        {
            return api("验证码 $code 无效");
        }
        $now = Carbon::now()->toDateTimeString();
        if ($smsLog->expires_at < $now)
        {
            return api("验证码 $code 已经过期");
        }
        if ($smsLog['phone'] != $phone)
        {
            return api("手机号 $phone 与验证码 $code 不匹配");
        }

        $update = ['phone' => $phone];
        if ($name)
        {
            $update['name'] = $name;
        }

        $user->update($update);

        $smsLog->update(['used_at' => $now]);

        return api(0, "OK", $update);
    }

    /**
     * 修改手机
     *
     * @param Request $request
     * @return array
     */
    public function changePhone(Request $request)
    {
        $user = \Auth::user();
        $code = $request->sms_verify_code;
        $phone = $request->phone;
        $checkCodeRes = (new AuthenticateRepository())->checkSmsVerifyCode($code, $phone);
        if ($checkCodeRes['ret'] != 0) {
            return $checkCodeRes;
        }
        $existUsers = User::where('phone', $phone)->get();
        if ($existUsers->isNotEmpty()) {
            $existUid = $existUsers->pluck('id');
            \Log::info(sprintf("%s, phone: %s bind to user: %s", __METHOD__, $phone, $existUid->implode(',')));
            User::whereIn('id', $existUid->all())->update(['phone' => '']);
        }

        $update = ['phone' => $phone];
        $user->update($update);

        $checkCodeRes['data']->update(['used_at' => now()]);

        return api(0, "OK", $update);
    }

    /**
     * 账号密码登录，获取token
     *
     * @param UserRequest $request
     * @return array
     */
    public function login(Request $request)
    {
        $data = [
            'grant_type' => 'password',
            'client_id' => $request->get('client_id', config('oauth.client_id')),
            'client_secret' => $request->get('client_secret', config('oauth.client_secret')),
            'username' => $request->username,
            'password' => $request->password,
            'scope' => '',
        ];
        $url = url('/') . "/oauth/token";
        return $this->requestOAuthServer("post", $url, ['form_params' => $data]);
    }

    /**
     * 手机登录
     *
     * @param Request $request
     * @return array
     */
    public function loginByPhone(Request $request)
    {
        $phone = $request->phone;
        $smsVerifyCode = $request->sms_verify_code;
        $client_id = $request->clientid;
        $platform = $request->platform;
        $openid = $request->openid;
        $log = sprintf('%s, params: %s', __METHOD__, json_encode($request->all(), 336));
        if (!$phone || !$smsVerifyCode)
        {
            return api("缺少 phone or sms_verify_code");
        }
        $smsLog = SmsSendLog::where("code", $smsVerifyCode)->whereNull('used_at')->isSuccess()->first();
        if (empty($smsLog))
        {
            return api("验证码 $smsVerifyCode 无效");
        }
        $now = Carbon::now()->toDateTimeString();
        if ($smsLog->expires_at < $now)
        {
            return api("验证码 $smsVerifyCode 已经过期");
        }
        if ($smsLog['phone'] != $phone)
        {
            return api("手机号 $phone 与验证码 $smsVerifyCode 不匹配");
        }
        //第三记账号信息
        $thirdPartyInfo = null;
        if ($openid && $platform) {
            $thirdPartyInfo = UserThirdPartyBind::where('platform', $platform)->where('openid', $openid)->firstOrFail();
        }

        $user = User::where("phone", $phone)->first();
        if (!$user)
        {
            $log .= ", user is new !!!";
            $user = User::create([
                'phone' => $phone,
                'clientid'=>$client_id,
                'nickname' => $thirdPartyInfo ? $thirdPartyInfo->nickname : "手机用户_$phone",
                'register_source' => $thirdPartyInfo ? $thirdPartyInfo->platform : User::REGISTER_SOURCE_PHONE,
            ]);

        }
        $smsLog->update(['used_at' => $now]);

        if ($thirdPartyInfo) {
            $thirdPartyInfo->update(['uid' => $user->id]);
        }
        \Log::info($log);
        return $this->doLogin($user, __METHOD__);
    }

    private function doLogin(User $user, $caller = __METHOD__)
    {
        $tokenResult = $user->createToken($caller);

        //触发登录事件，方便后续更多操作，添加更多事件监听者即可
        $user->login_ip = request()->ip();
        event(new UserLogin($user));

        $log = sprintf(
            "%s, params: %s, user: %s, access_token: %s",
            $caller, json_encode(request()->all(), 336), $user->id, $tokenResult->accessToken
        );
        \Log::info($log);

        //不返回过多敏感信息
        return api(RET_OK, RET_SUCCESS_MSG, [
            'id' => $user->id,
            'name' => $user->name,
            'nickname' => $user->nickname,
            'auth' => [
                'access_token' => $tokenResult->accessToken,
                'expires_at' => (string)$tokenResult->token->expires_at,
            ],
        ]);
    }

    private function checkSmsVerifyCode($smsVerifyCode, $phone)
    {
        $smsLog = SmsSendLog::where("code", $smsVerifyCode)->whereNull('used_at')->isSuccess()->first();
        if (empty($smsLog))
        {
            return api("验证码 $smsVerifyCode 无效");
        }
        $now = Carbon::now()->toDateTimeString();
        if ($smsLog->expires_at < $now)
        {
            return api("验证码 $smsVerifyCode 已经过期");
        }
        if ($smsLog['phone'] != $phone)
        {
            return api("手机号 $phone 与验证码 $smsVerifyCode 不匹配");
        }

        return api(RET_OK, RET_SUCCESS_MSG, $smsLog);
    }

    /**
     * 刷新token
     *
     * @param Request $request
     * @return array
     */
    public function refreshToken(Request $request)
    {
        $data = [
            'grant_type' => 'refresh_token',
            'client_id' => $request->get('client_id', config('oauth.client_id')),
            'client_secret' => $request->get('client_secret', config('oauth.client_secret')),
            'refresh_token' => $request->refresh_token,
            'scope' => '',
        ];
        $url = url('/') . "/oauth/token";
        //结果码-1001，刷新token失败，这里前端需要进行登录操作了。
        return $this->requestOAuthServer("post", $url, ['form_params' => $data], -1001);
    }

    /**
     * 退出，删除access_token
     *
     * @param Request $request
     * @return array
     */
    public function logout(Request $request)
    {
//        dd(\Auth::user());
        $result = \Auth::user()->tokens()->delete();
        if ($result)
        {
            return api(0, "OK", []);
        }
        else
        {
            return api("删除token失败");
        }
    }

    public function register(UserRequest $request)
    {
        $result = (new UserRepository())->create($request);
        if ($result['ret'] !== 0)
        {
            return $result;
        }
        //注册成功后直接登录
        $request->query->set('username', $request->email);
        return $this->login($request);
    }

    private function requestOAuthServer($method, $url, array $options = [], $failureCode = -1)
    {
        try
        {
            $client = new Client();
            $response = call_user_func_array([$client, $method], [$url, $options]);
            $result = json_decode((string)$response->getBody(), true);
            return api(0, "OK", $result);
        }
        catch (\Exception $e)
        {
            $result = json_decode((string)$e->getResponse()->getBody(), true);
            return api($failureCode, get_class($e) . ': ' . $result['message'], request()->all());
        }
    }

    /**
     * 根据代理商id获取代理商的前端uid
     * @param int $agent_id
     */
    protected function getFrontUidByAgentId($agent_id = 0)
    {
        try
        {
            $agent = Agent::findOrFail($agent_id);
            $agent_admin_front = Admin::findOrFail($agent->admin_uid);
            $agent_admin_front_uid = $agent_admin_front->front_uid;
            return $agent_admin_front_uid;
        }
        catch (\Exception $exception)
        {
            return 0;
        }

    }

    /**
     * 通过二维码注册
     *
     * @return array
     */
    public function registerByQrCode()
    {
        $code = \request('code');
        $phone = \request('phone');
        $smsVerifyCode = \request('sms_verify_code');
        if (empty($code)) {
            return api('缺少 code');
        }
        if (empty($phone)) {
            return api('缺少 phone');
        }
//        if (empty($smsVerifyCode)) {
//            return api('缺少 sms_verify_code');
//        }
        $codeData = \Hashids::decode($code);
        if (empty($codeData)) {
            return api("非法code: $code");
        }
//        $checkSmsVerifyCode = $this->checkSmsVerifyCode($smsVerifyCode, $phone);
//        if ($checkSmsVerifyCode['ret'] != 0) {
//            return $checkSmsVerifyCode;
//        }
//        $smsLog = $checkSmsVerifyCode['data'];
        $user = User::where('phone', $phone)->first();
        if (!empty($user)) {
            return api("手机 $phone 已经注册");
        }

        $agentId = $codeData[0];
        $agent = Agent::with(['admin', 'admin.frontUser'])->find($agentId);
        if (!$agent || !$agent->admin || !$agent->admin->frontUser) {
            \Log::notice(sprintf(
                '%s, params: %s, agent: %s => %s => %s not exists or has no admin or has no frontUser',
                __METHOD__, json_encode(\request()->all()), $code, $agentId, json_encode($agent->toArray(), 336)
            ));
            return api("发生错误，请联系相关人员");
        }
        $result = \DB::transaction(function () use ($phone, $agent) {
            //创建用户，绑定上级
            $user = User::create([
                'parent_id' => $agent->admin->front_uid,
                'phone' => $phone,
                'nickname' => "手机用户_$phone",
                'register_source' => User::REGISTER_SOURCE_QR_CODE,
            ]);
            //如果是二级代理商 绑定二级-和一级的uid parent
            $agent_parent_id = $agent->parent_id;
            if(!empty($agent_parent_id))
            {
                $agent_parent_front_uid = $this->getFrontUidByAgentId($agent_parent_id);
                User::where('id',$agent->admin->front_uid)->update([
                    'parent_id'=>$agent_parent_front_uid,
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
            }
            //送积分
            Score::assignForRegisterByQrCode($user->id);
            //标记验证码已使用
//            $smsLog->update(['used_at' => now()]);
            return $user;
        });

        return api(RET_OK, RET_SUCCESS_MSG, $result);

    }

    /**
     * desc 关注用户&取消关注
     */
    public function followUser(Request $request)
    {
        $to_follow_uid = (int)$request->follow_uid;
        if(empty($to_follow_uid)) return api("请传递关注对象");
        $user = \Auth()->user();
        $follow_result = $user->toggleFollow($to_follow_uid);
        if(!empty($follow_result['attached'])) return api(RET_OK,'关注成功',[]);
        if(!empty($follow_result['detached'])) return api(RET_OK,'取消关注成功',[]);
    }

    /**
     * @desc 我的关注好友列表
     * @param Request $request
     */
    public function myFollows(Request $request)
    {
        $user = \Auth()->user();
        $follow_user = $user->followings()->get();
        if($follow_user)
        {
            foreach ($follow_user as $k=>$v)
            {
                //给每个好友添加粉丝数
                $follow_user[$k]->fans_nums = $v->followers()->get()->count();
            }
        }
        $follow_user = $follow_user->toArray();
        $type = (int)$request->type;
        $this->handleFollowUser($follow_user,$type);

        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$follow_user]);
    }

    /**
     * @desc 处理关注列表
     * @param $follow_user
     * @param $type //type 0 全部 1商家 2达人（普通用户）
     */
    public function handleFollowUser(&$follow_user,$type = 0)
    {
        if(!empty($follow_user))
        {
            foreach ($follow_user as $k=>$v)
            {
                $is_agent_user = Admin::where('front_uid',$v['id'])->exists();
                if($type == 1)
                {
                    if(!$is_agent_user) unset($follow_user[$k]);
                }
                if ($type == 2)
                {
                    if($is_agent_user) unset($follow_user[$k]);
                }
            }
        }
    }

    /**
     * @desc 关注人的发帖列表
     * @param Request $request
     */
    public function followUserTopic(Request $request)
    {
        $login_user = \Auth()->user();

        $uid = (int)$request->uid;
        try
        {
            $user = User::findOrFail($uid);
            $nickname = $user->nickname;
            $avatar = $user->avatar;
            $is_follow = $login_user->isFollowing($uid) ? 1 : 0;
            $be_followed_nums =  $user->followers()->get()->count();
            $follow_user_topics = Topic::with(['first_comment', 'first_comment.user'])->where([
                'uid'=>$uid
            ])->orderBy('id', 'desc')->paginate();
            $follow_user_topics = TopicResource::collection($follow_user_topics);

            return api(RET_OK,RET_SUCCESS_MSG,[
                'nickname'=>$nickname,
                'avatar'=>$avatar,
                'nickname'=>$nickname,
                'is_follow'=>$is_follow,
                'be_followed_nums'=>$be_followed_nums,
                'follow_user_topics'=>$follow_user_topics,
            ]);

        }
        catch (\Exception $exception)
        {
            return api('未知错误');
        }
    }

}

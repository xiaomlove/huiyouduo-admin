<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlipayController extends Controller
{
    public function notify(Request $request)
    {
        \Log::info(sprintf('%s, params: %s', __METHOD__, json_encode($request->all())));
        try {
            $alipay = \Pay::alipay(config('pay.alipay'));
            $result = $alipay->verify();
            \Log::info(sprintf(__METHOD__, $result->all()));
        } catch (\Exception $e) {
            \Log::error(sprintf('%s, exception: %s, msg: %s', __METHOD__, get_class($e), $e->getMessage()));
        }

    }

    public function redirect(Request $request)
    {
        \Log::info(sprintf('%s, params: %s', __METHOD__, json_encode($request->all())));

    }
}

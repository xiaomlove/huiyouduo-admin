<?php

namespace App\Http\Controllers\Api;

use App\Libraries\Taobao;
use App\Models\ItemsCate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = (new ItemsCate())->toTree();
        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$list]);
    }

    public function cate_list(Request $request)
    {
        $page = max(1,$request->page);
        $pagesize = min($request->pagesize,100) ?? 20;
        $cate_title = trim($request->title);
        if(empty($cate_title)) return api("请求错误");
        $sort = $request->sort;
        $goods_list = Taobao::superSearch($cate_title,[
            'sort'=>$sort,
            'page'=>$page,
            'pagesize'=>$pagesize,
            'coupon'=>1,
        ]);
        if(!empty($goods_list))
        {
            array_walk($goods_list, function (&$value, $key) {
                $value['coupon_price'] = Taobao::getCouponPrice($value['coupon_info']);
                $value['coupon_amount'] = $value['coupon_price'];
                $value['item_id'] = $value['num_iid'];
                $value['forecast_commission'] = Taobao::forecastCommission($value['zk_final_price'],$value['commission_rate'] / 10000);
                $value['coupon_after_price'] = max(0,sprintf('%.2f',$value['zk_final_price'] - Taobao::getCouponPrice($value['coupon_info'])));
            });
            (new HomePageController())->sortData($sort,$goods_list);
        }
        return api(RET_OK,RET_SUCCESS_MSG,['goods_list'=>$goods_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

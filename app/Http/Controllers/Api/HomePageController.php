<?php

namespace App\Http\Controllers\Api;

use App\Jobs\UserLowScoreSendQueue;
use App\Libraries\Taobao;
use App\Models\Banner;
use App\Models\Config;
use App\Models\ItemsCate;
use App\Models\OfficalMessage;
use App\Models\Score;
use App\Models\TaobaoPids;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Earnp\Getui\Getui;
use Illuminate\Support\Facades\Log;
use Overtrue\EasySms\Exceptions\NoGatewayAvailableException;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->test == 1)
        {
            $a = Taobao::getGoodsDetail(566352261884);
            dd($a);
            require "../app/Libraries/getuioffical/IGt.Push.php";
            $igt = new \IGeTui('https://sdk.open.api.igexin.com/apiex.htm','1SeR6rdXmBA8vZOIF9e4l9','MEzU9H9USh74es94qQfRa3');
            $ret = $igt->setBadgeForCID("0",'7PHSiRvW9C7WTEayNLog66',['aa']);
            var_dump($ret);
            die;
        }

        //顶部导航
        $head_cate_list = ItemsCate::where("is_recommend",1)->select("id","title","cate_img")->get()->toArray();
        $head_cate_all_list = array_merge([[
            'id'=>0,
            'title'=>'精选',
            'cate_img'=>'https://img.alicdn.com/imgextra/i3/2053469401/TB2LX03HKuSBuNjy1XcXXcYjFXa-2053469401.png']
        ],$head_cate_list);
        $head_cate_short_list = array_slice($head_cate_all_list,0,7);
        //轮播
        $home_banner = Banner::where('position',0)->orderBy('sort','desc')->get();
        //消息通知
        $office_message = OfficalMessage::orderBy('created_at','desc')->get()->toArray();
        //为你推荐
        $hot_products_list = Taobao::getHotProducts($request->page,$request->pagesize);

        if(!empty($hot_products_list))
        {
            array_walk($hot_products_list, function (&$value, $key) {
                $value['forecast_commission'] = Taobao::forecastCommission($value['zk_final_price'],$value['commission_rate'] / 100);
            });
        }
        $juhusuan_url = 'https://s.click.taobao.com/t?e=m=2&s=EUukdNCeVcgcQipKwQzePCperVdZeJvipRe/8jaAHci5VBFTL4hn2U84IxgAV0Nsuk7+EL5k7/yR4ypTBJBwtLCfUhe+6x3KiBqx4AoGTRwTMkUtS6f4UXPcTBqw3dK2S/oSpJwneKmHWVRb3Fch4/nJ73QslXMe9v8DmP3irl3iFG0C/RttyMjsgNlbAOUI0CeVVYOewcXqowVs3xqBTvH39oa051tFCzVj3d8VNdrWrWkPy5NaInc3u7xpLx4J/oSYWZg7pQw=&pid='.config('vetbk.tb_official_default_pid');

        $user_red_packet_nums = Config::getUserRedPacketNums();

        return api(RET_OK,RET_SUCCESS_MSG,[
            'head_cate_short_list'=>$head_cate_short_list,
            'head_cate_all_list'=>$head_cate_all_list,
            'home_banner'=>$home_banner,
            'office_message'=>$office_message,
            'today_hot_order_goods_list'=>[],//取消今日热销
            'hot_products_list'=>$hot_products_list,
            'juhuansuan_url'=>$juhusuan_url,
            'user_red_packet_nums'=>$user_red_packet_nums
        ]);
    }

    /**
     * @decs 今日热销-不用
     */
    public function today_hot_order(Request $request)
    {
        $pagesize = (int)$request->pagesize ? (int)$request->pagesize : 20;
        $today_hot_order = \DB::table('tbk_ofical_order')
            ->select('num_iid', \DB::raw('count(*) as total'))
            ->whereBetween('created_at',[date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59')])
            ->orderBy('total','desc')
            ->groupBy('num_iid')
            ->paginate($pagesize)
            ->toArray();
        $today_hot_order = $today_hot_order['data'];
        $today_hot_order_goods_list = [];
        if(!empty($today_hot_order))
        {
            foreach ($today_hot_order as $k=>$v)
            {
                $cur_order_goods_data = Taobao::superSearch($v->num_iid);
                if(!empty($cur_order_goods_data))
                {
                    $coupon_amount = Taobao::getCouponPrice($cur_order_goods_data['coupon_info']);
                    $today_hot_order_goods_list[] = [
                        'pict_url'=>$cur_order_goods_data['pict_url'],
                        'has_got_coupon_nums'=>$cur_order_goods_data['coupon_total_count'] - $cur_order_goods_data['coupon_remain_count'],
                        'coupon_name'=>$coupon_amount."元劵",
                        'title'=>$cur_order_goods_data['title'],
                        'price'=>$cur_order_goods_data['zk_final_price'],
                        'ori_price'=>$cur_order_goods_data['reserve_price'] ?? $cur_order_goods_data['zk_final_price'],
                        'coupon_after_price'=>max(0,sprintf('%.2f',$cur_order_goods_data['zk_final_price'] - $coupon_amount)),
                    ];
                }
            }
        }
        return api(RET_OK,RET_SUCCESS_MSG,['data_list'=>$today_hot_order_goods_list]);
    }

    /**
     * @desc 栏目首页
     * @param Request $request
     * @return array
     */
    public function cate_index(Request $request)
    {
        $cid = (int)$request->cid;
        $sort = $request->sort;
        if(empty($cid)) return api("请求错误");
        try
        {
            $cate = ItemsCate::findOrFail($cid);
        }
        catch (\Exception $exception)
        {
            return api("请求错误");
        }
        //顶部导航
        $head_cate_list = ItemsCate::where("is_recommend",1)->select("id","title","cate_img")->get()->toArray();
        $head_cate_all_list = array_merge([[
            'id'=>0,
            'title'=>'精选',
            'cate_img'=>'https://img.alicdn.com/imgextra/i3/2053469401/TB2LX03HKuSBuNjy1XcXXcYjFXa-2053469401.png']
        ],$head_cate_list);

        $head_cate_short_list = array_slice($head_cate_all_list,0,7);
        //轮播
        $home_banner = Banner::where('position',$cid)->get();
        $children_cate_list = ItemsCate::where('parent_id',$cid)->orderBy('order','desc')->limit(10)->get();
        //为你推荐
        $hot_products_list = Taobao::getHotProducts($request->page,$request->pagesize,$cate->title);
        if(!empty($hot_products_list))
        {
            array_walk($hot_products_list, function (&$value, $key) {
                $value['forecast_commission'] = Taobao::forecastCommission($value['zk_final_price'],$value['commission_rate'] / 100);
                $value['coupon_after_price'] = max(0,sprintf('%.2f',$value['zk_final_price'] - $value['coupon_amount']));
            });
            $this->sortData($sort,$hot_products_list);
        }
        return api(RET_OK,RET_SUCCESS_MSG,[
            'head_cate_short_list'=>$head_cate_short_list,
            'head_cate_all_list'=>$head_cate_all_list,
            'home_banner'=>$home_banner,
            'children_cate_list'=>$children_cate_list,
            'hot_products_list'=>$hot_products_list,
        ]);
    }

    /**
     * @desc 便宜商品
     */
    public function cheap_goods(Request $request)
    {
        $page = max(1,$request->page);
        $pagesize = min($request->pagesize,100) ?? 20;
        $sort = $request->sort;
        $hot_keywords = Taobao::getTaobaoHotKeywords();
        if(empty($hot_keywords)) return api(RET_OK,RET_SUCCESS_MSG,[]);
        $goods_list = Taobao::superSearch($hot_keywords,[
            'start_price'=>1,
            'end_price'=>10,
            'sort'=>$sort,
            'page'=>$page,
            'pagesize'=>$pagesize,
            'coupon'=>1,

        ]);
        if(!empty($goods_list))
        {
            array_walk($goods_list, function (&$value, $key) {
                $value['coupon_price'] = Taobao::getCouponPrice($value['coupon_info']);
                $value['forecast_commission'] = Taobao::forecastCommission($value['zk_final_price'],$value['commission_rate'] / 10000);
                $value['coupon_after_price'] = max(0,sprintf('%.2f',$value['zk_final_price'] - Taobao::getCouponPrice($value['coupon_info'])));
            });
            $this->sortData($sort,$goods_list);
        }
        return api(RET_OK,RET_SUCCESS_MSG,['goods_list'=>$goods_list]);
    }

    /**
     * @desc 排序数据
     * @param $sort
     * @param $json_data
     */
    public function sortData($sort,&$json_data)
    {
        switch ($sort)
        {
            case 'coupon_des':
                $json_data = collect($json_data)->sortByDesc('coupon_amount');
                break;
            case 'coupon_asc':
                $json_data = collect($json_data)->sortBy('coupon_amount');
                break;
            case 'tk_rate_des':
                $json_data = collect($json_data)->sortByDesc('forecast_commission');
                break;
            case 'tk_rate_asc':
                $json_data = collect($json_data)->sortBy('forecast_commission');
                break;
            case 'price_des':
                $json_data = collect($json_data)->sortByDesc('coupon_after_price');
                break;
            case 'price_asc':
                $json_data = collect($json_data)->sortByDesc('coupon_after_price');
                break;
            case 'total_sales_des':
                $json_data = collect($json_data)->sortByDesc('volume');
                break;
            case 'total_sales_asc':
                $json_data = collect($json_data)->sortByDesc('volume');
                break;
            default:
                $json_data = collect($json_data)->sortByDesc('coupon_start_time');
                break;
        }
        $json_data = array_values($json_data->toArray());
    }

    /**
     * @desc 淘宝或天猫商品
     * @param Request $request
     */
    public function taobaoOrTmall(Request $request)
    {
        $sort = $request->sort;
        $ptype = (int)$request->ptype;
        $hot_products_list = Taobao::getHotProducts($request->page,$request->pagesize,'','母婴',$ptype,$sort);

        if(!empty($hot_products_list))
        {
            array_walk($hot_products_list, function (&$value, $key) {
                $value['forecast_commission'] = Taobao::forecastCommission($value['zk_final_price'],$value['commission_rate'] / 100);
                $value['coupon_after_price'] = max(0,sprintf('%.2f',$value['zk_final_price'] - $value['coupon_amount']));
            });
            $this->sortData($sort,$hot_products_list);
        }
        return api(RET_OK,RET_SUCCESS_MSG,['goods_list'=>$hot_products_list]);
    }

    /**
     * @desc 热销推荐
     * @param Request $request
     */
    public function hotSaleRecommend(Request $request)
    {
        $subcate = ['美妆','女装','男装','家居','数码','鞋包','内衣','运动','食品','母婴'];
        $rand_index = array_rand($subcate);
        $subcate = $subcate[$rand_index];
        $top_cate = '热销';
        $sort = $request->sort;
        $hot_products_list = Taobao::getHotProducts($request->page,$request->pagesize,$subcate,$top_cate,'all',$sort);

        if(!empty($hot_products_list))
        {
            array_walk($hot_products_list, function (&$value, $key) {
                $value['forecast_commission'] = Taobao::forecastCommission($value['zk_final_price'],$value['commission_rate'] / 100);
                $value['coupon_after_price'] = max(0,sprintf('%.2f',$value['zk_final_price'] - $value['coupon_amount']));
            });
            $this->sortData($sort,$hot_products_list);
        }
        return api(RET_OK,RET_SUCCESS_MSG,['goods_list'=>$hot_products_list]);

    }

    /**
     * @desc 根据用户获取聚划算链接
     * @return array
     */
//    public function juhuasuanByUser()
//    {
//        $user = \Auth::user();
//        $uid = $user->id;
//        $tb_uid = $user->tb_uid;
//        if(!empty($tb_uid)) $pid = config('tb_official_default_pid');
//        else
//        {
//            $piddata = (new ItemsController())->findOnePid($uid);
//            if(!$piddata) return api('网络繁忙,请稍后重试');
//            $pid = $piddata->pid;
//        }
//        return api(RET_OK,RET_SUCCESS_MSG,['juhuasuan_url'=>sprintf('https://s.click.taobao.com/t?e=m=2&s=EUukdNCeVcgcQipKwQzePCperVdZeJvipRe/8jaAHci5VBFTL4hn2U84IxgAV0Nsuk7+EL5k7/yR4ypTBJBwtLCfUhe+6x3KiBqx4AoGTRwTMkUtS6f4UXPcTBqw3dK2S/oSpJwneKmHWVRb3Fch4/nJ73QslXMe9v8DmP3irl3iFG0C/RttyMjsgNlbAOUI0CeVVYOewcXqowVs3xqBTvH39oa051tFCzVj3d8VNdrWrWkPy5NaInc3u7xpLx4J/oSYWZg7pQw=&pid=%s',$pid)]);
//    }
    /**
     * @desc 今日推荐
     * @param Request $request
     */
    public function todayRecommendGoods(Request $request)
    {
        //为你推荐
        $sort = $request->sort;
        $hot_products_list = Taobao::getHotProducts($request->page,$request->pagesize,'','特价','all',$sort);

        if(!empty($hot_products_list))
        {
            array_walk($hot_products_list, function (&$value, $key) {
                $value['forecast_commission'] = Taobao::forecastCommission($value['zk_final_price'],$value['commission_rate'] / 100);
                $value['coupon_after_price'] = max(0,sprintf('%.2f',$value['zk_final_price'] - $value['coupon_amount']));
            });
            $this->sortData($sort,$hot_products_list);
        }
        return api(RET_OK,RET_SUCCESS_MSG,['goods_list'=>$hot_products_list]);
    }

    /**
     * @desc 拼团
     * @param Request $request
     * @return array
     */
    public function pintuan(Request $request)
    {
        $goods_list = Taobao::getPinTuan($request->page,$request->pagesize);
//        if(!empty($goods_list))
//        {
//            array_walk($goods_list, function (&$value, $key) {
//                $value['forecast_commission'] = Taobao::forecastCommission($value['zk_final_price'],$value['commission_rate'] / 100);
//            });
//        }
        return api(RET_OK,RET_SUCCESS_MSG,['goods_list'=>$goods_list]);
    }

    /**
     * @desc 领取红包
     * @param Request $request
     */
    public function takeRedPacket(Request $request)
    {
        $user = \Auth::user();
        $user_red_packet_nums = Config::getUserRedPacketNums();
        $has_taked = Score::query()
            ->where([
                'uid'=>$user->id,
                'type'=>Score::TYPE_USER_RED_PACKET
            ])
            ->exists();
        if($has_taked) return api("请勿重复领取红包");
        else
        {
            Score::assignForRedPacket($user->id,$user_red_packet_nums,"领取红包",[]);
            return api(RET_OK,"领取成功",[]);
        }
    }
    /**
     * @desc 超级搜索接口
     * @param Request $request
     */
    public function super_search(Request $request)
    {
        $key_words = trim($request->keywords);
        $sort = $request->sort;
        if(empty($key_words)) return api("请输入要查询的关键字");
        $page = max(1,$request->page);
        $pagesize = min($request->pagesize,100) ?? 20;

        $goods_list = Taobao::superSearch($key_words,[
            'page'=>$page,
            'pagesize'=>$pagesize,
        ]);
        if(!empty($goods_list))
        {
            $is_goodsid_request_search = isset($goods_list['num_iid']) ? 1 : 0;
            if($is_goodsid_request_search) $goods_list = [$goods_list];
            array_walk($goods_list, function (&$value, $key) {
                $value['coupon_price'] = Taobao::getCouponPrice($value['coupon_info']);
                $value['coupon_amount'] = $value['coupon_price'];
                $value['forecast_commission'] = Taobao::forecastCommission($value['zk_final_price'],$value['commission_rate'] / 10000);
                $value['coupon_after_price'] = max(0,sprintf('%.2f',$value['zk_final_price'] - Taobao::getCouponPrice($value['coupon_info'])));
            });
            $this->sortData($sort,$goods_list);
        }
        return api(RET_OK,RET_SUCCESS_MSG,['goods_list'=>$goods_list]);

    }

    /**
     *官方通知
     */
    public function officeMessage()
    {
        $office_message = OfficalMessage::orderBy('created_at','desc')->get();
        if(!$office_message->isEmpty())
        {
            foreach ($office_message as $k=>$v)
            {
                $office_message[$k]->image = imageUrl($v->image);
            }
        }
        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$office_message]);
    }

    /**
     * @desc 官方详情
     * @param Request $request
     */
    public function officeMessageDetail(Request $request)
    {
        $id = (int)$request->id;
        if(empty($id)) return api("参数有误");
        try
        {
            $offical_message = OfficalMessage::findOrFail($id);
            $offical_message->image = imageUrl($offical_message->image);
            return api(RET_OK,RET_SUCCESS_MSG,['detail'=>$offical_message]);
        }
        catch (\Exception $exception)
        {
            return api("参数有误");
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @desc 批量创建pid
     * @param Request $request
     */
    public function importMuiltPids(Request $request)
    {
        set_time_limit(0);
        $data_directory = resource_path("data/pids/*");
        $files = glob($data_directory);
        foreach ($files as $k=>$v)
        {
            $cur_file = file_get_contents($v);
            $cur_file_data = json_decode($cur_file,true);
            $list_data = $cur_file_data['data']['pagelist'] ?? [];
            if(!empty($list_data))
            {
                foreach ($list_data as $kk=>$vv)
                {
                    if(!empty($vv['memberid']) && !empty($vv['adzonePid']) && !empty($vv['adzoneid']) && !empty($vv['siteid']))
                    {
                        $has_insert = TaobaoPids::where('pid',$vv['adzonePid'])->exists();
                        if(!$has_insert)
                        {
                            TaobaoPids::create([
                                'adzone_id'=>$vv['adzoneid'],
                                'site_id'=>$vv['siteid'],
                                'unid'=>$vv['memberid'],
                                'pid'=>$vv['adzonePid'],
                            ]);
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        Log::info("批量pid失败：",json_encode($vv,336));
                    }
                }
            }
        }
        dd('excute sucess');

    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\TopicRepository;
use App\Http\Requests\CommentRequest;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = \Auth::guard('api')->user();
        $topic = Topic::findOrFail($request->tid);
        $with = [
            'user',
        ];
        if ($topic['type'] == Topic::TYPE_APPLY_JOB) {
            $with[] = ['applyJob', 'applyJob.province', 'applyJob.city', 'applyJob.district'];
        }
        $results = $topic->comments()->with($with)->orderBy('floor_num', 'asc')->paginate();
        //添加用户是否已点赞标识
        if ($user) {
            $userLikedComments = $user->likes()->where('target_type', Comment::class)->pluck('id', 'target_id')->all();
            foreach ($results->getIterator() as &$item) {
                $item->is_liked = isset($userLikedComments[$item->id]) ? 1 : 0;
            }
        }

        $resource = CommentResource::collection($results);
        if ($results->currentPage() == 1) {
            $firstComment = $topic->first_comment;
            if ($firstComment) {
                $firstComment ->increment('view_count');
            }
        }
        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $result = (new TopicRepository())->createComment($request);

        return api(RET_OK, RET_SUCCESS_MSG, $result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\TopicResource;
use App\Models\Admin;
use App\Models\AgentStore;
use App\Models\Comment;
use App\Repositories\ShareRepository;
use App\Repositories\TopicRepository;
use Illuminate\Http\Request;
use App\Http\Requests\TopicRequest;
use App\Http\Controllers\Controller;
use App\Models\Topic;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = \Auth()->user();
        $type = $request->type ?? Topic::TYPE_SHENGHUODAREN;//默认生活达人
        $type = (int)$type;
        $results = Topic::with(['first_comment', 'first_comment.user'])->when($type, function ($query) use ($type) {
            $query->where("type", $type);
        })->orderBy('id', 'desc')->paginate();

        //添加用户是否已点赞标识
        if ($user) {
            $userLikedComments = $user->likes()->where('target_type', Comment::class)->pluck('id', 'target_id')->all();
            foreach ($results->getIterator() as &$item) {
                if ($item->first_comment) {
                    $item->first_comment->is_liked = isset($userLikedComments[$item->first_comment->id]) ? 1 : 0;
                }
            }
        }
        $is_agent_user = Admin::where('front_uid', $user->id)->exists();
        $is_store_user = AgentStore::where('store_bind_uid',$user->id)->exists();
        $header_cate_list = [
            ['type'=>Topic::TYPE_SHENGHUODAREN,'name'=>Topic::$typeNames[Topic::TYPE_SHENGHUODAREN]],
            ['type'=>Topic::TYPE_GOUWUFENXIANG,'name'=>Topic::$typeNames[Topic::TYPE_GOUWUFENXIANG]],
            ['type'=>Topic::TYPE_ZHOUBIANFENXIANG,'name'=>Topic::$typeNames[Topic::TYPE_ZHOUBIANFENXIANG]],
        ];
        if($is_agent_user || $is_store_user) $header_cate_list[] = ['type'=>Topic::TYPE_APPLY_JOB,'name'=>Topic::$typeNames[Topic::TYPE_APPLY_JOB]];

        $resource = TopicResource::collection($results);
        $cur_type = ['type'=>$type,'name'=>Topic::$typeNames[$type]];
        $resource->additional(['header_cate_list'=>$header_cate_list,'cur_type'=>$cur_type]);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TopicRequest $request)
    {
        $result = (new TopicRepository())->createTopic($request, \Auth::user());

        return api(RET_OK, RET_SUCCESS_MSG, $result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

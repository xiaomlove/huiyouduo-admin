<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ApplyJobResource;
use App\Models\ApplyJob;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApplyJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = ApplyJob::with(['province', 'city', 'district', 'comment', 'comment.target'])
            ->orderBy('id', 'desc')
            ->paginate();

        $resource = ApplyJobResource::collection($results);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $resume = ApplyJob::with(['province', 'city', 'district'])->findOrfail($id);
        }
        catch (\Exception $exception)
        {
            return api($exception->getMessage());
        }
        return api(RET_OK,RET_SUCCESS_MSG,['resume'=>$resume]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}

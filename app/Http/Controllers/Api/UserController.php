<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UserRequest;
use App\Http\Resources\StoreResource;
use App\Http\Resources\UserMoneyWaterLogResource;
use App\Http\Resources\UserResource;
use App\Models\AgentStore;
use App\Models\ApplyJob;
use App\Models\Config;
use App\Models\CouponCard;
use App\Models\Score;
use App\Models\TaobaoPids;
use App\Models\TbkOficalOrder;
use App\Models\Topic;
use App\Models\UserFinanceAccount;
use App\Models\UserThirdPartyBind;
use App\Models\WithdrawalLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = User::findOrFail($id);

        $resource = new UserResource($result);

        $alipay_account = UserFinanceAccount::where('uid',$id)->first();
        if(!$alipay_account) $alipay_account = "";
        else $alipay_account = $alipay_account->account;
        //添加qq和微信是否绑定
        $bind_qq = UserThirdPartyBind::where([
            'uid'=>$id,
            'platform'=>UserThirdPartyBind::PLATFORM_QQ
        ])->first();
        $bind_qq = $bind_qq ? $bind_qq->nickname : "";
        $bind_wechat = UserThirdPartyBind::where([
            'uid'=>$id,
            'platform'=>UserThirdPartyBind::PLATFORM_WEIXIN_OPEN
        ])->first();
        $bind_wechat = $bind_wechat ? $bind_wechat->nickname : "";
        $resource->additional(['alipay_account'=>$alipay_account,'bind_qq'=>$bind_qq,'bind_wechat'=>$bind_wechat]);
        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $update = $request->only(['name', 'nickname', 'avatar', 'password']);
        $update = array_filter($update);
        if (!empty($update['password'])) {
            $update['password'] = bcrypt($update['password']);
        }
//        dd($update);
        $user->update($update);

        return api(RET_OK, RET_SUCCESS_MSG, $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profile()
    {
        $result = \Auth::user();
        $resource = new UserResource($result);

        //合作商家
        $user_store = AgentStore::where('store_bind_uid',$result->id)->first();
        $is_hezuo_shangjia = 0;
        if($user_store) $is_hezuo_shangjia = 1;
        if($is_hezuo_shangjia)
            $has_bind_store_cards_nums = CouponCard::where('bind_store_id',$user_store->id)->where('bind_uid','>',0)->count();
        else
            $has_bind_store_cards_nums = 0;

        $fans_nums =  $result->followers()->get()->count();

        $has_filled_apply_job = ApplyJob::where('uid',$result->id)->exists();
        //添加官方客服
        $office_mobile = Config::getOfficeMobile();
        //添加签到积分赠送数值
        $sign_score_nums = Config::getSignScoreNums();
        $resource->additional([
            'money' => $result->money / 100,
            'is_hezuo_shangjia'=>$is_hezuo_shangjia,
            'shangjia_users'=>$has_bind_store_cards_nums,
            'fans_nums'=>$fans_nums,
            'has_filled_apply_job'=>$has_filled_apply_job,
            'office_mobile'=>$office_mobile,
            'sign_score_nums'=>$sign_score_nums,
            'has_sign_in' => $result->hasSignInToday(),
        ]);
        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    /**
     * 我的简历
     * @param Request $request
     */
    public function myResume(Request $request)
    {
        $user = \Auth::user();
        $myresume = ApplyJob::where('uid',$user->id)->first();
        return api(RET_OK,RET_SUCCESS_MSG,['myresume'=>$myresume]);
    }

    /**
     * @desc 修改我的简历
     * @param Request $request
     */
    public function updateMyResume(Request $request)
    {
        $user = \Auth::user();
        $myresume = ApplyJob::where('uid',$user->id)->first();
        if(!$myresume) return api("没有发过简历");
        $update_data = [
            'intention'=>$request->intention,
            'province_code'=>$request->province_code,
            'city_code'=>$request->city_code,
            'district_code'=>$request->district_code,
            'phone'=>$request->phone,
            'username'=>$request->username,
            'sex'=>$request->sex,
            'age'=>$request->age,
            'email'=>$request->email,
            'works'=>$request->works,
            'tpl_type'=>$request->tpl_type,
        ];
        $update_data = array_filter($update_data);
        $update = ApplyJob::where('uid',$user->id)->update($update_data);
        return api(RET_OK,RET_SUCCESS_MSG,['myresume'=>ApplyJob::where('uid',$user->id)->first()]);
    }

    public function moneyWaterLog()
    {
        $results = \Auth::user()->moneyWaterLogs()->paginate();
        $resource = UserMoneyWaterLogResource::collection($results);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    /**
     * 兑换记录
     * @param Request $request
     */
    public function exchangeLog(Request $request)
    {
        $user = \Auth::user();
        $exchange_log = Score::where('score','<',0)
            ->whereIn('type',[Score::TYPE_EXCHANGE_ITEM,Score::TYPE_LUCKY_DRAW_AWARD])
            ->where('uid',$user->id)
            ->orderBy('created_at','desc')->paginate();
        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$exchange_log]);

    }

    /**
     * @desc 收益主页
     * @param Request $request
     */
    public function userEarnList(Request $request)
    {
        $user = \Auth::user();
        //用户总收益-就是用户申请提现 已完成的总计
        $all_earn = $user->withdrawalLogs()->channelFront()->statusSuccess()->sum('money') / 100;
        $money = $user->money;//余额
        //上个月的结算收入
        $start_time = date("Y-m-01 00:00:00", strtotime("-1 month"));
        $end_time = date('Y-m-t 23:59:59', strtotime("-1 month"));
        $last_month_earn = WithdrawalLog::whereBetween('created_at',[$start_time,$end_time])->channelFront()->statusSuccess()->sum('money') / 100;
        //本月预估收益
        list($this_month_forcast_earn_count,$this_month_forcast_earn) = $this->user_order_forcast_earn($user->id,$user->tb_uid,1);
        //上月预估收益
        list($last_month_forcast_earn_count,$last_month_forcast_earn) = $this->user_order_forcast_earn($user->id,$user->tb_uid,2);
        //今日数据
        list($today_forcast_earn_count,$today_forcast_earn) = $this->user_order_forcast_earn($user->id,$user->tb_uid,3);
        //本周
        list($this_week_forcast_earn_count,$this_week_forcast_earn) = $this->user_order_forcast_earn($user->id,$user->tb_uid,4);

        return api(RET_OK,RET_SUCCESS_MSG,[
            'all_earn'=>$all_earn,
            'money'=>$money,
            'last_month_earn'=>$last_month_earn,
            'this_month_forcast_earn'=>$this_month_forcast_earn,
            'last_month_forcast_earn'=>$last_month_forcast_earn,
            'today_forcast_earn_count'=>$today_forcast_earn_count,
            'today_forcast_earn'=>$today_forcast_earn,
            'other_today'=>0,
            'this_week_forcast_earn_count'=>$this_week_forcast_earn_count,
            'this_week_forcast_earn'=>$this_week_forcast_earn,
            'other_week'=>0,

        ]);
    }

    /**
     * @desc 用户的预估佣金
     * @param int $uid
     * @param int $count_type 1:本月；2：上月，3：今日，4：本周
     * @return array
     */
    protected function user_order_forcast_earn($uid = 0,$tb_uid = 0,$count_type = 1)
    {
        if(empty($uid)) return;
        $user_order_list = [];
        if($count_type == 1)
        {
            $start_time = date("Y-m-01 00:00:00", time());
            $end_time = date('Y-m-t 23:59:59', time());
        }
        elseif ($count_type == 2)
        {
            $start_time = date("Y-m-01 00:00:00", strtotime("-1 month"));
            $end_time = date('Y-m-t 23:59:59', strtotime("-1 month"));
        }
        elseif ($count_type == 3)
        {
            $start_time = date("Y-m-d 00:00:00", time());
            $end_time = date('Y-m-d 23:59:59', time());
        }
        elseif ($count_type == 4)
        {
            //当前日期
            $sdefaultDate = date("Y-m-d");
            $first = 1;
            $w = date('w',strtotime($sdefaultDate));
            $week_start = date('Y-m-d 00:00:00',strtotime("$sdefaultDate -".($w ? $w - $first : 6).' days'));
            $week_end = date('Y-m-d 23:59:59',strtotime("$week_start +6 days"));
            $start_time = $week_start;
            $end_time = $week_end;
        }
        if(!empty($tb_uid))
        {
            //这个是怎么筛选订单的 uid  和 订单 后6位 匹配
            $user_order_list = TbkOficalOrder::where([
                'tk_status'=>2,
            ])->whereBetween('earning_time',[$start_time,$end_time])
                ->select(
                    \DB::raw('commission * 0.89  as commission_cal'),
                    'trade_parent_id'
                )->get()->toArray();
            if(!empty($user_order_list))
            {
                foreach ($user_order_list as $k=>$v)
                {
                    if($tb_uid != handle_tbk_trade_parent_id($v['trade_parent_id']))
                    {
                        unset($user_order_list[$k]);
                    }
                    else
                    {
                        unset($user_order_list[$k]['trade_parent_id']);
                    }
                }
            }
        }
        else
        {
            //这种是绑定pid的用户
            $tb_pid_info = TaobaoPids::where("uid",$uid)->first();
            if($tb_pid_info)
            {
                $adzone_id = $tb_pid_info->adzone_id;
                $site_id = $tb_pid_info->site_id;
                $user_order_list = TbkOficalOrder::where([
                    "adzone_id"=>$adzone_id,
                    "site_id"=>$site_id,
                    'tk_status'=>2
                ])->whereBetween('earning_time',[$start_time,$end_time])
                    ->select(
                        \DB::raw('commission * 0.89 as commission_cal')
                    )->get()->toArray();//commission
            }
        }
        if(empty($user_order_list)) return [0,0];
        else
        {
            $user_percent = Config::getUserCommissionPercent() / 100;
            return [count($user_order_list),collect($user_order_list)->sum('commission_cal') * $user_percent];
        }
    }



    /**
     * @desc 用户的收益订单列表
     * @param int $uid
     * @return array
     */
    public function userEarnOrder(Request $request)
    {
        $user = \Auth::user();
        $uid = $user->id;
        $tb_uid = $user->tb_uid;
        $type = $request->type ?? 1;//0是维权订单 1是结算订单
        $user_order_list = [];
        if(!empty($tb_uid))
        {
            //这个是怎么筛选订单的 uid  和 订单 后6位 匹配
            $user_order_list = TbkOficalOrder::
            where('is_settled',$type)
            ->select(
                "pay_price",
                'create_time',
                'trade_parent_id'
            )->orderBy('create_time','desc')->paginate()->toArray();
            if(!empty($user_order_list))
            {
                foreach ($user_order_list as $k=>$v)
                {
                    if($tb_uid != handle_tbk_trade_parent_id($v['trade_parent_id']))
                    {
                        unset($user_order_list[$k]);
                    }
                    else
                    {
                        unset($user_order_list[$k]['trade_parent_id']);
                    }
                }
            }
        }
        else
        {
            //这种是绑定pid的用户
            $tb_pid_info = TaobaoPids::where("uid",$uid)->first();
            if($tb_pid_info)
            {
                $adzone_id = $tb_pid_info->adzone_id;
                $site_id = $tb_pid_info->site_id;
                $user_order_list = TbkOficalOrder::where([
                    "adzone_id"=>$adzone_id,
                    "site_id"=>$site_id,
                ])->where('is_settled',$type)
                    ->select(
                    "pay_price",
                    'create_time'
                )->orderBy('create_time','desc')->paginate()->toArray();//
            }
        }
        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$user_order_list]);
    }

    /**
     * @desc 合作商家
     * @param Request $request
     */
    public function cooperPartner(Request $request)
    {
        $user = \Auth::user();
        $user_store = AgentStore::where('store_bind_uid',$user->id)->first();
        if(!$user_store) return api(RET_OK,RET_SUCCESS_MSG,[]);
        $resource = new StoreResource($user_store);
        $query_month = $request->query_month;
        $start_time = $end_time = 0;
        if(!empty($query_month))
        {
            $timestamp = strtotime( $query_month );
            $start_time = date( 'Y-m-1 00:00:00', $timestamp );
            $mdays = date( 't', $timestamp );
            $end_time = date( 'Y-m-' . $mdays . ' 23:59:59', $timestamp );
        }
        //月份总卡数
        $total_card_nums = CouponCard::when($query_month,function ($query) use ($start_time,$end_time){
            return $query->whereBetween('created_at',[$start_time,$end_time]);
        })->where('bind_store_id',$user_store->id)->count();
        $has_bind_nums = CouponCard::when($query_month,function ($query) use ($start_time,$end_time){
            return $query->whereBetween('created_at',[$start_time,$end_time]);
        })->where('bind_store_id',$user_store->id)->where('bind_uid','>',0)->count();
        $un_bind_nums = $total_card_nums - $has_bind_nums;

        //今日-本周-本月
        $today_bind_nums = CouponCard::whereBetween('used_at',[date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59')])
            ->where('bind_store_id',$user_store->id)->where('bind_uid','>',0)->count();
        $week_start_time = date('Y-m-d 00:00:00', strtotime("this week Monday", time()));
        $week_end_time = date('Y-m-d 23:59:59',strtotime(date('Y-m-d', strtotime("this week Sunday", time()))) + 24 * 3600 - 1);
        $week_bind_nums = CouponCard::whereBetween('used_at',[$week_start_time,$week_end_time])
            ->where('bind_store_id',$user_store->id)->where('bind_uid','>',0)->count();
        $month_start_time = date('Y-m-d 00:00:00',mktime(0, 0, 0, date('m'), 1, date('Y')));
        $month_end_time = date('Y-m-d 23:59:59',mktime(23, 59, 59, date('m'), date('t'), date('Y')));

        $month_bind_nums = CouponCard::whereBetween('used_at',[$month_start_time,$month_end_time])
            ->where('bind_store_id',$user_store->id)->where('bind_uid','>',0)->count();

        $card_nums_data = [
            'total_card_nums'=>$total_card_nums,
            'has_bind_nums'=>$has_bind_nums,
            'un_bind_nums'=>$un_bind_nums,
        ];
        $user_nums_data = [
            'today_bind_nums'=>$today_bind_nums,
            'week_bind_nums'=>$week_bind_nums,
            'month_bind_nums'=>$month_bind_nums,
        ];
        return api(RET_OK,RET_SUCCESS_MSG,['store'=>$resource,'card_nums_data'=>$card_nums_data,'user_nums_data'=>$user_nums_data]);



    }

}

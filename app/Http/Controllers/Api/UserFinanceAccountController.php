<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UserFinanceAccountRequest;
use App\Http\Resources\UserFinanceAccountResource;
use App\Models\UserFinanceAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserFinanceAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFinanceAccountRequest $request)
    {

        $result = \Auth::user()->financeAccounts()->create($request->all());

        return api(RET_OK, RET_SUCCESS_MSG, $result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = \Auth::user()->financeAccounts()->findOrFail($id);
        $resource = new UserFinanceAccountResource($result);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserFinanceAccountRequest $request, $id)
    {
        $result = $result = \Auth::user()->financeAccounts()->findOrFail($id);
        $result->update($request->except(['uid', 'type']));

        return api(RET_OK, RET_SUCCESS_MSG, $result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function typeJson()
    {
        $results = UserFinanceAccount::$typeNames;

        return api(RET_OK, RET_SUCCESS_MSG, $results);
    }
}

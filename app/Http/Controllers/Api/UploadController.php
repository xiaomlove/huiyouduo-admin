<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UploadController extends Controller
{
    protected $imageFieldName = 'image';
    /**
     * 图片上传
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function image(Request $request)
    {
        $image = $request->file($this->imageFieldName);
        if (empty($image)) return api("没有上传name为{$this->imageFieldName}的图片");

        $relativePath = sprintf('/public/uploads/image/%s', date('Y/m/d'));

        $result = \Storage::putFile($relativePath, $image);
        if (!$result) return api("保存到{$relativePath}失败");
        $filename = pathinfo($result, PATHINFO_BASENAME);
        $key = "{$relativePath}/{$filename}";
        $url = \Storage::disk()->url($key);
        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$url]);
    }
}

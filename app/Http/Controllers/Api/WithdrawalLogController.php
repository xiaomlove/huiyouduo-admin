<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\WithdrawalLogRequest;
use App\Http\Resources\UserFinanceAccountResource;
use App\Http\Resources\WithdrawalLogResource;
use App\Models\UserFinanceAccount;
use App\Models\WithdrawalLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WithdrawalLogController extends Controller
{

    public function create()
    {
        $user = \Auth::user();
        $alipayAccount = $user->financeAccounts()->where('type', UserFinanceAccount::TYPE_ALIPAY)->first();
        if (!$alipayAccount) {
            return api(RET_BIND_ALIPAY, '请先绑定支付宝', []);
        }
        $resource = new UserFinanceAccountResource($alipayAccount);
        $resource->additional(['money' => $user->money / 100]);
        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = \Auth::user()->withdrawalLogs()->channelFront()->paginate();

        $resource = WithdrawalLogResource::collection($results);

        return api(RET_OK, RET_SUCCESS_MSG, $resource);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WithdrawalLogRequest $request)
    {
        $data = $request->all();
        $data['channel'] = WithdrawalLog::CHANNEL_FRONT;
        $result = \DB::transaction(function () use ($data) {
            $result = \Auth::user()->withdrawalLogs()->create($data);
            return $result;
        });

        return api(RET_OK, RET_SUCCESS_MSG, $result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

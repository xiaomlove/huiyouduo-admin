<?php

namespace App\Http\Controllers\Api;

use App\Models\PushMessageLog;
use App\Models\PushMessageState;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\LBSRepository;
use Illuminate\Support\Facades\Log;

class PushMessageLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //获取用户的省份
        $user_province = (new LBSRepository())->getUserProvinceByIp($request);

        //获取用户的推送 全国的 + 省份的
        $page_size = (int)$request->page_size ? (int)$request->page_size : 20;

        $push_message_log_list = PushMessageLog::where(function($query) use ($user_province) {
            $query->whereRaw('FIND_IN_SET(?,push_province_name)', [$user_province]);
            $query->orWhere('push_province_name',"");
        })->paginate($page_size)->toArray();

        //每次进入这个列表 就直接将我的所有消息都置为已经读取
        //个推接口 删除小红点
        if(!empty(\Auth::user()->clientid))
        {
            require "../app/Libraries/getuioffical/IGt.Push.php";
            $igt = new \IGeTui('https://sdk.open.api.igexin.com/apiex.htm','1SeR6rdXmBA8vZOIF9e4l9','MEzU9H9USh74es94qQfRa3');
            $ret = $igt->setBadgeForCID("0",'7PHSiRvW9C7WTEayNLog66',[\Auth::user()->clientid]);
            Log::info("删除小红点返回结果：".json_encode($ret));
        }

        if(!empty($push_message_log_list['data']))
        {
            $my_push_message_ids = array_column($push_message_log_list['data'], 'id');
            if(!empty($my_push_message_ids))
            {
                foreach ($my_push_message_ids as $k=>$v)
                {
                    $is_insert_push_message_state = PushMessageState::where([
                        "message_id"=>$v,
                        "uid"=>\Auth::user()->id,
                    ])->exists();
                    if(!$is_insert_push_message_state)
                    {
                        PushMessageState::create([
                            "message_id"=>$v,
                            "uid"=>\Auth::user()->id,
                            "is_read"=>1
                        ]);
                    }
                }
            }
        }

        return api(RET_OK,RET_SUCCESS_MSG,['push_message_list'=>$push_message_log_list['data']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Libraries\Taobao;
use App\Models\Config;
use App\Models\Score;
use App\Models\TaobaoPids;
use App\Models\TbkOficalOrder;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $items = Taobao::superSearch($id);
        if(empty($items)) return api(RET_OK,RET_SUCCESS_MSG,['list'=>[],'recommend_goods_list'=>[]]);
        if(!empty($items))
        {
            if(isset($items['coupon_info']))
                $coupon_price = Taobao::getCouponPrice($items['coupon_info']);
            else
                $coupon_price = 0;
            Log::info(json_encode($items,336));
            $items['coupon_after_price'] = max(0,sprintf('%.2f',$items['zk_final_price'] - $coupon_price));
            $items['items_content'] = Taobao::getGoodsDetail($id);
            $items['items_content'] = $items['items_content'] ? \GuzzleHttp\json_decode($items['items_content'],true) : [];
            $items['coupon_click_url'] = '';
            $items['coupon_short_url'] = '';
            $items['forecast_commission'] = Taobao::forecastCommission($items['zk_final_price'],$items['commission_rate'] / 100);
        }
        //添加为你推荐
        $recommend_goods_list = Taobao::superSearch($items['cat_name'],[
            'page'=>1,
            'pagesize'=>10,
            'coupon'=>1,
        ]);
        if(!empty($recommend_goods_list))
        {
            array_walk($recommend_goods_list, function (&$value, $key) {
                $value['coupon_price'] = Taobao::getCouponPrice($value['coupon_info']);
                $value['forecast_commission'] = Taobao::forecastCommission($value['zk_final_price'],$value['commission_rate'] / 10000);
                $value['coupon_after_price'] = max(0,sprintf('%.2f',$value['zk_final_price'] - Taobao::getCouponPrice($value['coupon_info'])));
            });
        }
        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$items,'recommend_goods_list'=>$recommend_goods_list]);
    }

    /**
     * @desc 商品分享送积分 每个商品送
     */
    public function item_share_score()
    {
        $getShareItemScore = Config::getShareItemScore();

    }

    /**
     * @desc 商品兑换优惠券
     * @param Request $request
     */
    public function exchange_counpon(Request $request)
    {
        $items_id = (int)$request->items_id;
        if(empty($items_id)) return api("请传入商品id");
        $user = \Auth::user();
        $uid = $user->id;
        $tb_uid = $user->tb_uid;
        if(!empty($tb_uid)) $pid = config('tb_official_default_pid');
        else
        {
            $piddata = $this->findOnePid($uid);
            if(!$piddata) return api('网络繁忙,请稍后重试');
            $pid = $piddata->pid;
        }
        $data = Taobao::hcapiItem($items_id,$pid);
        $items = Taobao::superSearch($items_id);
        if(empty($data)) return api('网络繁忙,请稍后重试');
        $data['name'] = $items['title'] ?? "";
        $coupon_click_url = "";
        if(isset($data['original_uland_link']))
            $list = [
                'has_coupon'=>0,
                'coupon_click_url'=>$data['coupon_click_url']
            ];
        else
            $list = [
                'has_coupon'=>1,
                'coupon_click_url'=>$data['coupon_click_url']
            ];
         //绑定pid和uid
        //扣除积分
        DB::beginTransaction();
        try
        {
            $piddata->uid = $uid;
            $piddata->save();
            if(!empty($data['coupon_info']))
            {
                //扣除用户积分的
                //用户积分是否充足
                $user_score_total = \Auth::user()->scores;
                $coupon_amount = Taobao::getCouponPrice($data['coupon_info']);
                if($user_score_total < $coupon_amount) return api("积分余额不足，请充值后兑换");
                //消耗积分
                Score::assignForExchangeItem($uid,$coupon_amount,"领券",$data);
            }
            //模拟添加订单数据
//            if(env('APP_ENV') == 'local')
//            {
//                $price = mt_rand(1,999);
//                $commission = sprintf('%.2f',mt_rand(1,900) / 100);
//                TbkOficalOrder::create([
//                    'adzone_id'=>$piddata->adzone_id,
//                    'adzone_name'=>'测试',
//                    'item_num'=>1,
//                    'trade_id'=>mt_rand(1,99999),
//                    'trade_parent_id'=>mt_rand(1,99999),
//                    'alipay_total_price'=>$price,
//                    'click_time'=>date('Y-m-d H:i:s'),
//                    'commission'=>$commission,
//                    'create_time'=>date('Y-m-d H:i:s'),
//                    'item_title'=>'sadasdasd',
//                    'num_iid'=>$items_id,
//                    'pay_price'=>$price,
//                    'site_id'=>$piddata->site_id,
//                    'tk_status'=>3,
//                    'total_commission_fee'=>$commission,
//                    'earning_time'=>date('Y-m-d H:i:s'),
//                ]);
//            }
            DB::commit();
            return api(RET_OK,RET_SUCCESS_MSG,$list);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return api($exception->getMessage());
        }

    }

    /**
     * @desc 获取一个可用pid
     */
    public function findOnePid($uid)
    {
        $piddata = TaobaoPids::where("uid",$uid)->first();
        if($piddata) return $piddata;
        else
        {
            $piddata = TaobaoPids::where("uid",0)->first();
            if(!$piddata) return false;
            else return $piddata;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\UserFeedBack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserFeedBackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = UserFeedBack::where("is_audit",1)->orderBy("created_at",'desc')->get()->toArray();
        return api(RET_OK,RET_SUCCESS_MSG,['list'=>$list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->title;
        $contends = $request->contents;
        if(empty($title) || empty($contends)) return api("填写不完整");
        $data = UserFeedBack::create([
            "title"=>$title,
            "contents"=>$contends,
        ]);
        return api(RET_OK,RET_SUCCESS_MSG,['detail'=>$data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $data = UserFeedBack::findOrFail($id);
            return api(RET_OK,RET_SUCCESS_MSG,['detail'=>$data]);
        }
        catch (\Exception $exception)
        {
            return api("错误");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

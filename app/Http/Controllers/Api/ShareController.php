<?php

namespace App\Http\Controllers\Api;

use App\Models\AgentStore;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Topic;
use App\Models\StoreActivity;
use App\Repositories\ShareRepository;

class ShareController extends Controller
{
    public function topic($id)
    {
        $topic = Topic::findOrFail($id);
        $user = \Auth::user();
        $exists = $topic->shareLogs()->where('uid', $user->id)->count();
        if ($exists) {
            return api('用户已经分享过');
        }
        $shareLog = (new ShareRepository())->createForTopic($topic, $user);
        return api(RET_OK, RET_SUCCESS_MSG, $shareLog);
    }

    public function activity($id)
    {
        $activity = StoreActivity::findOrFail($id);
        $user = \Auth::user();
        $exists = $activity->shareLogs()->where('uid', $user->id)->count();
        if ($exists) {
            return api('用户已经分享过');
        }
        $shareLog = (new ShareRepository())->createForStoreActivity($activity, $user);
        return api(RET_OK, RET_SUCCESS_MSG, $shareLog);
    }

    public function store($id)
    {
        $store = AgentStore::findOrFail($id);
        $user = \Auth::user();
        $exists = $store->shareLogs()->where('uid', $user->id)->count();
        if ($exists) {
            return api('用户已经分享过');
        }
        $shareLog = (new ShareRepository())->createForAgentStore($store, $user);
        return api(RET_OK, RET_SUCCESS_MSG, $shareLog);
    }
}

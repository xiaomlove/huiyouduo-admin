<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Models\Agent;
use Encore\Admin\Auth\Permission as Checker;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function canAccessAgent($agentId)
    {
        $admin = \Admin::user();
        if (!$admin) {
            return Checker::error();
        }
        if ($admin->isSuperior()) {
            return true;
        }
        $agent = Agent::findOrFail($agentId);
        $adminAgentId = $admin->agent->id;
        if ($agent->id != $adminAgentId && $agent->parent_id != $adminAgentId) {
            return Checker::error();
        }
    }
}

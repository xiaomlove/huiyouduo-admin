<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->routeIs('api*'))
        {
            $modelName = get_class($exception);
            if (($pos = strrpos($modelName, '\\')) !== false)
            {
                $modelName = substr($modelName, $pos + 1);
            }
            $msg = sprintf("%s %s", $modelName, $exception->getMessage());
            if ($exception instanceof ValidationException)
            {
                $errors = $exception->errors();
                $msg = sprintf("%s", array_first(array_first($errors)));
                return response()->json(api($msg, [
                    'errors' => $errors,
                    'params' => $request->all(),
                ]), $exception->status);
            }
            elseif ($exception instanceof AuthenticationException)
            {
                return response()->json(api(RET_UN_AUTH, $msg, $request->all()), 401);
            }
            else
            {
                return response()->json(api($msg, $request->all()), 500);
            }
        }
        return parent::render($request, $exception);
    }
}

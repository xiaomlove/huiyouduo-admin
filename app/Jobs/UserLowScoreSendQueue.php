<?php

namespace App\Jobs;

use App\Models\Config;
use App\Models\Score;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Overtrue\EasySms\EasySms;
use Overtrue\EasySms\Exceptions\NoGatewayAvailableException;

class UserLowScoreSendQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //直接将少于多少积分的用户赠送积分
        $user_low_score_send = Config::getUserLowScoreSend();
        $user_low_score_send_nums = Config::getUserLowScoreSendNums();
        $low_score = User::where('scores','<',$user_low_score_send)->get()->toArray();
        if(!empty($low_score))
        {
            foreach ($low_score as $k=>$v)
            {
                Score::assignForOther($v['id'],$user_low_score_send_nums,"系统赠送积分",[]);
                //发短信
                $config = config('sms');
                $easySms = new EasySms($config);
                if(empty($v['phone'])) continue;
                $result = $easySms->send($v['phone'], [
                    'content'  => sprintf('尊敬的%s, 送您%s积分已到账，详情请进入惠友多APP查看',$v['name'],$user_low_score_send_nums),
                    'template' => $config["template_id_score_send"],
                    'data' => [
                        'name' => $v['name'],
                        'points' => $user_low_score_send_nums,
                    ],
                ]);
                Log::info("低于多少送积分日志记录：".\GuzzleHttp\json_encode($result));
            }
        }
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


$attributes = [
    'namespace' => 'Api',
    'as' => 'api.',
];

//免认证部分路由
Route::group($attributes, function ($router) {
    $router->resource('test', 'TestController');//测试专用
//    $router->post('upload-image', 'UploadController@image')->name('upload-image');
    $router->resource('cate', 'CateController');//
    $router->resource('homepage', 'HomePageController');//
    $router->resource('user-feed-back', 'UserFeedBackController');//
    $router->get('homepage-super-search', 'HomePageController@super_search');//超级搜索
    $router->get('homepage-import-pids', 'HomePageController@importMuiltPids');//批量创建pid
    $router->get('homepage-today-hot-order', 'HomePageController@today_hot_order');//超级搜索
    $router->resource('items', 'ItemsController');//
    $router->get('homepage-cate-index', 'HomePageController@cate_index')->name('homepage-cate-index');//
    $router->get('homepage-office-message', 'HomePageController@officeMessage')->name('homepage-office-message');//
    $router->get('homepage-office-message-detail', 'HomePageController@officeMessageDetail')->name('homepage-office-message-detail');//
    $router->get('homepage-taobao-or-tmall', 'HomePageController@taobaoOrTmall')->name('homepage-taobao-or-tmall');//
    $router->get('homepage-today-recommendgoods', 'HomePageController@todayRecommendGoods')->name('homepage-today-recommendgoods');//
    $router->get('homepage-pintuan', 'HomePageController@pintuan')->name('homepage-pintuan');//
    $router->get('homepage-hot-salerecommend', 'HomePageController@hotSaleRecommend')->name('homepage-hot-salerecommend');//
    $router->get('homepage-cheap-goods', 'HomePageController@cheap_goods')->name('homepage-cheap-goods');//
    $router->get('cate-list', 'CateController@cate_list')->name('cate-list');//


    $router->post('auth/sms-verify-code', 'AuthenticateController@smsVerifyCode')->name('sms-verify-code');
    $router->post('auth/login-phone', 'AuthenticateController@loginByPhone')->name('login-by-phone');
    $router->post('auth/login-weixin-open', 'AuthenticateController@loginByWeixinOpen2')->name('login-by-weixin-open');
    $router->post('auth/login-qq', 'AuthenticateController@loginByQQ')->name('login-by-qq');
    $router->resource('store', 'StoreController');
    $router->get('promotion/qrcode', 'OtherController@promotionQrcode')->name('promotion.qrcode');
    $router->post('auth/register-qrcode', 'AuthenticateController@registerByQrcode')->name('register.qrcode');
    $router->get('promotion/qrcode-success', 'OtherController@promotionQrCodeSuccess')->name('promotion.qrcode.success');
    $router->get('activity/{id}', 'StoreController@activity');

    $router->get('feedbacklist', 'OtherController@feedbacklist')->name('feedback.list');
    $router->get('feedbackshow', 'OtherController@feedbackshow')->name('feedback.show');


    $router->any('alipay/notify', 'AlipayController@notify')->name('alipay.notify');
    $router->any('alipay/redirect', 'AlipayController@redirect')->name('alipay.redirect');
    $router->get('district-json', 'OtherController@districtJson');

//    $router->get('topic', 'TopicController@index');//修改社区要强制登陆
    $router->get('comment', 'CommentController@index');
    $router->get('oss-upload-token', 'OtherController@ossUploadToken');
    $router->get('verifyprize', 'LuckyDrawController@verifyprize')->name('verifyprize');
    $router->get('about', 'OtherController@about')->name('about');

});


/*--------------------------------------------------------------------------------------------------------------*/
//需要认证部分路由
$attributes['middleware'] = ['auth:api'];

Route::group($attributes, function ($router) {
    $router->get('api-test', 'TestController@apiTest');

    $router->resource('topic', 'TopicController');
    $router->resource('comment', 'CommentController')->except(['index']);
    $router->resource('collectAndViewhistory', 'CollectAndViewhistoryController');
    $router->resource('couponCard', 'CouponCardController');
    $router->resource('user-push-message', 'UserPushMessageController');
    $router->resource('like', 'LikeController');
    $router->resource('user', 'UserController');
    $router->resource('sign-in', 'SignInController');
    $router->get('profile', 'UserController@profile')->name('user.profile');
    $router->get('user-money-water', 'UserController@moneyWaterLog');
    $router->resource('user-address', 'UserAddressController');
    $router->resource('user-finance-account', 'UserFinanceAccountController');
    $router->get('user-finance-account/type/json', 'UserFinanceAccountController@typeJson');
    $router->resource('withdrawal', 'WithdrawalLogController');

    $router->get('recruitment/{id}', 'StoreController@recruitment');
    $router->get('lucky-draw/{id}', 'LuckyDrawController@show');
    $router->post('lucky-draw/action/{id}', 'LuckyDrawController@action');
    $router->post('items-exchange-counpon', 'ItemsController@exchange_counpon');//
    $router->resource('push-message-log', 'PushMessageLogController');//
    $router->resource('tb-offical-order', 'TbOfficalOrderController');//

    $router->resource('complain', 'ComplainController');
    $router->resource('apply-job', 'ApplyJobController');

    $router->post('share/activity/{id}', 'ShareController@activity');
    $router->post('share/topic/{id}', 'ShareController@topic');
    $router->post('share/store/{id}', 'ShareController@store');

    $router->post('homepage-take-redpacket', 'HomePageController@takeRedPacket')->name('homepage-take-redpacket');

    //关注用户
    $router->post('auth-follow-user', 'AuthenticateController@followUser')->name('auth-follow-user');
    $router->get('auth-my-follows', 'AuthenticateController@myFollows')->name('auth-my-follows');
    $router->get('auth-follow-user-topic', 'AuthenticateController@followUserTopic')->name('auth-follow-user-topic');

    $router->post('auth/change-phone', 'AuthenticateController@changePhone')->name('change-phone');
    $router->post('auth/bind-qq', 'AuthenticateController@bindQQ');
    $router->post('auth/bind-weixin-open', 'AuthenticateController@bindWeixinOpen');
    //兑换记录
    $router->get('user-exchange-log', 'UserController@exchangeLog')->name('user-exchange-log');
    $router->get('user-earn-list', 'UserController@userEarnList')->name('user-earn-list');
    $router->get('user-earn-order', 'UserController@userEarnOrder')->name('user-earn-order');
    $router->get('user-cooper-partner', 'UserController@cooperPartner')->name('user-cooper-partner');
    $router->get('user-myresume', 'UserController@myResume')->name('user-myresume');
    $router->post('user-update-myresume', 'UserController@updateMyResume')->name('user-update-myresume');
});
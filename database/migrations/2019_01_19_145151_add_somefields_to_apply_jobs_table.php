<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomefieldsToApplyJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apply_jobs', function (Blueprint $table) {
            $table->tinyInteger('sex')->default(1)->comment("1:男，2:女,3:保密");
            $table->integer('age')->default(0)->comment("年龄");
            $table->string('email')->default('')->comment("邮箱");
            $table->text('works')->nullable()->comment("工作经验等");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apply_jobs', function (Blueprint $table) {
            $table->dropColumn(['sex','age','email','works']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFinanceAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_finance_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index();
            $table->string('type')->comment('类型，如alipay, icbc');
            $table->string('account')->comment('账号');
            $table->string('bind_username')->comment('账号绑定的用户真实姓名');
            $table->string('bind_organization')->nullable()->comment('账号所属机构，如某支行');
            $table->string('bind_phone')->nullable()->comment('账号绑定的手机');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_finance_accounts');
    }
}

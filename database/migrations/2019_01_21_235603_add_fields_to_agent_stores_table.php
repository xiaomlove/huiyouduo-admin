<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToAgentStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agent_stores', function (Blueprint $table) {
            $table->integer('store_bind_uid')->nullable()->comment("商品绑定前端uid");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agent_stores', function (Blueprint $table) {
            $table->dropColumn(['store_bind_uid']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCardCreateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_card_create_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('points_quota_input')->default(0)->comment("生成积分额度");
            $table->integer('create_nums_input')->default(0)->comment("生成数目");
            $table->integer('create_admin_uid')->default(0)->comment("生成操作管理员uid");
            $table->string('create_card_start')->default('0')->comment("优惠卡生成批次开始编号");
            $table->string('create_card_end')->default('0')->comment("优惠卡生成批次结束编号");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_card_create_log');
    }
}

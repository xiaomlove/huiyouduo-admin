<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastmonthcaijilogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lastmonthcaijilog', function (Blueprint $table) {
            $table->increments('id');
            $table->string("caiji_date")->default("2018-10")->comment("采集日期:精确到天");
            $table->integer("nums")->default(0)->comment("采集次数");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lastmonthcaijilog');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsEntityToLuckyDrawPrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lucky_draw_prizes', function (Blueprint $table) {
            $table->boolean('is_entity')->default(0)->comment("是否实物");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lucky_draw_prizes', function (Blueprint $table) {
            $table->dropColumn('is_entity');
        });
    }
}

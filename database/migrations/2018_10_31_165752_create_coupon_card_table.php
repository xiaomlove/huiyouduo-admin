<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_card', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coupon_card_no')->default("")->comment("优惠卡唯一编号");
            $table->string('coupon_card_pub_key')->default("")->comment("优惠卡帐号");
            $table->string('coupon_card_pri_key')->default("")->comment("优惠卡密码");
            $table->integer('first_class_agent_id')->default(0)->comment("一级代理商id");
            $table->integer('second_class_agent_id')->default(0)->comment("二级代理商id");
            $table->integer('points_quota')->default(0)->comment("积分额度");
            $table->tinyInteger('is_used')->default(0)->comment("是否使用");
            $table->integer('bind_uid')->default(0)->comment("绑定的uid");
            $table->dateTime('used_at')->nullable()->comment("使用时间");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_card');
    }
}

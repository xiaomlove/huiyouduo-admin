<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsSettleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents_settle', function (Blueprint $table) {
            $table->increments('id');
            $table->string('day')->default('')->comment('结算日期')->index();
            $table->float("settle_total_price")->default(0)->comment("结算总金额");
            $table->integer('agent_admin_uid')->default(0)->comment('代理商uid');
            $table->boolean('is_all_settled')->default(0)->comment('是否代理商结算了');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents_settle');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index();
            $table->string('target_type')->comment('评论目标类型');
            $table->integer('target_id')->comment('评论目标ID');
            $table->integer('pid')->default(0)->comment('父级ID')->index();
            $table->integer('floor_num')->default(-1)->comment('楼层号')->index();
            $table->text('content');
            $table->text('images')->nullable();
            $table->integer('like_count')->default(0);
            $table->integer('reply_count')->default(0);
            $table->integer('share_count')->default(0);
            $table->integer('view_count')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->index(['target_id', 'target_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}

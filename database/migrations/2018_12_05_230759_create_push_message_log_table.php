<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushMessageLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_message_log', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('push_type')->default(1)->comment('1:商品推送;2:活动推送;3:积分推送');
            $table->integer('push_params_id')->default(0)->comment('参数id,-1代表忽略参数')->index();
            $table->string('push_province_code',666)->default('')->comment('推送城市编码')->index();
            $table->string('push_province_name',666)->default('')->comment('推送城市')->index();
            $table->string('push_title')->default('')->comment('推送标题');
            $table->string('push_message',666)->default('')->comment('推送内容');
            $table->string('remark')->default('')->comment('推送备注');
            $table->string('push_id')->default('')->comment('推送id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_message_log');
    }
}

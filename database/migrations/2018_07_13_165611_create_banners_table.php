<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default("")->comment("图片标题");
            $table->string('images')->default("")->comment("图片地址");
            $table->tinyInteger('type')->default(1)->comment('跳转类型');
            $table->string('url')->nullable()->comment("跳转地址");
            $table->integer('params_id')->default(0)->comment("内部链接跳转参数");
            $table->tinyInteger('position')->default("0")->comment("轮播位置:默认首页位置0;");
            $table->integer('sort')->default(1)->comment("排序");

            $table->timestamps();

            $table->index('sort', 'idx_sort');
            $table->index('created_at', 'idx_created_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficalMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offical_message', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default('')->comment("首页官方消息通知");
            $table->string('params_id')->default('0')->comment("跳转商品地址");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offical_message');
    }
}

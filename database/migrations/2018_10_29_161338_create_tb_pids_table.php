<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbPidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('adzone_id')->default("");
            $table->string('site_id')->default("");
            $table->string('unid')->default("")->comment('淘宝联盟账号的ID');
            $table->string('pid')->default("推广位的ID");
            $table->integer('uid')->default(0)->comment('绑定的uid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pids');
    }
}

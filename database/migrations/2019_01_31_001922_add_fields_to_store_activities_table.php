<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToStoreActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_activities', function (Blueprint $table) {
            $table->mediumText('act_desc')->nullable()->comment("活动详情");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_activities', function (Blueprint $table) {
            $table->dropColumn(['act_desc']);
        });
    }
}

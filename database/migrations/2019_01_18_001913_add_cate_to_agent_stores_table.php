<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCateToAgentStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agent_stores', function (Blueprint $table) {
            $table->integer('cate')->default(0)->comment("店铺分类")->index();
            $table->string('zhaopin_tpl_url')->default('')->comment("招聘模板url");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agent_stores', function (Blueprint $table) {
            $table->dropColumn(['cate','zhaopin_tpl_url']);
        });
    }
}

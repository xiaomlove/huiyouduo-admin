<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecruitmentTextToStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agent_stores', function (Blueprint $table) {
            $table->text('recruitment_text')->nullable()->comment('招聘信息');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agent_stores', function (Blueprint $table) {
            $table->dropColumn('recruitment_text');
        });
    }
}

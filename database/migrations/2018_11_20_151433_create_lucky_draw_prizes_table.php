<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLuckyDrawPrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lucky_draw_prizes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lucky_draw_id')->index()->comment('抽奖ID');
            $table->integer('store_id')->default(0)->comment('代表哪个店铺提供');
            $table->string('title');
            $table->string('cover')->nullable()->comment('图片');
            $table->text('description')->nullable()->comment('描述信息');
            $table->integer('possibility')->default(0)->comment('中奖概率, 具体占比 = 该值/所有奖品该值之和');
            $table->integer('award_one_day')->default(1)->comment('每天中奖数限制');
            $table->integer('award_total')->default(1)->comment('总的中奖数，即库存');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lucky_draw_prizes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMoneyWaterLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_money_water_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index();
            $table->string('source_type')->comment('来源类型，如订单，如提现等');
            $table->integer('source_id')->comment('来源ID，如订单ID，如提现记录ID');
            $table->integer('money')->comment('钱数，单位：分');
            $table->integer('total')->nullable()->comment('当前总钱数');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_money_water_logs');
    }
}

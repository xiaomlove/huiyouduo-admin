<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsVerifyTakePrizeToLuckyDrawWinLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lucky_draw_win_logs', function (Blueprint $table) {
            $table->boolean('is_verify_take_prize')->default(0)->comment("是否实物核销");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lucky_draw_win_logs', function (Blueprint $table) {
            $table->dropColumn('is_verify_take_prize');
        });
    }
}

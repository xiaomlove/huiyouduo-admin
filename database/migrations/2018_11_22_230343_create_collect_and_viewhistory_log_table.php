<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectAndViewhistoryLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collect_and_viewhistory_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index();
            $table->string('item_id')->default('0')->comment('对象id');
            $table->tinyInteger('type')->default(1)->comment('1:收藏;2:浏览');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collect_and_viewhistory_log');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawalLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index();
            $table->tinyInteger('channel')->comment('申请渠道，1用户前台，2代理商后台');
            $table->integer('finance_account_id')->comment('提现到哪个账号ID');
            $table->integer('money')->comment('提现金额，单位分');
            $table->tinyInteger('status')->default(0)->comment('状态，0未处理，1处理中, 2已完成，3失败');
            $table->text('trade_snapshot')->nullable()->comment('交易快照');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawal_logs');
    }
}

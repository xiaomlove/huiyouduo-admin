<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStatisticalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statistical', function (Blueprint $table) {
            $table->increments('id');
            $table->date('day')->nullable()->comment('订单日期')->index();
            $table->integer("count")->default(0)->comment("订单数");
            $table->float("pay_price_count")->default(0)->comment("订单总金额");
            $table->float("commission_count")->default(0)->comment("预估订单总佣金");
            $table->integer("agent_admin_uid")->default(0)->comment("代理商管理员id")->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_statistical');
    }
}

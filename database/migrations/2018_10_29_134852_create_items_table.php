<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cid')->default(0)->comment("本地分类ID");
            $table->integer('cate_leaf_id')->default(0)->comment("本地二级分类id");
            $table->bigInteger('category_id')->default(0)->comment("类目ID");
            $table->string('category_name')->default("")->comment("类目名称");
            $table->integer('commission_rate')->default(0)->comment("佣金");
            $table->dateTime('coupon_end_time')->nullable()->comment("优惠券结束时间");
            $table->string('coupon_id')->default("")->comment("优惠券id");
            $table->string('coupon_info')->default("")->comment("优惠券信息");
            $table->float('coupon_price')->default(0)->comment("优惠券金额");
            $table->float('coupon_after_price')->default(0)->comment(" 券后价");
            $table->integer('coupon_remain_count')->default(0)->comment("优惠券余量");
            $table->dateTime('coupon_start_time')->nullable()->comment("优惠券开始时间");
            $table->integer('coupon_total_count')->default(0)->comment("优惠券总量");
            $table->string('item_url')->default("")->comment("产品地址");
            $table->bigInteger('level_one_category_id')->default(0)->comment("一级栏目id");
            $table->string('level_one_category_name')->default("")->comment("一级栏目名称");
            $table->bigInteger('num_iid')->default(0)->comment("产品ID");
            $table->string('pict_url')->default("")->comment("产品主图");
            $table->string('provcity')->default("")->comment("卖家地址");
            $table->integer('reserve_price')->default(0)->comment("原价");
            $table->bigInteger('seller_id')->default(0)->comment(" 卖家ID");
            $table->bigInteger('shop_dsr')->default(0)->comment(" 店铺DSR评分");
            $table->string('shop_title')->default("")->comment("卖家昵称");
            $table->string('short_title')->default("")->comment("短标题");
            $table->text('small_images')->nullable()->comment("其它主图");
            $table->mediumText('detail_images')->nullable()->comment("商品详情图片");
            $table->string('title')->default("")->comment(" 产品标题");
            $table->float('tk_total_commi')->default(0)->comment("月支出佣金");
            $table->integer('tk_total_sales')->default(0)->comment("淘客30天月推广量");
            $table->tinyInteger('user_type')->default(0)->comment("卖家类型，0表示淘宝产品，1表示天猫商城");
            $table->integer('volume')->default(0)->comment("  30天销量");
            $table->string('white_image')->default("")->comment("商品短标题");
            $table->float('zk_final_price')->default(0)->comment(" 商品折后价");
            $table->string('quan_link',666)->default("")->comment("优惠券链接");

            $table->timestamps();
            $table->index('cid', 'idx_cid');
            $table->index('cate_leaf_id', 'idx_cate_leaf_id');
            $table->index('coupon_price', 'idx_coupon_price');
            $table->index('commission_rate', 'idx_commission_rate');
            $table->index('coupon_after_price', 'idx_coupon_after_price');
            $table->index('volume', 'idx_volume');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}

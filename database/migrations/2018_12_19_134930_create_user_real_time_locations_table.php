<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRealTimeLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_real_time_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index();
            $table->string('ip')->nullable();
            $table->float('longitude', 11, 8)->nullable();
            $table->float('latitude', 11, 8)->nullable();
            $table->string('province_code')->nullable();
            $table->string('province_name')->nullable();
            $table->string('city_code')->nullable();
            $table->string('city_name')->nullable();
            $table->string('district_code')->nullable();
            $table->string('district_name')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_real_time_locations');
    }
}

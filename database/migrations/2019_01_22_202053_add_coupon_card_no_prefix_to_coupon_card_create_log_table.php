<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCouponCardNoPrefixToCouponCardCreateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupon_card_create_log', function (Blueprint $table) {
            $table->string('name')->default('')->comment("批次卡名称");
            $table->string('coupon_card_no_prefix')->default('000')->comment("批次卡前三位自定义");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupon_card_create_log', function (Blueprint $table) {
            $table->dropColumn(['name','coupon_card_no_prefix']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushMessageStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_message_state', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('message_id')->default(0)->comment('消息id')->index();
            $table->tinyInteger('is_read')->default(0)->comment('0:未读;1:已读');
            $table->integer('uid')->default(0)->comment('uid')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_message_state');
    }
}

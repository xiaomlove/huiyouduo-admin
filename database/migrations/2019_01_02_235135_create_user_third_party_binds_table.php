<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserThirdPartyBindsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_third_party_binds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->index();
            $table->string('platform');
            $table->string('openid')->index();
            $table->string('unionid')->nullable()->index();
            $table->string('nickname')->nullable()->index();
            $table->json('info')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_third_party_binds');
    }
}

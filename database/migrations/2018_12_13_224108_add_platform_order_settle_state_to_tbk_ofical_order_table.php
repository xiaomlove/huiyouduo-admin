<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlatformOrderSettleStateToTbkOficalOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbk_ofical_order', function (Blueprint $table) {
            $table->tinyInteger('is_settled')->default(0)->comment("结算状态 0-未结算,1-结算了");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbk_ofical_order', function (Blueprint $table) {
            $table->dropColumn('is_settled');
        });
    }
}

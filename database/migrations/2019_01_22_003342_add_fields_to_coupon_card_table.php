<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCouponCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupon_card', function (Blueprint $table) {
            $table->integer('bind_store_id')->default(0)->comment("绑定的店铺");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupon_card', function (Blueprint $table) {
            $table->dropColumn(['bind_store_id']);
        });
    }
}

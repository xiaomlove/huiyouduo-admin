<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbkOficalOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbk_ofical_order', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('adzone_id')->default(0);
            $table->char('adzone_name',10)->default("");
            $table->float('alipay_total_price')->default(0);
            $table->string('auction_category',15)->default("");
            $table->dateTime('click_time')->nullable();
            $table->float('commission')->default(0);
            $table->float('commission_rate')->default(0);
            $table->dateTime('create_time')->nullable();
            $table->float('income_rate')->default(0);
            $table->integer('item_num')->default(0);
            $table->string('item_title')->default("");
            $table->bigInteger('num_iid')->default(0);
            $table->char('order_type',10)->default("");
            $table->float('pay_price')->default(0);
            $table->float('price')->default(0);
            $table->float('pub_share_pre_fee')->default(0);
            $table->string('seller_nick')->default("");
            $table->string('seller_shop_title')->default("");
            $table->bigInteger('site_id')->default(0);
            $table->string('site_name')->default("");
            $table->float('subsidy_fee')->nullable();
            $table->float('subsidy_rate')->default(0);
            $table->tinyInteger('subsidy_type')->default(0);
            $table->tinyInteger('terminal_type')->default(0);
            $table->char('tk3rd_type',5)->default("");
            $table->tinyInteger('tk_status')->default(0);
            $table->float('total_commission_fee')->nullable();
            $table->float('total_commission_rate')->default(0);
            $table->string('trade_id')->default("");
            $table->string('trade_parent_id')->default("");
            $table->dateTime('earning_time')->nullable();


            $table->index('adzone_id', 'idx_adzone_id');
            $table->index('num_iid', 'idx_num_iid');
            $table->index('site_id', 'idx_site_id');
            $table->index('trade_id', 'idx_trade_id');
            $table->index('trade_parent_id', 'idx_trade_parent_id');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbk_ofical_order');
    }
}

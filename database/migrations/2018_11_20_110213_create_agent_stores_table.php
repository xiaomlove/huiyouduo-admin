<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_stores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id')->index();
            $table->string('name');
            $table->string('province_code')->nullable();
            $table->string('city_code')->nullable()->index();
            $table->string('district_code')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('cover')->nullable();
            $table->text('images')->nullable();
            $table->float('longitude', 10, 7)->nullable();
            $table->float('latitude', 10, 7)->nullable();
            $table->string('promotion_text')->nullable()->comment('促销文案');
            $table->string('remarks')->nullable()->comment('备注');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_stores');
    }
}

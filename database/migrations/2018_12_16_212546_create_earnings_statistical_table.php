<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEarningsStatisticalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earnings_statistical', function (Blueprint $table) {
            $table->increments('id');
            $table->date('day')->nullable()->comment('日期')->index();
            $table->float("income")->default(0)->comment("今日收入");
            $table->float("spending")->default(0)->comment("今日支出");
            $table->string('remark')->default('')->comment('备注');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('earnings_statistical');
    }
}

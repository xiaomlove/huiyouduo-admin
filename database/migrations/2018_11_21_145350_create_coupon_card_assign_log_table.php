<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCardAssignLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_card_assign_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_no')->default('0')->comment("优惠卡编号");
            $table->integer('assign_admin_uid')->default(0)->comment("分配这个卡的管理员");
            $table->dateTime('assign_admin_at')->nullable()->comment("分配这个卡的时间");
            $table->string('remark')->default('')->comment("备注");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_card_assign_log');
    }
}

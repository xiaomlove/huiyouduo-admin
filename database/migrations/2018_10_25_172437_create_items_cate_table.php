<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsCateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_cate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default("")->comment("分类标题");
            $table->string('cate_img',500)->default("")->comment("icon");
            $table->integer('parent_id')->default(0)->comment("父级id");
            $table->integer('order')->default(0)->comment("排序");
            $table->tinyInteger('is_recommend')->default(0)->comment('1：推荐；0不推荐');
            $table->timestamps();
            $table->softDeletes();

            $table->index('parent_id', 'idx_parent_id');
            $table->index('order', 'idx_order');
            $table->index('created_at', 'idx_created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_cate');
    }
}

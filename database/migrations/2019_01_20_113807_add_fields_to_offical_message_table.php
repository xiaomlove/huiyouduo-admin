<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOfficalMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offical_message', function (Blueprint $table) {
            $table->string('image')->default('')->comment("背景图片");
            $table->text('desctext')->nullable()->comment("描述");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offical_message', function (Blueprint $table) {
            $table->dropColumn(['image','desctext']);
        });
    }
}

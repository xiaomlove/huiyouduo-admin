<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOutBizNoToWithdrawalLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdrawal_logs', function (Blueprint $table) {
            $table->string('out_biz_no')->nullable()->index()->comment('商户业务号');
            $table->string('alipay_trade_no')->nullable()->index()->comment('支付宝交易凭证号');
            $table->dateTime('paid_at')->nullable()->comment('支付时间');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdrawal_logs', function (Blueprint $table) {
            $table->dropColumn('out_biz_no');
            $table->dropColumn('alipay_trade_no');
            $table->dropColumn('paid_at');
        });
    }
}

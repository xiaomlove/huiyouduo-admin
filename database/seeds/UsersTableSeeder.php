<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Admin;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(User::class)->times(10)->create();
        //为后台账号绑定前端用户
        $admins = Admin::all();
        $frontUsers = $users->random($admins->count());
        $admins->each(function ($admin) use (&$frontUsers) {
            $admin->frontUser()->associate($frontUsers->pop())->save();
        });
    }
}

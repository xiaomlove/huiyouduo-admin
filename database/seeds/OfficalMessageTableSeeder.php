<?php

use Illuminate\Database\Seeder;

class OfficalMessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\OfficalMessage::create([
            'title'=>'我是一条测试官方通知消息',
            'params_id'=>0
        ]);
    }
}

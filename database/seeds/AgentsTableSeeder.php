<?php

use Illuminate\Database\Seeder;
use App\Models\Agent;
use App\Models\Admin;
use App\Models\District;
use App\Models\AgentStore;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //全部在默认城市有一个店
        $city = District::where('code', config('lbs.default_city_code'))->first();
        $districts = $city->districts;
        $province = $city->province;
        Admin::whereIn('username',['agent-1','agent-2'])->each(function ($admin) use ($province, $city, $districts) {
            $agent = $admin->agent()->create([
                'name' => "代理商" . $admin->name,
                'level' => $admin->username == 'agent-1' ? 1 : 2,
            ]);
            $store = factory(AgentStore::class)->make();
            $store->city_code = $city->code;
            $store->province_code = $province->code;
            $store->district_code = $districts->random()->code;
            $agent->stores()->create($store->toArray());
        });

    }

    private function getDistrict($provinces)
    {
        $province = $provinces->random();
        $city = $province->cities()->get();
        if ($city->isEmpty())
        {
            echo "can't get city of {$province->name}, again \n";
            return $this->getDistrict($provinces);
        }
        $city = $city->random();

        $district = $city->districts()->get();
        if ($district->isEmpty())
        {
            echo "can't get district of {$city->name}, again \n";
            return $this->getDistrict($provinces);
        }
        $district = $district->random();

        echo sprintf("%s-%s-%s \n", $province->name, $city->name, $district->name);
        return [
            'province_code' => $province->code,
            'city_code' => $city->code,
            'district_code' => $district->code,
        ];
    }
}

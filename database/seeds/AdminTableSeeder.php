<?php

use Illuminate\Database\Seeder;
use Laravel\Passport\Client;
use Carbon\Carbon;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Menu;
use App\Models\Admin;


class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Artisan::call('admin:install');//执行迁移，生成默认管理员，自带的菜单

        //添加权限
        $this->createPermission();

        //添加默认角色
        $this->createRole();

        //添加默认菜单
        $this->createMenu();

        //为超级管理员添加全部角色
//        Admin::first()->roles()->saveMany(Role::where('slug', '!=', 'administrator')->get());

        //添加后台账号
        $this->createAdministrator();

        //添加 OAUTH_CLIENT_ID
        if (Client::count() == 0)
        {
            \Artisan::call("passport:install");
        }
    }

    private function createPermission()
    {
        foreach (Permission::$defaults as $slug => $value)
        {
            $httpBasePath = sprintf("%s", trim($value['http_path'], '/'));
            $httpPath = sprintf("/%s", $httpBasePath);
            Permission::create([
                'slug' => $slug,
                'name' => $value['name'],
                'http_path' => $httpPath,
            ]);
        }
    }

    private function createRole()
    {
        foreach (Role::$defaults as $slug => $value)
        {
            $role = Role::create([
                'name' => $value['name'],
                'slug' => $slug,
            ]);
            if (!empty($value['permissions']))
            {
                $permissions = Permission::whereIn("slug", $value['permissions'])->get();
                $role->permissions()->saveMany($permissions);
            }
            //基本权限
            if ($slug == Role::SLUG_AGENT)
            {
                $basePermissions = Permission::WhereIn("slug", ['auth.login', 'auth.setting'])->get();
                $role->permissions()->saveMany($basePermissions);
            }
            if ($slug == Role::SLUG_OFFICIAL)
            {
                $basePermissions = Permission::WhereIn("slug", ['dashboard', 'auth.login', 'auth.setting', 'auth.management'])->get();
                $role->permissions()->saveMany($basePermissions);
            }
        }


    }

    private function createMenu()
    {
        $order = Menu::max('order');
        foreach (Menu::$defaults as $item)
        {
            $roles = $item['roles'] ?? [Role::SLUG_OFFICIAL];
            $menu = Menu::create([
                'title' => $item['title'],
                'icon' => $item['icon'],
                'uri' => trim($item['uri'], '/'),
                'order' => $order,
            ]);
            $rolesToBind = Role::whereIn("slug", $roles)->get();
            $menu->roles()->saveMany($rolesToBind);
            $order++;
        }
        //把系统放到最下边
        $adminMenu = Menu::find(2);
        if ($adminMenu)
        {
            $adminMenu->order = $order;
            $adminMenu->save();
        }
    }

    private function createAdministrator()
    {
        foreach (Role::$defaults as $slug => $value)
        {
            //每种角色的创建一个
            $admin = Admin::create([
                'username' => $slug,
                'password' => bcrypt($slug),
                'name' => $value['name'],
            ]);
            $admin->roles()->save(Role::where("slug", $slug)->first());
            if (Role::isInAgent($slug) && $slug != Role::SLUG_AGENT)
            {
                $admin->roles()->save(Role::where("slug", Role::SLUG_AGENT)->first());
            }
        }

    }

}

<?php

use Illuminate\Database\Seeder;
use App\Models\PushMessageLog;

class PushMessageLogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PushMessageLog::class)->times(30)->make()->each(function ($mesage)  {
            $mesage->save();
        });
    }
}

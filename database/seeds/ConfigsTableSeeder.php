<?php

use Illuminate\Database\Seeder;
use App\Models\Config;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Config::$defaults as $name => $value) {
            Config::create([
                'name' => $name,
                'name_human' => $value['name'],
                'value' => $value['value'],
            ]);
        }
    }
}

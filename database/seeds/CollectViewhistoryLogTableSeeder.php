<?php

use Illuminate\Database\Seeder;

use App\Models\CollectAndViewhistory;

class CollectViewhistoryLogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CollectAndViewhistory::class)->times(30)->make()->each(function ($model)  {
            $model->save();
        });
    }
}

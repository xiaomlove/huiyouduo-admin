<?php

use Illuminate\Database\Seeder;

class LuckyDrawsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $luckyDraw = factory(\App\Models\LuckyDraw::class)->create();
        $prizeCount = random_int(4, 10);
        $stores = \App\Models\AgentStore::all();
        for ($i = 0; $i < $prizeCount; $i++) {
            $luckyDrawPrize = factory(\App\Models\LuckyDrawPrize::class)->make();
            if (random_int(1, 5) == 1) {
                $luckyDrawPrize->store_id = $stores->random()->id;
            }
            $luckyDraw->prizes()->create($luckyDrawPrize->toArray());
        }
    }
}

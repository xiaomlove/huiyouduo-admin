<?php

use Faker\Generator as Faker;

$factory->define(App\Models\LuckyDraw::class, function (Faker $faker) {
    return [
        'title' => '抽奖' . $faker->sentence(),
        'description' => $faker->paragraph(),
        'cover' => $faker->imageUrl(),
        'consume_score' => $faker->numberBetween(1, 20),
        'begin_time' => \Carbon\Carbon::now(),
        'end_time' => \Carbon\Carbon::now()->addDay(10),
    ];
});

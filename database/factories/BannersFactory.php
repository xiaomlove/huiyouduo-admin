<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Banner::class, function (Faker $faker) {
    return [
        'images' => 'Cg-4V1D84B-IP2nMAAMDOXGgTCgAAEliwGiRqgAAwNR348.jpg',
        'title' => $faker->title,
        'type'=>2,
        'url'=>"http://www.biadu.com",
        'position'=>0,
    ];
});

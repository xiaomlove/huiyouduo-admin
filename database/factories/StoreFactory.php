<?php

use Faker\Generator as Faker;

$factory->define(App\Models\AgentStore::class, function (Faker $faker) {
    return [
        'name' => $faker->text('20') . '店',
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'cover' => $faker->imageUrl(),
        'images' => [$faker->imageUrl(), $faker->imageUrl(), $faker->imageUrl()],
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'promotion_text' => '满100减20',
        'recruitment_text' => $faker->paragraph,
    ];
});

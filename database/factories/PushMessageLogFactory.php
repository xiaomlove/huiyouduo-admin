<?php

use Faker\Generator as Faker;

$factory->define(App\Models\PushMessageLog::class, function (Faker $faker) {
    return [
        'push_type' => mt_rand(1,2),
        'push_params_id' => mt_rand(11111,99999),
        'push_title'=>$faker->title,
        'push_message'=>$faker->title,
    ];
});

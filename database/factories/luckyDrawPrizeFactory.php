<?php

use Faker\Generator as Faker;

$factory->define(App\Models\LuckyDrawPrize::class, function (Faker $faker) {
    return [
        'title' => '奖品' . $faker->sentence(),
        'description' => $faker->paragraph(),
        'cover' => $faker->imageUrl(),
        'possibility' => $faker->numberBetween(10, 50),
        'award_one_day' => 2,
        'award_total' => 10,
    ];
});

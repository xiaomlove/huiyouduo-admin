<?php

/**
 * 文档：https://github.com/overtrue/easy-sms
 */

return [
    // HTTP 请求的超时时间（秒）
    'timeout' => 5.0,

    'template_id_login' => env('ALIYUN_SMS_TEMPLATE_ID_LOGIN'),//模板ID
    'template_id_register' => env('ALIYUN_SMS_TEMPLATE_ID_REGISTER'),//模板ID
    'template_id_change_password' => env('ALIYUN_SMS_TEMPLATE_ID_CHANGE_PASSWORD'),//模板ID
    'template_id_score_send' => env('ALIYUN_SMS_TEMPLATE_ID_SCORE_SEND'),//模板ID


    'lifetime' => env('SMS_VERIFY_CODE_LIFETIME', 300),//有效时间, 单位：秒

    // 默认发送配置
    'default' => [
        // 网关调用策略，默认：顺序调用
        'strategy' => \Overtrue\EasySms\Strategies\OrderStrategy::class,

        // 默认可用的发送网关
        'gateways' => [
            'aliyun',
        ],
    ],
    // 可用的网关配置
    'gateways' => [
        'errorlog' => [
            'file' => storage_path('sms.log'),
        ],
        'aliyun' => [
            'access_key_id' => env('ALIYUN_ACCESS_KEY_ID'),
            'access_key_secret' => env('ALIYUN_ACCESS_KEY_SECRET'),
            'sign_name' => '惠友多',
        ],
        //...
    ],
];
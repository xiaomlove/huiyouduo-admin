<?php

return [
    'baidu_ak' => env('BAIDU_LBS_AK'),
    'baidu_sk' => env('BAIDU_LBS_SK'),
    'default_city_code' => env('LBS_DEFAULT_CODE', 441300),//默认城市，惠州
];
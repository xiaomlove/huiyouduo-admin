<?php

/**
 * http://wsd.591hufu.com 维易淘客接口API
 */

return [
//    'tb_official_default_pid' => env('TB_OFFICIAL_DEFAULT_PID'),
//    'vetbk_key' => env('VETBK_KEY'),
    'super_base_api'=>env('VETBK_SUPER_API_URL'),
    'vetbk_products_api_url'=>env('VETBK_PRODUCTS_API_URL'),
    'vetbk_hcapi_api_url'=>env('VETBK_HCAPI_API_URL'),
    'vetbk_pintuan_api_url'=>env('VETBK_PINTUAN_API_URL'),
    'vetbk_key'=>env('VETBK_KEY'),
    'tb_official_default_pid'=>env('TB_OFFICIAL_DEFAULT_PID'),
    'items_crawlered_collections_redis_key' => env('ITEMS_CRAWLERED_COLLECTIONS_REDIS_KEY'),
    'super_search_goods_by_cate_name_api_url' => "http://apis.vephp.com/super?vekey=".env('VETBK_KEY')."&para=%s&pid=".env('TB_OFFICIAL_DEFAULT_PID')."&page=%d&coupon=1&pagesize=100",//每页最大只能100
];
<?php

return [
    'region_id' => env('ALIYUN_STS_REGION_ID'),

    'endpoint' => env('ALIYUN_STS_ENDPOINT'),

    'access_key_id' => env('ALIYUN_STS_ACCESS_KEY_ID'),

    'access_key_secret' => env('ALIYUN_STS_ACCESS_KEY_SECRET'),

    'role_arn' => env('ALIYUN_STS_ROLE_ARN'),
];
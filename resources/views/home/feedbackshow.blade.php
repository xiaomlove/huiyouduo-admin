<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <title>常见问题</title>
        <style type="text/css">
            *,
            *:before,
            *:after {
                margin: 0;
                padding: 0;
                /*防止内容溢出 不出现滚动条 移动端常用布局是非固定像素*/
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                /*轻击 轻触 高亮效果清除*/
                tap-highlight-color: transparent;
                -webkit-tap-highlight-color: transparent;

            }

            /*当样式表里font-size<12px时，中文版chrome浏览器里字体显示仍为12px，这时可以用 */
            html {
                -webkit-text-size-adjust: none;
            }

            /*-webkit-text-size-adjust放在body上会导致页面缩放失效*/

            input,
            textarea {
                boder: none;
                outline: none;
                /*禁止缩放textarea*/
                resize: none;
                /*元素的外观*/
                -webkit-appearance: none;
            }

            img {
                border: none;
                vertical-align: middle;
                -ms-interpolation-mode: bicubic;
                width: 100%;
            }

            input[type="submit"],
            input[type="reset"],
            input[type="button"],
            button {
                -webkit-appearance: none;
            }

            /*去掉苹果手机按钮圆角样式*/
            input[type="button"],
            input[type="submit"],
            input[type="reset"] {
                -webkit-appearance: none;
            }

            textarea {
                -webkit-appearance: none;
            }

            /*如果还有圆角的问题，
            .button{ border-radius: 0; } */
            /*去除Chrome等浏览器文本框默认发光边框*/
            input:focus,
            textarea:focus {
                outline: none;
            }

            /*去掉高光样式*/
            input:focus {
                -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
                -webkit-user-modify: read-write-plaintext-only;
            }

            /*去除IE10+浏览器文本框后面的小叉叉*/
            input::-ms-clear {
                display: none;
            }

            html,
            body {
                font-family: "Source Han Sans CN";
            }

            #app_hyd {
                width: 100%;

            }

            .topbj {
                width: 100%;
                height: 44px;
                background: linear-gradient(to right, #EE8432, #F91E62);
                position: relative;
            }

            .topbj img {
                width: 17px;
                height: 14px;
                position: absolute;
                left: 14px;
                top: 16px;
            }

            .topbj p {
                width: 100%;
                text-align: center;
                color: #FFFFFF;
                font-size: 17px;
                line-height: 44px;
            }

            .content {
                padding: 15px;
            }

            .content p {
                color: #444444;
                font-size: 15px;
                line-height: 26px;
                text-indent: 2em;
            }

            .content h1 {
                font-size: 17px;
                color: #222222;
                font-weight: bold;
                text-align: center;
                line-height: 17px;
                padding-bottom: 15px;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
            }
        </style>
        <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    </head>
    <body style="background: #F5F5F5;">
        <div id="app_hyd">
            {{--<div class="topbj">--}}
                {{--<a href=""><img src="{{asset('img/back.png')}}" alt=""></a>--}}
                {{--<p>常见问题</p>--}}
            {{--</div>--}}
            <div class="content" id="problem-con">
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                // 获得get参数
                function getQueryVariable(variable) {
                    var query = window.location.search.substring(1);
                    var vars = query.split("&");
                    for (var i = 0; i < vars.length; i++) {
                        var pair = vars[i].split("=");
                        if (pair[0] == variable) {
                            return pair[1];
                        }
                    }
                    return false;
                }
                var id = getQueryVariable('id');
                console.log(id);

                $.ajax({
                    type: "GET",
                    url: "http://veykoo.com:7777/api/user-feed-back/" + id,
                    dataType: "json",
                    data: {},
                    success: function(res) {
                        var data = res.data.detail;
                        var div = document.getElementById("problem-con");
                        var html = "<h1>" + data.title + "</h1><p>" + data.contents + "</p>";
                        $('#problem-con').html(html);
                    },
                    error: function(res) {
                        console.log('请求数据失败');
                    },
                });

            })
        </script>
    </body>
</html>

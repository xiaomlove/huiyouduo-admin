<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <title>扫码领券</title>
    <style type="text/css">
        *,
        *:before,
        *:after {
            margin: 0;
            padding: 0;
            /*防止内容溢出 不出现滚动条 移动端常用布局是非固定像素*/
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            /*轻击 轻触 高亮效果清除*/
            tap-highlight-color: transparent;
            -webkit-tap-highlight-color: transparent;

        }

        /*当样式表里font-size<12px时，中文版chrome浏览器里字体显示仍为12px，这时可以用 */
        html {
            -webkit-text-size-adjust: none;
        }

        /*-webkit-text-size-adjust放在body上会导致页面缩放失效*/

        input,
        textarea {
            boder: none;
            outline: none;
            /*禁止缩放textarea*/
            resize: none;
            /*元素的外观*/
            -webkit-appearance: none;
        }

        img {
            border: none;
            vertical-align: middle;
            -ms-interpolation-mode: bicubic;
            width: 100%;
        }

        input[type="submit"],
        input[type="reset"],
        input[type="button"],
        button {
            -webkit-appearance: none;
        }

        /*去掉苹果手机按钮圆角样式*/
        input[type="button"],
        input[type="submit"],
        input[type="reset"] {
            -webkit-appearance: none;
        }

        textarea {
            -webkit-appearance: none;
        }

        /*如果还有圆角的问题，
        .button{ border-radius: 0; } */
        /*去除Chrome等浏览器文本框默认发光边框*/
        input:focus,
        textarea:focus {
            outline: none;
        }

        /*去掉高光样式*/
        input:focus {
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            -webkit-user-modify: read-write-plaintext-only;
        }

        /*去除IE10+浏览器文本框后面的小叉叉*/
        input::-ms-clear {
            display: none;
        }

        html,
        body {
            font-family: "Source Han Sans CN";
        }

        #app {
            width: 100%;

        }

        #app div {
            width: 100%;
            position: relative;
            overflow: hidden;
        }

        #app div img {
            width: 100%;
            height: auto;
            display: block;
            float: left;
            vertical-align: middle;
            border: none;
        }

        #app form {
            width: 100%;
            display: inline-block;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);

        }

        #app label {
            width: 100%;
            display: block;
            overflow: hidden;
        }

        #app input {
            outline: none;
            display: block;
            margin: 0 auto;
            border-radius: 23px;
            width: 80%;
            height: 40px;
            line-height: 40px;
            font-family: "Source Han Sans CN";
            font-size: 15px;
            background: none;
            outline: none;
            border: 0px;

        }

        #app #tel {
            background: #FFFFFF;
            padding: 0px 15px;
            text-align: left;
            color: #000;
        }

        #app .btn {
            color: #fff;
            background: linear-gradient(to right, #FFD64E, #FF6000);
            margin-top: 18px;
            text-align: center;
        }
    </style>
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
</head>
<body>
<div id="app">
    <div><img src="{{asset('img/bj1_01.png')}}" id="topImage" /></div>
    <div>
        <img src="{{asset('img/bj1_02.png')}}" />
        <form>
            <label><input type="text" name="tel" id="tel" value="" placeholder="请输入手机号，领取优惠券" /></label>
            <label><input type="submit" class="btn" value="点击领取购物优惠" /></label>
        </form>
    </div>
</div>
<script type="text/javascript">
    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return false;
    }
    $("form").submit(function(e) {
        var tel = $("#tel").val();
        var code = getQueryVariable("code");
        if (tel == "") {
            alert("手机号码不能为空！");
            $("#tel").focus();
            return false;
        }
        if (!(/^1[34578]\d{9}$/.test(tel))) {
            alert("手机号码有误，请重填");
            $("#tel").focus();
            return false;
        }


        if (code==false) {
            alert("code=null");
            return false;
        }
        console.log(code);

        console.log("开始请求");
        $.ajax({
            type: "POST",
            url: "http://veykoo.com:7777/api/auth/register-qrcode",
            dataType: "json",
            data: {
                phone: tel,
                code: code
            },
            success: function(e) {

                if(e.ret == 0)
                {
                    window.location.href = "{{url('auth/register-qrcode-success')}}?phone="+tel;
                }
                else
                {
                    alert(e.msg);
                }
                console.log("请求成功");
                return false;
            },
            error: function(e) {

                console.log("请求失败");
                return false;
            },
        });
        return false;

    });
</script>
</body>
</html>

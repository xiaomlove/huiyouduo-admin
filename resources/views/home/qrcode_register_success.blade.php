<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title>领券成功</title>
    <style type="text/css">
        *{margin: 0;padding: 0;}
        a{text-decoration: none;outline: none;}

        img{border:none;vertical-align: middle;-ms-interpolation-mode: bicubic;width: 100%;}

        p{word-wrap: break-word;/*允许长单词换行到下一行*/}

        input[type="submit"],input[type="reset"],input[type="button"],button {-webkit-appearance: none;}
        /*去掉苹果手机按钮圆角样式*/
        input[type="button"], input[type="submit"], input[type="reset"] {-webkit-appearance: none;}
        textarea {  -webkit-appearance: none;}
        /*如果还有圆角的问题，
        .button{ border-radius: 0; } */
        /*去除Chrome等浏览器文本框默认发光边框*/
        input:focus, textarea:focus {outline: none;}
        /*去掉高光样式*/
        input:focus{
            -webkit-tap-highlight-color:rgba(0,0,0,0);
            -webkit-user-modify:read-write-plaintext-only;}
        /*去除IE10+浏览器文本框后面的小叉叉*/
        input::-ms-clear {display: none;}
        html,body{
            font-family: "Source Han Sans CN";
        }
        #app{
            width: 100%;
            height: auto;
            overflow: hidden;
        }
        #app div{
            width: 100%;
            overflow: hidden;
            float: left;
            position: relative;
            border: none;
        }
        #app img{
            width: 100%;
            height: auto;
            display: block;
            float: left;
            vertical-align: middle;
            border: none;
        }
        #app .cashCoupon p.pire{
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%,-50%);
            font-weight: bold;
            font-size: 79px;
            color: #E12111;
        }
        #app .title p.title{
            position: absolute;
            left: 0;
            top: 50%;
            transform: translate(0,-50%);
            width: 100%;
            font-size: 15px;
            color: #FFFFFF;
            text-align: center;
            margin-top: 15px;

        }#app .down .btn{
             position: absolute;
             left: 50%;
             top: 50%;
             transform: translate(-50%,-50%);
             font-size: 20px;
             color: #FFFFFF;
             font-weight: bold;

         }

    </style>
</head>
<body>
<div id="app">
    <div><img src="{{asset('img/bj2_01.png')}}" ></div>
    <div class="cashCoupon">
        <img src="{{asset('img/bj2_02.png')}}" >
        <p class="pire">￥{{$qr_register_score}}</p>
    </div>
    <div class="title">
        <img src="{{asset('img/bj2_03.png')}}" >
        <p class="title">{{$qr_register_score}}元现金券已经放置在您的账户 <span id="phone">{{request('phone')}}</span> 中</p>
    </div>
    <div class="down">
        <img src="{{asset('img/bj2_04.png')}}" >
        <span class="btn">下载APP</span>
    </div>

    <div><img src="{{asset('img/bj2_05.png')}}" ></div>



</div>
</body>
</html>

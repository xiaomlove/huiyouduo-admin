<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <title>帮助与反馈</title>
        <style type="text/css">
            *,
            *:before,
            *:after {
                margin: 0;
                padding: 0;
                /*防止内容溢出 不出现滚动条 移动端常用布局是非固定像素*/
                box-sizing: border-box;
                -webkit-box-sizing: border-box;
                /*轻击 轻触 高亮效果清除*/
                tap-highlight-color: transparent;
                -webkit-tap-highlight-color: transparent;

            }

            /*当样式表里font-size<12px时，中文版chrome浏览器里字体显示仍为12px，这时可以用 */
            html {
                -webkit-text-size-adjust: none;
            }

            /*-webkit-text-size-adjust放在body上会导致页面缩放失效*/

            input,
            textarea {
                boder: none;
                outline: none;
                /*禁止缩放textarea*/
                resize: none;
                /*元素的外观*/
                -webkit-appearance: none;
            }

            img {
                border: none;
                vertical-align: middle;
                -ms-interpolation-mode: bicubic;
                width: 100%;
            }

            input[type="submit"],
            input[type="reset"],
            input[type="button"],
            button {
                -webkit-appearance: none;
            }

            /*去掉苹果手机按钮圆角样式*/
            input[type="button"],
            input[type="submit"],
            input[type="reset"] {
                -webkit-appearance: none;
            }

            textarea {
                -webkit-appearance: none;
            }

            /*如果还有圆角的问题，
            .button{ border-radius: 0; } */
            /*去除Chrome等浏览器文本框默认发光边框*/
            input:focus,
            textarea:focus {
                outline: none;
            }

            /*去掉高光样式*/
            input:focus {
                -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
                -webkit-user-modify: read-write-plaintext-only;
            }

            /*去除IE10+浏览器文本框后面的小叉叉*/
            input::-ms-clear {
                display: none;
            }

            html,
            body {
                font-family: "Source Han Sans CN";
            }

            #app_hyd {
                width: 100%;

            }

            .topbj {
                width: 100%;
                height: 44px;
                background: linear-gradient(to right, #EE8432, #F91E62);
                position: relative;
            }

            .topbj img {
                width: 17px;
                height: 14px;
                position: absolute;
                left: 14px;
                top: 16px;
            }

            .topbj p {
                width: 100%;
                text-align: center;
                color: #FFFFFF;
                font-size: 17px;
                line-height: 44px;
            }

            .top-tab {
                width: 100%;
                display: flex;
                justify-content: space-between;
                align-items: center;
                height: 43px;
                background: #FFFFFF;
            }

            .top-tab .tab-item {
                width: 50%;
                text-align: center;
            }

            .top-tab .tab-item span {
                color: #444444;
                font-size: 13px;
                line-height: 13px;
                padding: 15px 0;
            }

            .top-tab .selection span {
                color: #FF6000;
                border-bottom: 2px solid #FF6000;
            }

            .tab-content {
                margin-top: 5px;
                background: #FFFFFF;

            }

            .content-ietm {
                display: none;
            }

            .tab-content .show {
                display: block;
            }

            .tab-content .help-a {}

            .tab-content .help-a li {
                width: 100%;
                border-bottom: 1px solid #F5F5F5;

                padding: 13px 15px;
            }

            .tab-content .help-a li a {
                width: 100%;
                display: block;
                overflow: hidden;
                display: flex;
                justify-content: space-between;
                align-items: center;
                text-decoration: none;
            }

            .tab-content .help-a li a span {
                color: #222222;
                font-size: 15px;
                line-height: 15px;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
            }

            .tab-content .help-a li a img {
                width: 7px;
                height: 12px;

            }

            .feedback {
                padding: 15px;
            }

            .feedback textarea {
                width: 100%;
                background: #F5F5F5;
                color: #B1B1B1;
                font-size: 13px;
                line-height: 24px;
                height: 300px;
                border: none;
                padding: 15px;

            }

            .feedback input.title-fh {
                width: 100%;
                height: 35px;
                font-size: 13px;
                line-height: 35px;
                color: #000;
                border: 1px solid #B1B1B1;
                margin-bottom: 10px;
                padding-left: 10px;
                border-radius: 2px;
            }

            .feedback input.tj-btn {
                width: 100%;
                text-align: center;
                height: 45px;
                font-size: 17px;
                line-height: 45px;
                color: #FFFFFF;
                background: linear-gradient(to right, #EE8432, #F91E62);
                border-radius: 30px;
                border: none;
                margin-top: 30px;
            }
        </style>
        <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    </head>
    <body style="background: #F5F5F5;">
        <div id="app_hyd">
            {{--<div class="topbj">--}}
                {{--<a href=""><img src="{{asset('img/back.png')}}" alt=""></a>--}}
                {{--<p>帮助与反馈</p>--}}
            {{--</div>--}}
            <div class="top-tab">
                <div class="tab-item selection"><span>常见问题</span></div>
                <div class="tab-item"><span>反馈</span></div>
            </div>
            <div class="tab-content">
                <div class="content-ietm show">
                    <ul class="help-a" id="ul-item">
                    </ul>
                </div>
                <div class="content-ietm feedback">
                    <input type="text" placeholder="请输入标题" class="title-fh">
                    <textarea onfocus="if(value=='请在此输入您的意见'){value=''}" onblur="if (value ==''){value='请在此输入您的意见'}">请在此输入您的意见</textarea>
                    <input type="submit" value="提交" class="tj-btn" />
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {

                window.alert = function(name){
                    var iframe = document.createElement("IFRAME");
                    iframe.style.display="none";
                    iframe.setAttribute("src", 'data:text/plain,');
                    document.documentElement.appendChild(iframe);
                    window.frames[0].window.alert(name);
                    iframe.parentNode.removeChild(iframe);
                }

                $(".top-tab .tab-item").click(function() {
                    var i = $(this).index();
                    $(this).addClass('selection').siblings().removeClass('selection');
                    $('.tab-content .content-ietm').eq(i).show().siblings().hide();
                })
                $(".feedback .tj-btn").click(function() {
                    var title = $(".feedback textarea").val().trim();
                    var contents = $(".feedback .title-fh").val().trim();
                    $.ajax({
                        type: "POST",
                        url: "http://veykoo.com:7777/api/user-feed-back",
                        dataType: "json",
                        data: {
                            title: title,
                            contents: contents
                        },
                        success: function(e) {
                            console.log("反馈成功");
                            $(".feedback textarea").val("");
                            $(".feedback .title-fh").val("");
                            if(e.ret == 0)
                            {
                                alert('提交成功');
                                window.location.reload();
                            }

                        },
                        error: function(e) {
                            console.log("反馈失败");

                        },
                    });
                })
                $.ajax({
                    type: "GET",
                    url: "http://veykoo.com:7777/api/user-feed-back",
                    dataType: "json",
                    data: {},
                    success: function(res) {
                        var data = res.data.list;
                        var ul = document.getElementById("ul-item");
                        var icohelp = "{{asset('img/ico-help.png')}}";
                        var feedbackshow_url = "{{url('api/feedbackshow')}}";
                        var html = "";
                        $.each(data, function(i, n) {
                            html += '<li><a href="' + feedbackshow_url + '?id=' + n.id + '"><span>' + n.title +
                                            '</span><img src="'+icohelp + '" /></a></li>';
                            // html += "<li><a href='/2.html?id=" + n.id + "'><span>" + n.title +
                            //     "</span><img src='images/ico-help.png'/></a></li>"

                        });


                        $('#ul-item').html(html);
                    },
                    error: function(res) {
                        console.log('请求数据失败');
                    },
                });

            })
        </script>
    </body>
</html>
